﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AJAXPMVC
{
    public class OdemePlaniController : MainClass
    {
        public new string oTema = "OdemePlaniController";
        string TabId = "#OdemePlaniListesi";
        public void OdemePlaniLoad()
        {
            FormModelGenerator frmArama = new FormModelGenerator();
            string Model = "OdemeArama";
            frmArama.FormEkleDuzenle(Model, "Ödeme Planı Sorgulama");
            frmArama.EkleTextBox(Model, "KimlikNo", "KimlikNo", "");
            frmArama.EkleTextBox(Model, "Ad", "Ad", "");
            frmArama.EkleTextBox(Model, "Soyad", "Soyad", "");

            frmArama.EkleDropDownList(Model, "ProgramId", "Program Adı", null, null, q.sq.getTipData("SELECT a.ProgramId id, a.ProgramAdi label, a.AkademikBirimAdi 'group' FROM  br.vw_FakulteProgramListesi a", "OdemePlani_vw_FakulteProgramListesi", false));


            frmArama.EkleDropDownList(Model, "HesapTipiId", "Hesap Seçimi", null, null, q.sq.getTipData("SELECT Id id, Kodu+' - '+Adi label,FirmaKodu + ' ' +HesapTipiAdi+ ' - '+ ISNULL(BankaAdi,'') AS 'group' FROM mh.vw_HesapKasa", "OdemePlani_vw_HesapKasa", false));

            frmArama.EkleDropDownList(Model, "SistemTipiId", "Sistem Tipi", null, null, q.sq.getTipData("SELECT Id id,a.SistemAdi label FROM mh.SistemTipi AS a WITH(NOLOCK)  ORDER BY label", "OdemePlani_SistemTipi", false));

            frmArama.EkleDropDownList(Model, "IslemTipiId", "İşlem Tipi", null, null, q.sq.getTipData("SELECT Id id,a.IslemTipi label FROM mh.IslemTipi AS a WITH(NOLOCK)  ORDER BY label", "OdemePlani_IslemTipi", false));

            frmArama.EkleDropDownList(Model, "ParaBirimiId", "Para Birimi", null, null, q.sq.getTipData("SELECT Id id, Kodu label FROM mh.ParaBirimi WITH(NOLOCK) ORDER BY id", "OdemePlani_ParaBirimi", false));

            frmArama.EkleDropDownList(Model, "OdemeSonucId", "Durum", null, null, q.sq.getTipData("SELECT Id id,Aciklama label FROM mh.OdemeSonuc WITH(NOLOCK) ORDER BY id", "OdemePlani_OdemeSonuc", false));

            frmArama.EkleDropDownList(Model, "BankaId", "Banka", null, null, q.sq.getTipData("SELECT Id id,BankaAdi label FROM mh.Banka WITH(NOLOCK) ORDER BY id", "OdemePlani_Banka", false));

            frmArama.EkleTextBox(Model, "ReferansKodu", "Referans Kodu", "");


            string Id = frmArama.FormEkleDuzenle("OdemePlanListesi","");


             frmArama.EkleButton(Model, oNamespace, oClassname, "OdemePlaniEkle", "FormData", "Ekle:true", " Ekle", ViewBtnClass.btn_default, fa_icons.fa_fa_plus);

            frmArama.EkleButton(Model, oNamespace, oClassname, "OdemePlaniArama", "FormData", "FormId:"+ Id.toSerializeJson()," Ara",ViewBtnClass.btn_warning,fa_icons.fa_fa_search_minus);

            //frmArama.EkleTextBox(Model, "KimlikNo", "KimlikNo", "");
            q.ajax.html(TabId, frmArama.Olustur());
        }
        public void OdemePlaniEkle()
        {
            string Model = "OdemePlaniDuzenleme";
            int OdemeId = 0;
            if (q.request["Ekle"].BoolNullValue() == false && q.request.ContainsKey("datarow") == false)
            {
                q.ajax.alert("asd", "Kayıt Seçmediniz");
                return;
            }
            else if(q.request["Ekle"].BoolNullValue() == false && q.request.ContainsKey("datarow") == true)
            {
                OdemeId=(int)q.request["datarow"].GetJObject()["Id"].toNullGetDecimalValue();

            }
            Ent vw_Odeme = new Ent();
            Ent vw_Borc=new Ent();
            if (OdemeId > 0) { 
                vw_Odeme = Ent.get("mh.vw_Odeme").Where("Id", OdemeId).FirstOrDefault<Ent>();
                vw_Borc = Ent.get("brc.vw_Borc").Where("Id", vw_Odeme["BorcId"]).FirstOrDefault<Ent>();
            }
            FormModelGenerator frm = new FormModelGenerator();
            frm.FormEkleDuzenle(Model, "Ödeme Planı Ekle - Düzenleme");
            FormAramaListesiNoktasi araList = new FormAramaListesiNoktasi();
            araList.Sql = @"SELECT  YilAdi ,
                                    DonemAdi ,
                                    UcretTipiKodu ,
                                    KimlikNo ,
                                    Ad ,
                                    Soyad ,
                                    AkademikBirimAdi ,
                                    ProgramAdi ,
                                    SinifAdi ,
                                    OgrenciDonemKayitDonemAdi ,
                                    BrutUcret ,
                                    ToplamIndirimOrani ,
                                    NetUcret ,
                                    EnBuyukTaksitSayisi ,
                                    ParaBirimiKodu ,
                                    FirmaAdi ,
                                    Id ,
                                    KullaniciId ,
                                    OgrenciBolumId
                            FROM    brc.vw_Borc WHERE ";


            araList.AddCol("YilAdi", "Borç Yılı",jqcolumntype.textbox, true);
            araList.AddCol("DonemAdi", "Borç Dönemi", jqcolumntype.textbox, true,false,true);
            araList.AddCol("UcretTipiKodu", "Üçret Tipi", jqcolumntype.textbox, true);
            araList.AddCol("KimlikNo", "Kimlik", jqcolumntype.textbox, true,false,true); 
            araList.AddCol("Ad", "Ad", jqcolumntype.textbox, true);
            araList.AddCol("Soyad", "Soyad", jqcolumntype.textbox, true); 
            araList.AddCol("AkademikBirimAdi", "Ust Birim", jqcolumntype.textbox, true); 
            araList.AddCol("ProgramAdi", "Program Adı", jqcolumntype.textbox, true, false, true); 
            araList.AddCol("SinifAdi", "Sınıf Adı", jqcolumntype.textbox, true, false, true); 
            araList.AddCol("OgrenciDonemKayitDonemAdi", "Sınıf Dönem", jqcolumntype.textbox, true, false, false);
            araList.AddCol("BrutUcret", "Brüt Ücret", jqcolumntype.number, true); 
            araList.AddCol("ToplamIndirimOrani", "Tpl. İnd. Oranı %", jqcolumntype.number, true);
            araList.AddCol("EnBuyukTaksitSayisi", "EnBuyukTaksitSayisi", jqcolumntype.number, true);
            araList.AddCol("NetUcret", "Net Ücret", jqcolumntype.number, true, false, true); 
            araList.AddCol("ParaBirimiKodu", "ParaBirimiKodu", jqcolumntype.textbox, true,false,true);
            araList.AddCol("FirmaAdi", "FirmaAdi", jqcolumntype.textbox, true);
            araList.AddCol("Id", "Id", jqcolumntype.number, true,true,false);
            araList.AddCol("KullaniciId", "KullaniciId", jqcolumntype.number, true);
            araList.AddCol("OgrenciBolumId", "OgrenciBolumId", jqcolumntype.number, true);

            frm.EkleAramaListesiNokta(Model, "BorcId", "Borç Seçimi (Öğrenci,Borç Dönemi,Program,Sinif,NetÜcret,PB.)", vw_Odeme["BorcId"].toNullStringValue(),

                vw_Borc["KimlikNo"].toNullStringValue() + "," +
                vw_Borc["DonemAdi"].toNullStringValue() + "," +
                vw_Odeme["ProgramAdi"].toNullStringValue() + "," +
                vw_Borc["SinifAdi"].toNullStringValue() + "," +
                vw_Odeme["NetUcret"].toNullStringValue() + "," +
                vw_Borc["ParaBirimiKodu"].toNullStringValue(), true, araList);


            FormAramaListesiNoktasi HesapKodu = new FormAramaListesiNoktasi();
            HesapKodu.Sql = @"SELECT Id,FirmaKodu,HesapTipiAdi,Adi,ParaBirimiId,ParaBirimiKodu,AktifMi FROM mh.vw_HesapKasa WHERE ";
            HesapKodu.AddCol("Id", "Id", jqcolumntype.number, true, true, false);
            HesapKodu.AddCol("FirmaKodu", "Firma Kodu", jqcolumntype.textbox, true, false, true); 
            HesapKodu.AddCol("HesapTipiAdi", "Hesap/Kasa", jqcolumntype.textbox, true, false, true); 
            HesapKodu.AddCol("Adi", "Adı", jqcolumntype.textbox, true, false, true);
            HesapKodu.AddCol("ParaBirimiKodu", "ParaBirimiKodu", jqcolumntype.textbox, true, false, true);

            frm.EkleAramaListesiNokta(Model, "HesapKasaId", "Hesap Kasa Seçimi (FirmaKodu,HTipi,Hesap,PB.)", vw_Odeme["HesapKasaId"].toNullStringValue(),

                vw_Odeme["HesapKasaFirmaKodu"].toNullStringValue() + "," +
                vw_Odeme["HesapTipiAdi"].toNullStringValue() + "," +
                vw_Odeme["HesapAdi"].toNullStringValue() + "," +
                vw_Odeme["HesapParabirimiKodu"].toNullStringValue()    , true, HesapKodu);

            frm.EkleTextBoxDate(Model, "DokumanTarihi", "Döküman Tarihi", vw_Odeme["DokumanTarihi"].toSerializeJson(), ViewDateFormat.Date, true);
            //frm.EkleTextBox(Model, "ReferansKodu", "Referans Kodu", Guid.NewGuid().ToString(), 50, false);
          
            frm.EkleDropDownList(Model, "IslemTipiId", "İşlem Tipi", vw_Odeme["IslemTipiId"].toNullStringValue(), vw_Odeme["IslemTipiAdi"].toNullStringValue(), q.sq.getTipData("SELECT Id id,IslemTipi label FROM  mh.IslemTipi WITH(NOLOCK)", "IslemTipiId", false),true);


            frm.EkleDropDownList(Model, "SistemTipiId", "İşlem Tipi", vw_Odeme["SistemTipiId"].toNullStringValue(), vw_Odeme["SistemTipiAdi"].toNullStringValue(), q.sq.getTipData("SELECT Id id,SistemAdi label FROM  mh.SistemTipi WITH(NOLOCK)", "SistemTipiId", false), true);


            frm.EkleDropDownList(Model, "OdemeSonucId", "Durum", vw_Odeme["OdemeSonucId"].toNullStringValue(), vw_Odeme["OdemeSonucAdi"].toNullStringValue(), q.sq.getTipData("SELECT Id id,Aciklama label FROM  mh.OdemeSonuc WITH(NOLOCK)", "OdemeSonucId", false),true);


            frm.EkleLabelBox(Model, "Referans Kodu", vw_Odeme["ReferansKodu"].toNullStringValue(), "ReferansKodu");

            frm.EkleLabelBox(Model,  "Bilgi Notu","İade İptal İşlemleri İade ve İptal İşlemleri Menüsünden Sadece Yetkili Personel Yapabilir.");



            string Id = frm.FormEkleDuzenle(Model + "_Liste","");

            jqw liste = new jqw(true);
            if (OdemeId > 0)
                liste.data = q.con.CollectionFromSql(@"SELECT  m.Id ,
                        m.OdemeId ,
                        m.Tutar ,
                        m.TaksitTarihi ,
                        m.BlokeTarihi ,
                        m.IadeTutar ,
                        m.IadeMi ,
                        m.OdemePlanindanDus ,
                        m.IadeIslemTipiId ,
                        i.IslemTipi ,
                        m.IadeKullaniciId ,
                        klc.Ad + ' ' + klc.Soyad AS IadeKullaniciAdi ,
                        m.IadeServisSicilKimlik ,
                        m.TaksitKomisyonTutar
                FROM    mh.OdemeTaksit m WITH ( NOLOCK )
                        LEFT JOIN mh.IslemTipi i WITH ( NOLOCK ) ON i.Id = m.IadeIslemTipiId
                        LEFT JOIN kul.Kullanici klc WITH ( NOLOCK ) ON klc.Id = m.IadeKullaniciId
                WHERE m.OdemeId=" + OdemeId).ToArray().getJsonArray();
            liste.AddColumn("Sil", "Sil", "100px", jqcolumntype.button, "", null, this.m("TaksitSil", "GridId:'" + liste.Id + "',datarow:datarow"), false);
            liste.AddColumn("Id", "Id", "40px", jqcolumntype.numberinput, "", null, null, false);
            liste.AddColumn("Taksit Tutarı", "Tutar", "100px", jqcolumntype.numberinput, "", null, null, true,true);
            liste.AddColumn("Taksit Tarihi", "TaksitTarihi", "100px", jqcolumntype.datetimeinput, "", null, null, true);
            liste.AddColumn("Bloke Tarihi", "BlokeTarihi", "100px", jqcolumntype.datetimeinput, "", null, null, true);
            liste.AddColumn("Taksit Komisyon Tutarı", "TaksitKomisyonTutar", "100px", jqcolumntype.numberinput, "", null, null, true,true);
            liste.AddColumn("İade Mi", "IadeMi", "100px", jqcolumntype.checkbox, "", null, null, false);
            liste.AddColumn("Ödeme Planindan Düş? (İptal Soransı)", "OdemePlanindanDus", "150px", jqcolumntype.checkbox, "", null, null, false);
            liste.AddColumn("İade Yapan", "IadeKullaniciAdi", "100px", jqcolumntype.textbox, "", null, null, false); 

            liste.AddDataColumns("Id", "int");
            liste.AddDataColumns("Tutar", "int");
            liste.AddDataColumns("TaksitKomisyonTutar", "decimal");
            liste.AddDataColumns("TaksitTarihi", "datetime");
            liste.AddDataColumns("BlokeTarihi", "datetime");
            liste.AddDataColumns("IadeMi", "bool");
            liste.AddDataColumns("OdemePlanindanDus", "bool");
            liste.AddDataColumns("IadeKullaniciAdi", "string");

            liste.Ozellikler.height = "300px";
            liste.Ozellikler.pageable = false;
            liste.Ozellikler.editable = true;
            liste.Ozellikler.autoRowHeight = false;
            
            liste.Ozellikler.selectionmode = jqselectionmode.singlecell;
            liste.Ozellikler.columnsResize = true;
            liste.Ozellikler.enableHover = true;
            liste.Ozellikler.showAggregates = true;
            liste.AddToolbarButton("Ekle", fa_icons.fa_fa_plus, this.m("TaksitEkle", "GridId:'#" + liste.Id + "', datarow:" + liste.getRowDatajs()));


            frm.EkleButton(Model, oNamespace, oClassname, "OdemePlaniKaydet", "FrmData", "Liste:" + liste.getDataJs()+", OdemeId: "+OdemeId, "Kaydet", ViewBtnClass.btn_success, fa_icons.fa_fa_save);
            string TabId = q.ajax.newTabId("asdf", "OdemePlaniOlustur", "Ödeme Planı Oluştur Düzenle");
            q.ajax.html("#OdemePlaniOlustur", frm.Olustur()+"\n"+liste.Olustur("OdemePlaniTaksit"));



            /*
             
SELECT Id ,
       HesapKasaId ,
       BorcId ,
       KullaniciId ,
       OlusturulmaTarihi ,
       DokumanTarihi ,
       OdemePlaniTutar ,
       SistemTipiId ,
       IslemTipiId ,
       ParaBirimiId ,
       TaksitSayisi ,
       TaksitToplamiTutar ,
       ReferansKodu ,
       OdemeSonucId ,
       IslemIp ,
       IslemUniq ,
       IslemXMLLog ,
       IslemSuresi ,
       IslemKuru ,
       DonenDataXMLlog ,
       ServisId ,
       OtomatikPesinIndrimiUygulandiMi FROM mh.Odeme
             */

        }
        public void OdemePlaniKaydet()
        {

            try
            {


                var FormData = q.request["FrmData"].GetJObject()["OdemePlaniDuzenleme"];
                var Liste = q.request["Liste"].GetJArray();

                int OdemeId = (int)q.request["OdemeId"].toNullGetDecimalValue();
                var ListeArray = Liste.Where(t => t["Tutar"].toNullGetDecimalValue() != null).ToArray();
                if (ListeArray.Count() == 0)
                {
                    q.ajax.alert("hata","Taksitleri Girmediniz.");
                    return;
                }
                Ent OdemePlani = Ent.get("mh.Odeme");
                if (OdemeId > 0)
                    OdemePlani = OdemePlani.Where("Id", OdemeId).FirstOrDefault<Ent>();
                else
                {
                    OdemePlani = OdemePlani.Create();
                    OdemePlani["ReferansKodu"] = Guid.NewGuid();
                    OdemePlani["IslemUniq"] = OdemePlani["ReferansKodu"];
                    OdemePlani["IslemXMLLog"] = "";
                    OdemePlani["DonenDataXMLlog"] = "";
                    OdemePlani["IslemSuresi"] = 0;
                    OdemePlani["IslemKuru"] = 1;
                    OdemePlani["OtomatikPesinIndrimiUygulandiMi"] = 0;
                }
                decimal Toplam = Liste.Sum(t => (decimal)t["Tutar"]);
                decimal TaksitToplamiTutar = Liste.Where(
                    t =>
                        t["IadeMi"].BoolNullValue() == false ||
                        (t["IadeMi"].BoolNullValue() == true && t["OdemePlanindanDus"].BoolNullValue() == true) /* İade ve İptallere göre Toplama*/
                ).Sum(t => (decimal)t["Tutar"]);

                Ent vw_HesapKasa = Ent.get("mh.vw_HesapKasa").Where("Id", (int)FormData["HesapKasaId"].toNullGetDecimalValue()).FirstOrDefault<Ent>();
                Ent vw_Servis = Ent.get("mh.Servis").Where("TanimKodu", "25c5be92-ffda-4343-a66a-712ab1597503").FirstOrDefault<Ent>();

                OdemePlani["HesapKasaId"] = (int)FormData["HesapKasaId"].toNullGetDecimalValue();
                OdemePlani["BorcId"] = (int)FormData["BorcId"].toNullGetDecimalValue();
                OdemePlani["KullaniciId"] = q.userdata.user.Id;
                OdemePlani["OlusturulmaTarihi"] = DateTime.Now;
                OdemePlani["DokumanTarihi"] = FormData["DokumanTarihi"].toNullStringValue();
                OdemePlani["OdemePlaniTutar"] = Toplam;
                OdemePlani["OdemeSonucId"] = (int)FormData["OdemeSonucId"].toNullGetDecimalValue();
                OdemePlani["DokumanTarihi"] = FormData["DokumanTarihi"].toNullStringValue();
                OdemePlani["SistemTipiId"] = (int)FormData["SistemTipiId"];
                OdemePlani["IslemTipiId"] = (int)FormData["IslemTipiId"];
                OdemePlani["ParaBirimiId"] = (int)vw_HesapKasa["ParaBirimiId"].toNullGetDecimalValue();
                OdemePlani["TaksitSayisi"] = ListeArray.Count();
                OdemePlani["TaksitToplamiTutar"] = TaksitToplamiTutar;
                OdemePlani["OdemeSonucId"] = (int)FormData["OdemeSonucId"];
                OdemePlani["IslemIp"] = conf.getIp();
                OdemePlani["ServisId"] = (int)vw_Servis["Id"];

                if(OdemePlani.Save())
                {
                    OdemeId = (int)OdemePlani["Id"].toNullGetDecimalValue();


                    for (int i = 0; i < ListeArray.Count(); i++)
                    {
                        var row = ListeArray[i];

                        Ent taksit = Ent.get("mh.OdemeTaksit");
                        if (row["Id"].toNullGetDecimalValue() > 0)
                            taksit = taksit.Where("Id", row["Id"].toNullGetDecimalValue()).FirstOrDefault<Ent>();
                        else
                        {
                            taksit = taksit.Create();
                            taksit["IadeTutar"] = 0;
                        }
                        taksit["OdemeId"] = OdemeId;
                        taksit["Tutar"] = row["Tutar"].toNullGetDecimalValue();
                        taksit["TaksitTarihi"] = row["TaksitTarihi"].ToObject<DateTime>();
                        taksit["BlokeTarihi"] = row["BlokeTarihi"].ToObject<DateTime>();
                        taksit["IadeMi"] = row["IadeMi"].BoolNullValue();
                        taksit["OdemePlanindanDus"] = row["OdemePlanindanDus"].BoolNullValue();
                        taksit["TaksitKomisyonTutar"] = row["TaksitKomisyonTutar"].toNullGetDecimalValue();
                        taksit.Save();

                    }


                }

            }
            catch (Exception ex)
            {
                 
            }


            /*
             
             */






        }
        public void TaksitEkle()
        {
            jqw.EkleNewRow(q.request["GridId"], false);
        }
        public void TaksitSil()
        {
            if (q.request["datarow"].GetJObject()["Id"].toNullGetDecimalValue() > 0)
            {
                q.ajax.alert("hata","Kayıt Edilmiş Taksit Silinemez Sadece Değiştirebilir.");
            }else
                jqw.DeleteRow(q.request["GridId"], q.request["datarow"].GetJObject()["uid"].toNullStringValue());
        }
        public void OdemePlaniArama()
        {
            Ent OdemeListesi = Ent.get("mh.vw_Odeme");
            var x = q.request["FormData"].GetJObject()["OdemeArama"];

            string[] Kontrol = new string[] {"KimlikNo", "Ad", "Soyad", "ReferansKodu" };
            for (int i = 0; i < Kontrol.Length; i++)
            {
                if (x[Kontrol[i]].toNullStringValue()!= null)
                    OdemeListesi.WhereLike(Kontrol[i], x[Kontrol[i]].toNullStringValue());
            }
            Kontrol = new string[] { "ProgramId", "HesapTipiId", "SistemTipiId", "IslemTipiId", "ParaBirimiId", "OdemeSonucId", "BankaId" };
            for (int i = 0; i < Kontrol.Length; i++)
            {
                int? Ids = (int?)x[Kontrol[i]].toNullGetDecimalValue();
                if(Ids.HasValue)
                { 
                    OdemeListesi.Where(Kontrol[i], Ids);
                }
            }

            jqw liste = new jqw(true);
            liste.data = OdemeListesi.ToListDynamic().ToArray().getJsonArray();

            liste.AddDataColumns("Id", "long");
            liste.AddColumn("Id", "Id", "80px");
            liste.AddDataColumns("HesapKasaId", "int");
            liste.AddDataColumns("HesapKasaFirmaId", "int");
            liste.AddDataColumns("HesapKasaFirmaAdi", "string"); 
            liste.AddColumn("Firma Adı", "HesapKasaFirmaAdi", "100px");
            liste.AddDataColumns("HesapKasaFirmaKodu", "string");
            liste.AddDataColumns("HesapTipiId", "int");
            liste.AddDataColumns("HesapTipiAdi", "string");
            liste.AddDataColumns("HesapTipKodu", "string");
            liste.AddDataColumns("BankaId", "int?");
            liste.AddDataColumns("BankaKodu", "string");

            liste.AddDataColumns("BankaAdi", "string"); 
            liste.AddColumn("Banka Adı", "BankaAdi", "100px");
            liste.AddDataColumns("HesapKodu", "string");
            liste.AddColumn("Hesap Kodu", "HesapKodu", "100px");
            liste.AddDataColumns("HesapAdi", "string");
            liste.AddDataColumns("HesapParaBirimiId", "int");
            liste.AddDataColumns("HesapParabirimiKodu", "string");
            liste.AddDataColumns("BorcId", "int");
            liste.AddDataColumns("AkademikBirimId", "long?");
            liste.AddDataColumns("AkademikBirimAdi", "string");

            liste.AddColumn("Akademik Birim", "AkademikBirimAdi", "100px");
            liste.AddDataColumns("ProgramId", "long?");
            liste.AddDataColumns("ProgramAdi", "string");
            liste.AddColumn("Program Adı", "ProgramAdi", "100px");
            liste.AddDataColumns("KullaniciId", "long");
            liste.AddDataColumns("KimlikNo", "string");
            liste.AddColumn("Kimlik No", "KimlikNo", "100px");
            liste.AddDataColumns("Ad", "string");
            liste.AddColumn("Ad", "Ad", "100px");
            liste.AddDataColumns("Soyad", "string");
            liste.AddColumn("Soyad", "Soyad", "100px");
            liste.AddDataColumns("BrutUcret", "decimal");
            liste.AddColumn("Brüt", "BrutUcret", "100px",jqcolumntype.numberinput);
            liste.AddDataColumns("ToplamIndirimOrani", "float?");
            liste.AddColumn("İnd.Oran", "ToplamIndirimOrani", "80px", jqcolumntype.numberinput);
            liste.AddDataColumns("NakitIndirimTutari", "decimal");
            liste.AddColumn("Nakit İnd.", "NakitIndirimTutari", "80px", jqcolumntype.numberinput);
            liste.AddDataColumns("NetUcret", "float?");
            liste.AddColumn("Net Ücret.", "NetUcret", "80px", jqcolumntype.numberinput);
            liste.AddDataColumns("OlusturulmaTarihi", "date");
            liste.AddColumn("Bildirim Tarihi", "OlusturulmaTarihi", "80px",jqcolumntype.datetime);
            liste.AddDataColumns("DokumanTarihi", "date");
            liste.AddColumn("Döküman Tarihi", "DokumanTarihi", "80px", jqcolumntype.datetime);
            liste.AddDataColumns("OdemePlaniTutar", "decimal");
            liste.AddColumn("Ödeme Plani Tutar", "OdemePlaniTutar", "80px", jqcolumntype.numberinput);
            liste.AddDataColumns("SistemTipiId", "int");
            liste.AddDataColumns("SistemTipiAdi", "string");
            liste.AddColumn("Sistem", "SistemTipiAdi", "80px");
            liste.AddDataColumns("IslemTipiId", "int");
            liste.AddDataColumns("IslemTipiAdi", "string");
            liste.AddColumn("İşlem Tipi", "IslemTipiAdi", "80px");
            liste.AddDataColumns("ParaBirimiId", "int");
            liste.AddDataColumns("ParaBirimiAdi", "string");
            liste.AddColumn("P.B.", "ParaBirimiAdi", "80px");
            liste.AddDataColumns("TaksitSayisi", "int");
            liste.AddDataColumns("TaksitToplamiTutar", "decimal");
            liste.AddDataColumns("ReferansKodu", "string");
            liste.AddColumn("RefKodu", "ReferansKodu", "80px");
            liste.AddDataColumns("OdemeSonucId", "int");
            liste.AddDataColumns("OdemeSonucAdi", "string");
            liste.AddDataColumns("OdemeSonucBasariDurumu", "bool");
            liste.AddDataColumns("IslemIp", "string");
            liste.AddDataColumns("IslemUniq", "Guid");
            liste.AddDataColumns("IslemXMLLog", "string");
            liste.AddDataColumns("IslemSuresi", "int");
            liste.AddDataColumns("IslemKuru", "decimal");
            liste.AddDataColumns("DonenDataXMLlog", "string");
            liste.AddDataColumns("ServisId", "int");
            liste.AddDataColumns("OtomatikPesinIndrimiUygulandiMi", "bool");
            liste.Ozellikler.height = "500px";
            liste.Ozellikler.pageable = true;
            liste.Ozellikler.editable = false;
            liste.Ozellikler.autoRowHeight = false;
            liste.PostRowDetailsFunc(oNamespace, oClassname, "OdemePlaniTaksitDettay", "Detay");

            liste.Ozellikler.columnsResize = true;
            liste.Ozellikler.enableHover = true;
            liste.Ozellikler.filterable = true;
            liste.Ozellikler.filterMode = jqfilterMode.Simple;
            liste.Ozellikler.rowdetails = true;
            liste.Ozellikler.columnsResize = true;
            liste.Ozellikler.selectionmode = jqselectionmode.singlecell;
            liste.Ozellikler.enableHover = true;
            liste.Ozellikler.groupable = true;
            liste.AddToolbarButton("Düzenle", fa_icons.fa_fa_plus, this.m("OdemePlaniEkle", "Ekle:false,datarow:" + liste.getRowDatajs()),ViewBtnClass.btn_default);


            q.ajax.html("#" + q.request["FormId"], liste.Olustur("OdemePlaniGosterimi"));
        }
        public void OdemePlaniTaksitDettay()
        {

            var rowDet=jqw.getRowDetails("Detay"); 
            string Id = rowDet["Select"].ToString();
            jqw liste = new jqw(true);
            liste.data = Ent.get("mh.OdemeTaksit").Where("OdemeId",rowDet["data"]["Id"].toNullStringValue()).ToListDynamic().ToArray().getJsonArray(); 
            liste.AddDataColumns("Id", "long"); 
            liste.AddColumn("Id", "Id", "80px");
            liste.AddDataColumns("OdemeId", "long"); 
            liste.AddColumn("Odeme Id", "OdemeId", "100px",jqcolumntype.numberinput);
            liste.AddDataColumns("Tutar", "number");
            liste.AddColumn("Taksit Tutar", "Tutar", "100px", jqcolumntype.numberinput);
            liste.AddDataColumns("TaksitKomisyonTutar", "number");
            liste.AddColumn("Komisyon", "TaksitKomisyonTutar", "100px", jqcolumntype.numberinput); 
            liste.AddDataColumns("TaksitTarihi", "date");
            liste.AddColumn("Taksit Tarihi", "TaksitTarihi", "150px",jqcolumntype.date); 
            liste.AddDataColumns("BlokeTarihi", "date");
            liste.AddColumn("Bloke Tarihi", "BlokeTarihi", "150px", jqcolumntype.date);  
            liste.AddDataColumns("IadeMi", "bool");
            liste.AddColumn("İade Mi?", "IadeMi", "80px", jqcolumntype.checkbox);  
            liste.Ozellikler.height = "220px";
            liste.Ozellikler.pageable = false;
            liste.Ozellikler.editable = false;
            liste.Ozellikler.autoRowHeight = false; 
            liste.Ozellikler.selectionmode = jqselectionmode.singlecell;
            q.ajax.html(Id, liste.Olustur());

        }
    }
}