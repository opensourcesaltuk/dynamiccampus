﻿using AJAXPMVC.Ents;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AJAXPMVC
{
    public class AutoCopleteBox
    {
        public void CompleteBoxList()
        {
           string e = q.request["event"];
            try
            {
                e = q.sessionaes.AES_decrypt(q.request["event"]); 
                string cacheKeyFirst = "ComleteBox:_" + e + "_event"; 
                CompleteBox bx = cacheKeyFirst.CacheOku<CompleteBox>();
                if (bx == null)
                {
                    bx = Ent.get<CompleteBox>().Where("Kod", e).FirstOrDefault<CompleteBox>();
                    if (bx == null)
                        return;
                    bx.CacheYaz<CompleteBox>(cacheKeyFirst);

                }
                string dataKeys = q.request["data"] ?? "";
                

                cacheKeyFirst = "ComleteBox_Cache:_" + e + "_Kullanici_" + (bx.Kullanici ? q.userdata.user.Id.ToString() : "0") + "_q_data_" + q.request["q"].Md5Sum()+"_"+dataKeys.Md5Sum();
                DataTable data = cacheKeyFirst.CacheOku<string>().unSerialize<DataTable>();
                if (data == null)
                {
                    string sql = bx.tSql;
                    if (bx.Kullanici == true)
                    {
                        sql = sql.Replace("@KullaniciNo", q.userdata.user.Id.ToString());
                    }
                    var par = new Dictionary<string, object>();
                    par.Add("@q", q.request["q"]);
                    if (dataKeys.Length > 0)
                    {
                        Newtonsoft.Json.Linq.JArray array = Newtonsoft.Json.Linq.JArray.Parse(dataKeys);
                        if (array.Count > 0)
                        {
                            try
                            {
                                for (int i = 0; i < array.Count; i++)
                                {
                                    par.Add(q.sessionaes.AES_decrypt(array[i]["Par"].ToString()), array[i]["Val"].ToString());

                                }
                            }
                            catch { }
                        }

                    }

                    data = q.con.DataTable(sql, par);
                    data.toSerialize().CacheYaz<string>(cacheKeyFirst);
                }
                Newtonsoft.Json.Linq.JArray ob = data.toSerialize().unSerialize<Newtonsoft.Json.Linq.JArray>();

                q.sq.unsetAjax = true;
                q.ajax.WriteText(ob.ToString());
             


            }
            catch (Exception ex)
            {

            }
        }
    }
}