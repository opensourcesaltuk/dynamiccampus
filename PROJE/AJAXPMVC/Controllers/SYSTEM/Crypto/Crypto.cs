﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
public static class Crypto
{
    static string AES_Key = ("PTVJQRk9QTEpNVU1DWUZCRVFGV1VVT0ZOV1RRU1NaWQ=");
    static string AES_IV = ("sKHmqedpc4GpIYIKnDk06g==");
    static string Base32Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
    static string Base36Alphaber = "0123456789abcdefghijklmnopqrstuvwxyz";
    static Random rnd = new Random();
    static CryptoAes sif = new CryptoAes(AES_Key, AES_IV);

    public static string AES_encrypt(this string Input)
    {
        return sif.AES_encrypt(Input);
    }
    public static string AES_decrypt(this string Input)
    {
        return sif.AES_decrypt(Input);
    }
    public static string Md5Sum(this string strToEncrypt)
    {
        return sif.Md5Sum(strToEncrypt);
    }

    public static AES_DEF getKey(){
        RijndaelManaged aes = new RijndaelManaged();
        aes.KeySize = 256;
        aes.BlockSize = 128;
        aes.Padding = PaddingMode.PKCS7;
        aes.Mode = CipherMode.ECB;

        aes.GenerateKey();
        aes.GenerateIV();

        AES_DEF def = new AES_DEF();
        def.Key = Convert.ToBase64String(aes.Key);
        def.IV = Convert.ToBase64String(aes.IV);
        return def;
    }


    static int InByteSize = 8;
    static int OutByteSize = 5;
    public static string Base32_encrypt(this string bytes)
    {
        if (bytes == null) return null;
        else if (bytes.Length == 0) return string.Empty;
        StringBuilder builder = new StringBuilder(bytes.Length * InByteSize / OutByteSize);
        int bytesPosition = 0;
        int bytesSubPosition = 0;
        byte outputBase32Byte = 0;
        int outputBase32BytePosition = 0;
        while (bytesPosition < bytes.Length)
        {
            int bitsAvailableInByte = Math.Min(InByteSize - bytesSubPosition, OutByteSize - outputBase32BytePosition);
            outputBase32Byte <<= bitsAvailableInByte;
            outputBase32Byte |= (byte)(bytes[bytesPosition] >> (InByteSize - (bytesSubPosition + bitsAvailableInByte)));
            bytesSubPosition += bitsAvailableInByte;
            if (bytesSubPosition >= InByteSize)
            {
                bytesPosition++;
                bytesSubPosition = 0;
            }
            outputBase32BytePosition += bitsAvailableInByte;
            if (outputBase32BytePosition >= OutByteSize)
            {
                outputBase32Byte &= 0x1F;
                builder.Append(Base32Alphabet[outputBase32Byte]);
                outputBase32BytePosition = 0;
            }
        }
        if (outputBase32BytePosition > 0)
        {
            outputBase32Byte <<= (OutByteSize - outputBase32BytePosition);
            outputBase32Byte &= 0x1F;
            builder.Append(Base32Alphabet[outputBase32Byte]);
        }
        return builder.ToString();
    }
    public static byte[] Base32_decrypt(this string base32String)
    {
        if (base32String == null) return null;
        else if (base32String == string.Empty) return new byte[0];
        string base32StringUpperCase = base32String.ToUpperInvariant();
        byte[] outputBytes = new byte[base32StringUpperCase.Length * OutByteSize / InByteSize];
        if (outputBytes.Length == 0)
            throw new ArgumentException("Specified string is not valid Base32 format because it doesn't have enough data to construct a complete byte array");
        int base32Position = 0;
        int base32SubPosition = 0;
        int outputBytePosition = 0;
        int outputByteSubPosition = 0;
        while (outputBytePosition < outputBytes.Length)
        {
            int currentBase32Byte = Base32Alphabet.IndexOf(base32StringUpperCase[base32Position]);
            if (currentBase32Byte < 0)
                throw new ArgumentException(string.Format("Specified string is not valid Base32 format because character \"{0}\" does not exist in Base32 alphabet", base32String[base32Position]));

            int bitsAvailableInByte = Math.Min(OutByteSize - base32SubPosition, InByteSize - outputByteSubPosition);
            outputBytes[outputBytePosition] <<= bitsAvailableInByte;
            outputBytes[outputBytePosition] |= (byte)(currentBase32Byte >> (OutByteSize - (base32SubPosition + bitsAvailableInByte)));
            outputByteSubPosition += bitsAvailableInByte;
            if (outputByteSubPosition >= InByteSize)
            {
                outputBytePosition++;
                outputByteSubPosition = 0;
            }
            base32SubPosition += bitsAvailableInByte;
            if (base32SubPosition >= OutByteSize)
            {
                base32Position++;
                base32SubPosition = 0;
            }
        }
        return outputBytes;
    }
    public static string toBase36(this Int64 deci)
    {
        if (deci < 0) throw new ArgumentOutOfRangeException("input", deci, "input cannot be negative");
        char[] clistarr = Base36Alphaber.ToCharArray();
        var result = new Stack<char>();
        while (deci != 0)
        {
            result.Push(clistarr[deci % 36]);
            deci /= 36;
        }
        return new string(result.ToArray());
    }
    public static Int64 FromBase36(this string base36str)
    {
        var reversed = base36str.ToLower().Reverse();
        long result = 0;
        int pos = 0;
        foreach (char c in reversed)
        {
            result += Base36Alphaber.IndexOf(c) * (long)Math.Pow(36, pos);
            pos++;
        }
        return result;
    }
    public static string NewPass(int charCount, bool alfanumeric = false, bool anlasilirlik = false)
    {
        string buyuk = "ABCDEFGHJKLMNPRSTUVYZQWX";
        string kucuk = "abcdefghijkmnoprstuvyzqwx";
        string sayi = "23456789";
        string alfanum = "!#%+?";
        int minVal = 1;
        int maxVal = alfanumeric == true ? charCount - 4 : charCount - 3;
        rnd = new Random();
        List<string> a = new List<string>();
        int byk = rnd.Next(minVal, maxVal);
        int kck = rnd.Next(minVal, charCount - byk);
        int sy = alfanumeric == true ? rnd.Next(minVal, charCount - byk - kck) : charCount - byk - kck;
        int alf = alfanumeric == true ? charCount - byk - kck - sy : 0;
        if (anlasilirlik)
        {
            string sBuyuk = "BCDFGHJKLMNPRSTVYZW";
#pragma warning disable CS0219 // The variable 'eBuyuk' is assigned but its value is never used
            string eBuyuk = "AEU";
#pragma warning restore CS0219 // The variable 'eBuyuk' is assigned but its value is never used
            string sKucuk = "bcdfghjkmnprstvyzw";
            string eKucuk = "aeiou";
            sy = sy >= 4 ? sy - 2 : sy;
            minVal = 1;
            byk = rnd.Next(minVal, maxVal);
            byk = (charCount - (sy + alf)) / 2;
            kck = (charCount - byk);
            int elCo = 0;
            for (int i = 0; i < charCount - 1 - sy - alf; i++)
            {
                if (i == 0)
                {
                    elCo = sBuyuk.Length;
                    a.Add(sBuyuk[rnd.Next(0, elCo)].ToString());
                }
                else
                {
                    elCo = sKucuk.Length;
                    if (i % 2 == 0)
                    {
                        elCo = sKucuk.Length;
                        a.Add(sKucuk[rnd.Next(0, elCo)].ToString());
                    }
                    else
                    {
                        elCo = eKucuk.Length;
                        a.Add(eKucuk[rnd.Next(0, elCo)].ToString());
                    }
                }
            }
            List<string> b = new List<string>();
            elCo = sayi.Length;
            for (int i = 1; i <= sy; i++) a.Add(sayi[rnd.Next(0, elCo)].ToString());
            elCo = alfanum.Length;
            for (int i = 1; i <= alf; i++) a.Add(alfanum[rnd.Next(0, elCo)].ToString());
            b = b.OrderBy(c => rnd.Next()).ToList();
            for (int i = 0; i < b.Count; i++)
            {
                a.Add(b[i].ToString());
            }
        }
        else
        {
            int elCo = buyuk.Length;
            for (int i = 1; i <= byk; i++) a.Add(buyuk[rnd.Next(0, elCo)].ToString());
            elCo = kucuk.Length;
            for (int i = 1; i <= kck; i++) a.Add(kucuk[rnd.Next(0, elCo)].ToString());
            elCo = sayi.Length;
            for (int i = 1; i <= sy; i++) a.Add(sayi[rnd.Next(0, elCo)].ToString());
            elCo = alfanum.Length;
            for (int i = 1; i <= alf; i++) a.Add(alfanum[rnd.Next(0, elCo)].ToString());
            a = a.OrderBy(c => rnd.Next()).ToList();
        }

        return String.Join("", a.ToArray());
    }
}