﻿$$ = {
    Domain: null,
    SessionId: null,
    AJAXP: {
        SendData: function (data, btnObject) {
            if (btnObject != null)
            {
                $(btnObject).find('i').addClass('fa-spin');
                $(btnObject).prop('disabled', true);
            }
            $.ajax({
                type: "POST",
                url: $$.Domain + "Main/Index/?ASPSESSID=" + $$.SessionId,
                data: data,
                btnObject: btnObject,
                success: function (html) {
                    debugger;
                    //html = html || $.trim(html);
                    //$("#sonuc").html(html);
                    html = JSON.parse(html);
                    var snc = $$.AJAXP.runScript(html);
                    try { 
                        $(this.btnObject).find('i').removeClass('fa-spin');
                        $(this.btnObject).prop('disabled', false);
                    } catch (e) {

                    }

                },

                error: function (xhr, ajaxOptions, thrownError) {
                    try { 
                        $(this.btnObject).find('i').removeClass('fa-spin'); 
                        $(this.btnObject).prop('disabled', false);
                    } catch (e) { };
                }
            });

        },
        POST: function (data, btnObject) { $$.AJAXP.SendData(data, btnObject); },
        runScript: function (jsc) {
            for (var i = 0; i < Object.keys(jsc).length; i++) {
                try {
                    eval(jsc[Object.keys(jsc)[i]]);
                } catch (e) {
                    console.log(e);
                }
            }
            $$.AJAXP.lastFunc();
            return true;
        },
        lastFunc: function () {
            //LoadForm();

            $('input[type=checkbox]').not('[class*=icheckbox_square]').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        }
    },
    load: function (data) {
        $$.Domain = data.Domain;
        $$.SessionId = data.SessionId;
        $$.AJAXP.runScript(data.LastData);
    },
    C: {}
};
$$.MainTab = {
    Tabli: null, Tabcon: null,
    Tablist: {},
    load: function (tabli, content, MainTabWindow) {
        $$.MainTab.Tabli = tabli;
        $$.MainTab.Tabcon = content;
        $$.MainTab.MainTabWindow = MainTabWindow;
        $($$.MainTab.Tabli).jqxSortable();
    },
    counter: 0,
    add: function (generated, id, TabName) {
        if (generated) {
            $$.MainTab.counter += 1;
            id = "NewIslemDivi_" + $$.MainTab.counter.toString() + "_dv";
        }

        var obj = $$.MainTab.Tablist[id];
        if (obj == undefined || obj == null) {
            var tabName = "<li class=\"panels\" tabId=\"" + id + "\" onmousedown=\"$$.MainTab.mousedown('" + id + "',event)\"><a onmousedown=\"$$.MainTab.mousedown('" + id +"',event)\" href=\"#" + id + "\" data-toggle=\"tab\" style=\"float:left;\">" + TabName + "</a><div onclick=\"$$.MainTab.close('" + id + "')\" class=\"fa fa-close btn\" style=\"    float: right;margin-left: -17px;padding: 3px 3px 3px 3px;position: absolute;margin-top: -5px;\"></div></li>";
            var divContent = "<div class=\"tab-pane window-tab\" id=\"" + id + "\">" + TabName + "</div>";
            obj = {
                tabName: tabName,
                divContent: divContent,
                id: id
            };
            // $$.MainTab.Tab.jqxTabs('addFirst', obj.tabName, obj.divContent,54);
            $$.MainTab.Tabli.append(obj.tabName);
            $$.MainTab.Tabcon.append(obj.divContent);
            $$.MainTab.Tablist[id] = obj;
        } else {


        }

        $("#" + id).html('');
        $("#" + id).append('<div id="load' + id + '" style="overflow:hidden;"><div class="loader">Lütfen Bekleyiniz. - Loading...</div></div>');
        // $("#load" + id).jqxLoader({ isModal: true, width: 100, height: 100, imagePosition: 'top' });


        $$.MainTab.focus(id);
        try {
            $$.MainTab.resize();
        } catch (e) {

        }
        return id;
    },
    close: function (id) {
        debugger;
        var ids = $$.MainTab.Tabli.find('li[tabid=' + id + ']').prev().attr('tabid');
        $$.MainTab.Tabli.find('li[tabid=' + id + ']').remove();
        $$.MainTab.Tabcon.find('#' + id).remove();
        delete $$.MainTab.Tablist[id];
        try { 
            if (ids.toString().length == 0)
                ids = $$.MainTab.Tabli.find('li:first').attr('tabid')
        } catch (e) { 
            ids = $$.MainTab.Tabli.find('li:first').attr('tabid');
        }
        $$.MainTab.focus(ids);
    },
    focus: function (id) {

        $$.MainTab.Tabli.find('.panels[tabid!=' + id + ']').removeClass('active');
        $$.MainTab.Tabli.find('.panels[tabid=' + id + ']').addClass('active');
        $$.MainTab.Tabcon.find('.tab-pane').removeClass('active');
        $$.MainTab.Tabcon.find("#" + id).addClass('active');
    },
    resize: function () {
        var h = $('#wrap').height() - 52;
        $('.window-tab').height(h);
    },
    mousedown: function(id,event) {
        if (event.button == 1 || event.button == 4) {
            $$.MainTab.close(id)
        }
    }
};
$$.MENU = {
    list: null,
    RUN: function (Id) {
        try {
            try {

                $$.dialog.TumuIndir = false;
                $$.dialog.TumuIndirKaldir();
            } catch (e) {

            }

            eval($$.MENU.list[Id].mFunc);
        } catch (e) {
            console.log(e);
        }
        if ($('.main-header a.logo').css('display') == "none")
            $('.sidebar-toggle').trigger('click');
    },
    load: function (list) {
        $$.MENU.list = list;
    }
};