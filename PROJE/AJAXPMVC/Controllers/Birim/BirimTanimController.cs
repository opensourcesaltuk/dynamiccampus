﻿using AJAXPMVC.Ents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AJAXPMVC
{
    public class BirimTanimController : MainClass
    {

        public new string oTema = "BirimTanimController";


        public vw_Birim[] getBirimArray(long? Id)
        {
            string Srg = "";
            if (Id != null)
            {
                Srg = " WHERE bir.Id=" + Id.Value.ToString();
            }
            vw_Birim[] sonuc = q.con.CollectionFromSql(@"SELECT
	                                                            bir.Id
                                                               ,bir.UstBirimId
                                                               ,bir.BirimTipId
                                                               ,birimtip.Ad AS BirimTipAdi
                                                               ,bir.BirimAdi
                                                               ,bir.BirimAdiGlobal
                                                               ,ustBir.BirimAdi AS UstBirimAdi
                                                               ,bir.Siralama
                                                               ,bir.AktifId
                                                               ,evet.Ad AS AktifAdi
                                                               ,bir.BirimKodu
                                                            FROM 
                                                            br.Birim bir WITH (NOLOCK)
                                                            LEFT JOIN br.Birim ustBir WITH(NOLOCK) ON ustBir.Id=bir.UstBirimId
                                                            INNER JOIN tpl.vw_TipGrupListesi birimtip
	                                                            ON birimtip.GrupKodu = 'birimtipi'
		                                                            AND bir.BirimTipId = birimtip.TipId
                                                            INNER JOIN tpl.vw_TipGrupListesi evet
	                                                            ON evet.GrupKodu = 'evethayir'
		                                                            AND bir.AktifId = evet.TipId
                                                            "+ Srg + @"
                                                            ORDER BY bir.Siralama", null).ToArray().toSerialize().unSerialize<vw_Birim[]>();
            return sonuc;

        }
        public vw_BirimGenelOzellik[] GetBirimGenelOzellik(long Id)
        {
            vw_BirimGenelOzellik[] sonuc = q.con.CollectionFromSql(@"SELECT
	                                                                                        ISNULL(br.Id, 0) AS Id
                                                                                           ,ISNULL(br.AzamiYariYil, 0) AzamiYariYil
                                                                                           ,ISNULL(br.KacYariYil, 0) KacYariYil
                                                                                           ,br.WebGorunsunId WebGorunsunId
                                                                                           ,a.Ad WebGorunsunAdi
																						   ,ISNULL(br.HazirlikVarMi,0) HazirlikVarMi
																						   ,br.HazirlikModulId
																						   ,hazirlikmodul.Ad HazirlikModulAdi
			                                                                               ,br.IsyeriId
			                                                                               ,f.FirmaAdi + ' - ' +i.IsyeriAdi IsyeriAdi
                                                                                        FROM (SELECT
		                                                                                        1 AS a) b
                                                                                        LEFT JOIN br.BirimGenelOzellik br WITH (NOLOCK)
	                                                                                        ON b.a = b.a AND br.Id=" + Id.ToString()+ @"
                                                                                        LEFT JOIN tpl.vw_TipGrupListesi a
	                                                                                        ON a.GrupKodu = 'evethayir'
		                                                                                        AND a.TipId = ISNULL(br.WebGorunsunId, 1)
                                                                                        LEFT JOIN tpl.vw_TipGrupListesi hazirlikmodul
	                                                                                        ON hazirlikmodul.GrupKodu = 'hazirlikmodul'
		                                                                                        AND hazirlikmodul.TipId = ISNULL(br.HazirlikModulId, 1)
		                                                                                LEFT JOIN mh.Isyeri i (NOLOCK) ON i.Id = br.IsyeriId
		                                                                                LEFT JOIN mh.Firma f (NOLOCK) ON f.Id = i.FirmaId", null).ToArray().toSerialize().unSerialize< vw_BirimGenelOzellik[]>();
            return sonuc;

        }
        public vw_BirimKodlari[] GetBirimKodlari(long Id)
        {
            vw_BirimKodlari[] sonuc = q.con.CollectionFromSql(@"SELECT
	                                                                                        ISNULL(br.Id,0) as Id,
	                                                                                        ISNULL(br.BirimId,"+ Id.ToString()+ @") as BirimId,
	                                                                                        ISNULL(br.AktarimOsymKodu,'') as AktarimOsymKodu,
	                                                                                        ISNULL(br.AktarimBirimKodu,'') as AktarimBirimKodu  ,
	                                                                                        ISNULL(br.BildirimOysmKodu,'') as BildirimOysmKodu,
	                                                                                        ISNULL(br.BildirimBirimKodu,'') as BildirimBirimKodu,
	                                                                                        aktif.TipId AS AktifId,
	                                                                                        aktif.Ad AktifAdi,
	                                                                                        aktifw.TipId WebGorunsunId,
	                                                                                        aktifw.Ad as WebGorunsunAdi,
	                                                                                        oysmBurs.TipId as BursTipId,
	                                                                                        oysmBurs.Ad as BursTipAdi
                                                                                        FROM tpl.vw_TipGrupListesi oysmBurs
                                                                                        LEFT JOIN br.BirimKodlari br WITH (NOLOCK)
	                                                                                        ON br.BursTipId = oysmBurs.TipId AND br.BirimId=" + Id.ToString() + @"
                                                                                        LEFT JOIN tpl.vw_TipGrupListesi aktif
	                                                                                        ON aktif.GrupKodu = 'evethayir'
		                                                                                        AND aktif.TipId = ISNULL(br.AktifId, 1)
                                                                                        LEFT JOIN tpl.vw_TipGrupListesi aktifw
	                                                                                        ON aktifw.GrupKodu = 'evethayir'
		                                                                                        AND aktifw.TipId = ISNULL(br.WebGorunsunId, 1)
                                                                                        WHERE oysmBurs.GrupKodu = 'osymburs'
                                                                                        order by oysmBurs.BasDec").ToArray().toSerialize().unSerialize< vw_BirimKodlari[]>();
            return sonuc;

        }
        public void load()
        {
            string TabId = q.request["TabDivId"]= "BirimTanimlariList";

            jqw liste = new jqw(true);

            liste.data = getBirimArray(null).getJsonArray();
           
            liste.AddDataColumns("Id", "number");
            liste.AddDataColumns("UstBirimId", "number");
            liste.AddDataColumns("BirimTipId", "number");
            liste.AddDataColumns("BirimTipAdi", "string");
            liste.AddDataColumns("BirimAdi", "string");
            liste.AddDataColumns("BirimAdiGlobal", "string");
            liste.AddDataColumns("UstBirimAdi", "string"); 
            liste.AddDataColumns("Siralama", "string");
            liste.AddDataColumns("AktifId", "number");
            liste.AddDataColumns("AktifAdi", "string"); 
            liste.AddDataColumns("BirimKodu", "string");

            liste.AddColumn("Birim Tipi", "BirimTipAdi", "140px");
            liste.AddColumn("Birim Adı", "BirimAdi", "40%");
            liste.AddColumn("Birim Adı Global", "BirimAdiGlobal", "30%");
            liste.AddColumn("Sıralama", "Siralama", "80px");
            liste.AddColumn("Aktif Mi?", "AktifAdi", "80px");
            liste.AddColumn("Birim Kodu", "BirimKodu", "80px");

            liste.AddColumn("Id", "Id", "50px");
            liste.hierarchy("Id", "UstBirimId");
            liste.SourceId = "Id";
            //liste.AddColumn("Düzenle", "edit2", "100px", jqcolumntype.button, "", null, q.ajax.newTabId("Duzenleme2", "BirimDuzenleme", "Birim Düzenleme",true)+ this.m("Duzenle", "datarow:datarow"));


            liste.AddToolbarButton("Ekle", fa_icons.fa_fa_plus, q.ajax.newTabId("Duzenleme2", "BirimDuzenleme", "Birim Düzenleme", true) + this.m("Duzenle", "ekle:true,datarow:null"));
            liste.AddToolbarButton("Düzenle", fa_icons.fa_fa_pencil, q.ajax.newTabId("Duzenleme2", "BirimDuzenleme", "Birim Düzenleme", true) + this.m("Duzenle", "ekle:false,datarow:"+liste.getRowDatajs()));
            liste.Ozellikler.height = "720px";
            liste.Ozellikler.disabled = false;
            liste.Ozellikler.filterMode = jqfilterMode.Advanced;
            liste.Ozellikler.columnsResize = true;
            liste.Ozellikler.enableHover = true; 

            string html = liste.Olustur();
            q.ajax.html("#" + TabId, html); 
        } 
        public void Duzenle()
        {
            vw_Birim birim = q.request["datarow"].unSerialize<vw_Birim>();
            vw_ModelBirim model = new vw_ModelBirim();
            long BirimId = 0;
            if (birim != null && birim.Id > 0)
            {
                BirimId = birim.Id;
                model.Birim = getBirimArray(BirimId).FirstOrDefault();

            }
            else
            {
                model.Birim = new vw_Birim();
                model.Birim.BirimAdi = "Yeni Birim";
            }
            model.BirimOzellikleri = GetBirimGenelOzellik(BirimId).FirstOrDefault();
            model.BirimKodlari = GetBirimKodlari(BirimId).ToArray();
            q.viewData["BirimTip"] = model.BirimTipi= q.con.CollectionFromSql("SELECT a.* from tpl.vw_TipGrupListesi a where a.GrupKodu='birimtipi'").ToArray().toSerialize().unSerialize<vw_TipUst[]>();
            q.viewData["EvetHayir"] = q.con.CollectionFromSql("SELECT a.* from tpl.vw_TipGrupListesi a where a.GrupKodu='evethayir'").ToArray().toSerialize().unSerialize<vw_TipUst[]>();

            q.viewData["HazirlikModulId"] = q.con.CollectionFromSql("SELECT a.* FROM tpl.vw_TipGrupListesi a where a.GrupKodu='hazirlikmodul'").ToArray().toSerialize().unSerialize<vw_TipUst[]>();
            q.viewData["IsyeriId"] = q.con.CollectionFromSql("SELECT  i.Id TipId,f.FirmaAdi + ' - ' +i.IsyeriAdi AS Ad FROM mh.Firma f (NOLOCK) INNER JOIN mh.Isyeri i (NOLOCK) ON i.FirmaId = f.Id").ToArray();
            q.viewData["modelBirim"] = model;

            q.viewData["CompleteBirimFunc"] = q.ajax.set_combobox("asd","birimler", "#BirimDuzenlemeRow #UstBirimId",true,model.Birim.UstBirimId.HasValue?model.Birim.UstBirimId.Value.ToString():null,model.Birim.UstBirimAdi); // q.sq.m("AJAXPMVC", "AutoCopleteBox", "CompleteBoxList", "event=birimler", mGetPost.GET);

            q.viewData["FuncSave"] = this.m("Kaydet", "data:data");

            string html = getPageHtml("Duzeleme");

            q.ajax.newTabId("Duzenleme", "BirimDuzenleme", "Birim Düzenleme - " + model.Birim.BirimAdi);


            q.ajax.html("#BirimDuzenleme", html);
           



            jqw grid = new jqw(false, "BursTanimlariGridData");

            grid.AddDataColumns("Id", "long");
            grid.AddDataColumns("BirimId", "long");
            grid.AddDataColumns("AktarimOsymKodu", "string");
            grid.AddDataColumns("AktarimBirimKodu", "string");
            grid.AddDataColumns("BildirimOysmKodu", "string");
            grid.AddDataColumns("BildirimBirimKodu", "string");
            grid.AddDataColumns("AktifId", "long");
            grid.AddDataColumns("WebGorunsunId", "long");
            grid.AddDataColumns("BursTipId", "long");

            grid.AddDataColumns("AktifAdi", "string");
            grid.AddDataColumns("WebGorunsunAdi", "string");
            grid.AddDataColumns("BursTipAdi", "string");

            grid.AddColumn("Burs Adı", "BursTipAdi", "150px", jqcolumntype.textbox, "",null,"",false);
            grid.AddColumn("Aktarım Ösym Kodu", "AktarimOsymKodu", "150px", jqcolumntype.textbox, "");
            grid.AddColumn("Aktarım Birim Kodu", "AktarimBirimKodu", "150px", jqcolumntype.textbox, "");
            grid.AddColumn("Bildirim Öysm Kodu", "BildirimOysmKodu", "159px", jqcolumntype.textbox, "");
            grid.AddColumn("Bildirim Birim Kodu", "BildirimBirimKodu", "150px", jqcolumntype.textbox, "");
            grid.AddColumn("Aktif Mi?", "AktifId", "100px", jqcolumntype.dropdownlist, "AktifAdi", (q.viewData["EvetHayir"] as vw_TipUst[]).Select(t=>new { id=t.TipId,label=t.Ad }).ToArray().getJsonArray());
            grid.Ozellikler.height = "220px";
            grid.Ozellikler.disabled = false;
            grid.Ozellikler.filterMode = jqfilterMode.Advanced;
            grid.Ozellikler.selectionmode = jqselectionmode.singlecell;
            grid.Ozellikler.columnsResize = true;
            grid.Ozellikler.enableHover = true;
            grid.Ozellikler.editable = true; 
            grid.data = model.BirimKodlari.getJsonArray(); 
            q.ajax.html("#BursTanimlariGrid", grid.Olustur()); 
        }

        public void Kaydet()
        {
            var a = q.request["data"];
            vw_ModelBirim model = a.unSerialize<vw_ModelBirim>();


            vw_TipUst tip = model.BirimTipi.Where(t => t.TipId == model.Birim.BirimTipId).FirstOrDefault();
            if (tip != null)
            {
                if (tip.ExtraKod != "program")
                {
                    string idList = string.Join("," ,model.BirimKodlari.Where(t => t.Id > 0).Select(t => t.Id.ToString()).ToArray());
                    if (idList.Length > 0)
                    {
                        try
                        {
                            q.con.ExecuteNonQuery("DELETE FROM br.BirimKodlari WHERE Id IN(" + idList + ")", null);
                        }
                        catch (Exception ex)
                        {
                            q.ajax.alert("Kullanim", "Bursluluklar Kullanımda Silinemiyor");
                            return;
                        }
                    }
                   
                }


                Birim birim = Ent.get<Birim>().Where("Id", model.Birim.Id).FirstOrDefault<Birim>();
                if (birim == null)
                    birim = Ent.get<Birim>().Create<Birim>();

                birim.AktifId = model.Birim.AktifId;
                birim.BirimAdi = model.Birim.BirimAdi;
                birim.BirimAdiGlobal = model.Birim.BirimAdiGlobal;
                birim.BirimKodu = model.Birim.BirimKodu;
                birim.BirimTipId = model.Birim.BirimTipId;
                birim.Siralama = model.Birim.Siralama??"";
                birim.UstBirimId = model.Birim.UstBirimId;
                birim.FakulteId = null;
                if (birim.Save())
                {
                    BirimGenelOzellik ozellik = Ent.get<BirimGenelOzellik>().Where("Id", birim.Id).FirstOrDefault<BirimGenelOzellik>();
                    if (ozellik == null)
                    {
                        ozellik = Ent.get<BirimGenelOzellik>().Create<BirimGenelOzellik>();
                        ozellik.Id = birim.Id;
                    }
                    ozellik.KacYariYil = model.BirimOzellikleri.KacYariYil;
                    ozellik.AzamiYariYil = model.BirimOzellikleri.AzamiYariYil;
                    ozellik.WebGorunsunId = model.BirimOzellikleri.WebGorunsunId;
                    ozellik.HazirlikVarMi = model.BirimOzellikleri.HazirlikVarMi;
                    ozellik.HazirlikModulId = model.BirimOzellikleri.HazirlikModulId;
                    //ozellik.IsyeriId = model.BirimOzellikleri.IsyeriId;
                    ozellik.Save();

                    if (tip.ExtraKod == "program")
                    {
                        for (int i = 0; i < model.BirimKodlari.Length; i++)
                        {
                            BirimKodlari birimKodlari = Ent.get<BirimKodlari>().Where("BirimId", birim.Id).Where("BursTipId", model.BirimKodlari[i].BursTipId).FirstOrDefault<BirimKodlari>();
                            if (birimKodlari == null)
                            {
                                birimKodlari = Ent.get<BirimKodlari>().Create<BirimKodlari>();
                            }
                            birimKodlari.AktarimBirimKodu = model.BirimKodlari[i].AktarimBirimKodu.Length>0? model.BirimKodlari[i].AktarimBirimKodu:null;
                            birimKodlari.AktarimOsymKodu = model.BirimKodlari[i].AktarimOsymKodu.Length > 0 ? model.BirimKodlari[i].AktarimOsymKodu : null;  
                            birimKodlari.AktifId = model.BirimKodlari[i].AktifId;
                            birimKodlari.BildirimBirimKodu = model.BirimKodlari[i].BildirimBirimKodu.Length > 0 ? model.BirimKodlari[i].BildirimBirimKodu : null;
                            birimKodlari.BildirimOysmKodu = model.BirimKodlari[i].BildirimOysmKodu.Length > 0 ? model.BirimKodlari[i].BildirimOysmKodu : null;
                            birimKodlari.WebGorunsunId = model.BirimKodlari[i].WebGorunsunId;
                            birimKodlari.BursTipId = model.BirimKodlari[i].BursTipId;
                            birimKodlari.BirimId = birim.Id;
                            birimKodlari.Save();

                        }
                    }
                    this.load();
                    q.ajax.alert("kaydedildi","Kaydedildi.");
                    q.ajax.TabClose("BirimDuzenleme", "BirimDuzenleme");
                }


            }
        }

    }
}

