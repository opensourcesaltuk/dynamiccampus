﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Transactions;
using System.Web;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json.Linq;

namespace AJAXPMVC
{
    public class sq : System.IDisposable
    {
        // public Mems mem;
        public Microsoft.AspNetCore.Http.HttpContext context;

        public string m(string oNamespace, string oClassname, string funcName, string data = "", mGetPost p = mGetPost.POST,string JavaObjectBtn="null")
        {
            string Token = new TokenFunc() { oClassname = oClassname, oNamespace = oNamespace, funcName = funcName }.toSerialize();


            if (p == mGetPost.POST)
            {
                if (JavaObjectBtn.Length > 0)
                {
                    JavaObjectBtn = ',' + JavaObjectBtn;
                }
                Token = q.sessionaes.AES_encrypt(Token);
                data = string.IsNullOrEmpty(data) ? "" : ("," + data);
                data = @"{token:'" + Token + @"'" + data + @"}";
                Token = "$$.AJAXP.POST(" + data + JavaObjectBtn + ");";
            }
            else
            {

                var xxx = HttpUtility.UrlEncodeUnicode(q.sessionaes.AES_encrypt(Token));
                Token = q.getDomanin + "Main/Index/?ASPSESSID=" + q.session.OturumId + "&token=" + xxx + "&" + data;
            }
            return Token;
        }

        public sq(Microsoft.AspNetCore.Http.HttpContext context)
        {
            this.context = context;
            //this.ProgramLoad(context);

        }


        public void ProgramLoad()
        {
            if (!q.request.ContainsKey("token"))
            {
                /*
                Sayfa Load 
                */

                string sayfa = q.request.Keys.Contains("sayfa") ? q.request["sayfa"].ToString() : "";
                string[] rowPage = sayfa.Split('/');

                Dictionary<string, object> param = new Dictionary<string, object>();
                param.Add("@SayfaAdi", rowPage[0]);

                var row = q.con.CollectionFromSql(@"SELECT ct.Namespaces ,ct.ClassName,a.FuncName,a.Bakim from sis.Ekranlar a WITH(NOLOCK) INNER JOIN sis.Controller ct WITH(NOLOCK) ON a.ControllerId=ct.Id WHERE a.SayfaAdi=@SayfaAdi OR a.Varsayilan=1 ORDER BY a.Varsayilan ASC", param).Select(p => new { Namespaces = (string)p.Namespaces, ClassName = (string)p.ClassName, FuncName = (string)p.FuncName, Bakim = (bool)p.Bakim, }).FirstOrDefault();


                if (row.Bakim)
                {
                    //    Tablo Row = Tablo.get("sis.Ekranlar").Where("SayfaAdi", rowPage[0]); 

                }
                else
                {
                    try
                    { 
                        RunNamespaceClassFunction(row.Namespaces, row.ClassName, row.FuncName);
                    }
                    catch (Exception)
                    {
                         
                    }
                }
            }
            else
            {
                try
                {
                    TokenFunc token = q.sessionaes.AES_decrypt(q.request["token"]).unSerialize<TokenFunc>();
                    RunNamespaceClassFunction(token.oNamespace, token.oClassname, token.funcName);
                }
                catch { }
            }
        }

        public void RunNamespaceClassFunction(string namespaces, string typeName, string methodName)
        {
            if (namespaces.Length > 0)
                typeName = namespaces + "." + typeName;
            Type type = Type.GetType(typeName);
            object instance = Activator.CreateInstance(type);
            MethodInfo method = type.GetMethod(methodName);
            var x= method.GetParameters();
            object[] obj = new object[x.Length];
            for (int i = 0; i < obj.Length; i++)
            {
                obj[i] = x[i].DefaultValue;
            }

            method.Invoke(instance, obj);
        }

        public bool unsetAjax = false;

        #region IDisposable Support
        public bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            conf.GCC++;
         //   if (!unsetAjax)
           //    this.context.Response.WriteAsync(q.ajax.dispose());


            q.session.Dispose();

            try
            {
              //  q.con.Dispose();
            }
            catch
            {
            }


            if (!disposedValue)
            {
                if (disposing)
                {

                    if (conf.GCC % 10 == 0)
                    {
                        GC.SuppressFinalize(this);
                        conf.GCC = 0;
                    }
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }

        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~sq() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        public string GlobalCacheKetCreate(bool kullanici, string Key,string Classname)
        {
            if (kullanici)
               Key= Classname + ":" + Key + ":Kullanici:" + q.userdata.user.Id.ToString()+"_";
            else
                Key = Classname + ":" + Key + "_";
            return Key;
        }

        public JArray getTipData(string sql, string cacheKey,bool Kullanici=false)
        {
            cacheKey = this.GlobalCacheKetCreate(Kullanici, cacheKey, "TipData");
            JArray x = cacheKey.CacheOku<string>().unSerialize<JArray>();
            if (x == null)
            {
                x = q.con.CollectionFromSql(sql).ToArray().getJsonArray();
                if (x.Count > 0) {
                    x.ToString().CacheYaz(cacheKey);

                }
                

            }
            return x;

        }
        public JArray getvwtipGrupListesi(string grupkodu)
        {
           
            return this.getTipData("SELECT a.TipId AS id,a.Ad AS label FROM tpl.vw_TipGrupListesi a WHERE a.GrupKodu='"+grupkodu+"'",grupkodu,false);

        }
        #endregion
    }
}

public class TokenFunc
{
    public string oNamespace, oClassname, funcName;
}