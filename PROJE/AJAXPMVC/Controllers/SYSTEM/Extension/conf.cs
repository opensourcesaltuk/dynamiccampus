﻿using System;
using System.IO;
using System.Web;
using System.Data;
using System.Linq; 
using System.Dynamic;
using System.Reflection; 
using System.Data.Common;
using System.Transactions;
using System.Collections.Generic;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.EntityFrameworkCore.Query;

public static class conf
{
    public static int GCC;

    public static bool IsNumeric(this object s)
    {
        float output;
        return float.TryParse(s.ToString(), out output);
    }
    public static bool IsNull(this JToken s)
    {
        return s.Type == JTokenType.Null;
    }
    public static bool? BoolNullValue(this object s,bool? def=null)
    {
        //if (s == null)
        //    return null??def;
        bool ret = false;
        if (bool.TryParse(s.toNullStringValue(), out ret))
        {
            return ret;
        }
        else
        {
            return def;
        } 
    }
    public static object IsNullValueString(this JToken s)
    {
        return s.Type != JTokenType.Null ? s.ToString() : null;
    }
    public static JObject GetJObject(this string s)
    {
        return JObject.Parse(s);
    }
    public static JArray GetJArray(this string s)
    {
        return JArray.Parse(s);
    }

    public static string GetFormatViewDate(this DateTime time, ViewDateFormat viewDateFormat = ViewDateFormat.Date)
    {
        return GetFormatViewDate((DateTime?)time, viewDateFormat);
    }
    public static string GetFormatViewDate(this DateTime? time, ViewDateFormat viewDateFormat= ViewDateFormat.Date)
    {
        if (time == null)
            return "";
        switch (viewDateFormat)
        {
            case ViewDateFormat.DateTime:
                return ((DateTime)time).ToString("dd.MM.yyyy hh:mm");                
            case ViewDateFormat.Time:

                return ((DateTime)time).ToString("hh:mm");
            case ViewDateFormat.Date:

                return ((DateTime)time).ToString("dd.MM.yyyy");
        }
        return "";
    }
    public static string StaticMemIp { get; internal set; }

    public static string StaticMemKeyFrist { get; set; }
    public static memsnew MemsCacheOturum { get; set; }
    public static memsnew MemsCache { get; set; }

    public static string toSerialize(this object o)
    { 
        return JsonConvert.SerializeObject(o);
    }
    public static string toNullStringValue(this object o)
    {
        
        if (o!=null && (o.GetType().Name == "JToken" || o.GetType().Name == "JValue"))
        {
            return ((JToken)o).Type == JTokenType.Null ? null : o.ToString();
        }
        return o == null ? null : o.ToString();
    }
    public static string toJavaScriptValue(this string o)
    {

        return System.Web.HttpUtility.JavaScriptStringEncode(o, true);

    }
    public static decimal? toNullGetDecimalValue(this object o)
    {
        if (o == null || o.IsNumeric()==false)
            return null;
        return decimal.Parse(o.ToString()); 
    }
    public static object deSerialize(this string o)
    {
        return JsonConvert.DeserializeObject(o);
    }
    public static Newtonsoft.Json.Linq.JArray getJsonArray(this object[] s)
    {
        return s.toSerialize().unSerialize<Newtonsoft.Json.Linq.JArray>();
    }
    public static Newtonsoft.Json.Linq.JArray getJsonArray<T>(this List<T> s)
    {
        return s.toSerialize().unSerialize<Newtonsoft.Json.Linq.JArray>();
    }
    public static T unSerialize<T>(this string o)
    {
        try
        {

            T sonuc = JsonConvert.DeserializeObject<T>(o);
            return sonuc;
        }
        catch
        {

            return default(T);
        }
    }
    public static object deSerializeJson(this string o)
    {
        return JsonConvert.DeserializeObject(o, new Newtonsoft.Json.JsonSerializerSettings
        {
          //  TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Objects,
            StringEscapeHandling = Newtonsoft.Json.StringEscapeHandling.EscapeHtml
        });
    }

    public static T unSerializeJson<T>(this string o)
    {
        T sonuc = JsonConvert.DeserializeObject<T>(o, new Newtonsoft.Json.JsonSerializerSettings
        {
          //  TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Objects,
            StringEscapeHandling = Newtonsoft.Json.StringEscapeHandling.EscapeHtml
        });
        return sonuc;
    }
    public static string toSerializeJson(this object o)
    {

        return JsonConvert.SerializeObject(o, Newtonsoft.Json.Formatting.None, new Newtonsoft.Json.JsonSerializerSettings
        {
            //TypeNameHandling = Newtonsoft.Json.TypeNameHandling.Objects,
            StringEscapeHandling = Newtonsoft.Json.StringEscapeHandling.EscapeHtml
        });
    }

    public static string urlEncode(this string link)
    { 
        return HttpUtility.UrlEncodeUnicode(link); 
    }
    public static string urlDecode(this string link)
    {
        return HttpUtility.UrlDecode(link);
    } 
    public static bool IsNumbers(this Type dataType)
    {
        if (dataType == null)
            throw new ArgumentNullException("dataType is null");

        return (dataType == typeof(int)
            || dataType == typeof(double)
            || dataType == typeof(long)
            || dataType == typeof(short)
            || dataType == typeof(float)
            || dataType == typeof(Int16)
            || dataType == typeof(Int32)
            || dataType == typeof(Int64)
            || dataType == typeof(uint)
            || dataType == typeof(UInt16)
            || dataType == typeof(UInt32)
            || dataType == typeof(UInt64)
            || dataType == typeof(sbyte)
            || dataType == typeof(Single));
    }

    public static bool IsDateTime(this Type dataType)
    {
        if (dataType == null)
            throw new ArgumentNullException("dataType is null");

        return (dataType == typeof(DateTime));
    }
    public static DataTable DataTable(this DbContext context, string sqlQuery, Dictionary<string, Object> parameters=null)
    {

       if (context.Database.GetDbConnection().State != ConnectionState.Open) context.Database.GetDbConnection().Open();
        DbProviderFactory dbFactory = DbProviderFactories.GetFactory(context.Database.GetDbConnection());

        using (var cmd = dbFactory.CreateCommand())
        { 
            cmd.Connection = context.Database.GetDbConnection();
            cmd.CommandType = CommandType.Text;
            if (parameters != null)
                foreach (var pr in parameters)
                {
                    var p = cmd.CreateParameter();
                    p.ParameterName = pr.Key;
                    p.Value = pr.Value;
                    cmd.Parameters.Add(p);
                }
            cmd.CommandText = sqlQuery;
            using (DbDataAdapter adapter = dbFactory.CreateDataAdapter())
            { 
                adapter.SelectCommand = cmd; 
                DataTable dt = new DataTable();
                adapter.Fill(dt);                 
                return dt;
            }
        }
    }


    public static IEnumerable<dynamic> CollectionFromSql(this DbContext dbContext, string tsql, Dictionary<string, object> parameters = null)
    {

        using (var cmd = dbContext.Database.GetDbConnection().CreateCommand())
        {
            cmd.CommandText = tsql;

            if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();

            if (parameters != null)
                foreach (KeyValuePair<string, object> param in parameters)
                {
                    DbParameter dbParameter = cmd.CreateParameter();
                    dbParameter.ParameterName = param.Key;
                    dbParameter.Value = param.Value;
                    cmd.Parameters.Add(dbParameter);
                }

            using (var dataReader = cmd.ExecuteReader())
            {
                while (dataReader.Read())
                {
                    var dataRow = GetDataRow(dataReader);
                    yield return dataRow;
                }
            }
        }

    }
   
    public static IEnumerable<Dictionary<string, object>> CollectionFromSqlDataReader(this DbContext dbContext, string tsql, Dictionary<string, object> parameters = null)
    {
        using (var cmd = dbContext.Database.GetDbConnection().CreateCommand())
        {
            cmd.CommandText = tsql;

            if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();

            if (parameters != null)
                foreach (KeyValuePair<string, object> param in parameters)
                {
                    DbParameter dbParameter = cmd.CreateParameter();
                    dbParameter.ParameterName = param.Key;
                    dbParameter.Value = param.Value;
                    cmd.Parameters.Add(dbParameter);
                }

            using (var dataReader = cmd.ExecuteReader())
            {
                while (dataReader.Read())
                {
                    yield return GetDataRowDic(dataReader);
                }
            }
        }
    }
    public static DataSet getSqlDataSet(this DbContext dbContext, string tsql, Dictionary<string, object> parameters = null, CommandType commandType=CommandType.Text)
    {
        DataSet ret = new DataSet();
        using (var cmd = dbContext.Database.GetDbConnection().CreateCommand())
        {
            cmd.CommandText = tsql;
            //cmd.CommandText = "EXEC brc.OTS_GET_BORC";
            //cmd.CommandText = "SELECT 1 as a;";

            if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();

            if (parameters != null)
                foreach (KeyValuePair<string, object> param in parameters)
                {
                    DbParameter dbParameter = cmd.CreateParameter();
                    dbParameter.ParameterName = param.Key;
                    dbParameter.Value = param.Value;
                    cmd.Parameters.Add(dbParameter);
                }


            //cmd.CommandType = commandType;

            SqlDataAdapter da = new SqlDataAdapter();
            da.SelectCommand = cmd as SqlCommand;
           
            da.Fill(ret);
        }
        return ret;
    }
    private static Dictionary<string, object> GetDataRowDic(DbDataReader dataReader)
    {
        var dataRow = new Dictionary<string, object>();
        for (var fieldCount = 0; fieldCount < dataReader.FieldCount; fieldCount++)
            dataRow.Add(dataReader.GetName(fieldCount), dataReader[fieldCount]);
        return dataRow;
    }

    public static string GetDescription(this Enum value)
    {
        FieldInfo fi = value.GetType().GetField(value.ToString());
        var attributes = fi.CustomAttributes.FirstOrDefault();

        if (attributes.ConstructorArguments.FirstOrDefault() != null)
        {
            return attributes.ConstructorArguments.FirstOrDefault().Value.ToString();
        }
        else
        {
            return value.ToString();
        }
    }
    public static void ParameterValueForSQL(string data, object value)
    {
        //var builder = new SqlBuilder();




        /*
        string retval = "";

        switch (sp.DbType)
        {
            case DbType.Time:
            case DbType.String:
            case DbType.AnsiString:
            case DbType.Xml:
            case DbType.Date:
            case DbType.DateTime:
            case DbType.DateTime2:
            case DbType.DateTimeOffset:
                if (sp.Value == DBNull.Value || sp.Value==null)
                {
                    retval = "NULL";
                }
                else
                {
                    retval = "'" + sp.Value.ToString().Replace("'", "''") + "'";
                }
                break;

            case DbType.Boolean:
                if (sp.Value == DBNull.Value || sp.Value == null)
                {
                    retval = "NULL";
                }
                else
                {
                    retval = ((bool)sp.Value == false) ? "0" : "1";
                }
                break;

            default:
                if (sp.Value == DBNull.Value || sp.Value == null)
                {
                    retval = "NULL";
                }
                else
                {
                    retval = sp.Value.ToString().Replace("'", "''");
                }
                break;
        }
        sp.Value = retval;
        return sp;*/
    }
   

    public class DbRet
    {
#pragma warning disable CS0169 // The field 'conf.DbRet.key' is never used
#pragma warning disable CS0169 // The field 'conf.DbRet.value' is never used
        string key, value;
#pragma warning restore CS0169 // The field 'conf.DbRet.value' is never used
#pragma warning restore CS0169 // The field 'conf.DbRet.key' is never used
    }

    public static object LastInsertId(this DbContext dbContext)
    {
        try
        {
           var ddd= dbContext.CollectionFromSql("SELECT NewID = SCOPE_IDENTITY();").FirstOrDefault();
            return long.Parse(ddd);
            //return dbContext.Database.SqlQuery<long?>("SELECT NewID = SCOPE_IDENTITY();").FirstOrDefault();

        }
        catch (Exception ex)
        {
            return 0;
        }
    }

    public static TransactionScope TransactionSql(this DbContext dbContext)
    {
        return new TransactionScope();
    }
    
 
 

    public static bool ExecuteNonQuery(this DbContext dbContext, string Sql, Dictionary<string, object> Parameters = null)
    {
     
        using (var cmd = dbContext.Database.GetDbConnection().CreateCommand())
        {

            cmd.CommandText = Sql;

            if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();

       
            
            if (Parameters != null)
                foreach (KeyValuePair<string, object> param in Parameters)
                {
                    DbParameter dbParameter = cmd.CreateParameter();
                    dbParameter.ParameterName = param.Key;
                    dbParameter.Value = param.Value ?? DBNull.Value;
                    //dbParameter.ResetDbType();// = param.Value;
                    cmd.Parameters.Add(dbParameter);
                }

            if (cmd.ExecuteNonQuery() > 0)
            {       
                return true;
            } 


            return false;
        }
    }

    private static dynamic GetDataRow(DbDataReader dataReader)
    {
        var dataRow = new ExpandoObject() as IDictionary<string, object>;
        for (var fieldCount = 0; fieldCount < dataReader.FieldCount; fieldCount++)
            dataRow.Add(dataReader.GetName(fieldCount), dataReader[fieldCount]==DBNull.Value?null: dataReader[fieldCount]);
        return dataRow;
    }

    public static T getEntities<T>(bool locked = false, bool lazyLoading = false, string connectionString = "", bool sql = true)
    {
        T ent = (T)Activator.CreateInstance(typeof(T));
        if (sql)
        {
           // (ent as DbContext).Configuration.LazyLoadingEnabled = lazyLoading;
            if (locked == false)
                (ent as DbContext).Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;");

            //DbInterception.Add(new NoLockInterceptor());
        }
        return ent;
    }

    internal static string getIp()
    {

        string ipAddress = AJAXPMVC.q.getContext.Connection.RemoteIpAddress.ToString();//  HttpContext.Current.Request. ["HTTP_X_FORWARDED_FOR"];

        if (!string.IsNullOrEmpty(ipAddress))
        {
            string[] addresses = ipAddress.Split(',');
            if (addresses.Length != 0)
            {
                return addresses[0];
            }
        }

        return ipAddress;// context.Request.ServerVariables["REMOTE_ADDR"];
    }
}

public static class IQueryableExtensions
{
    private static readonly TypeInfo QueryCompilerTypeInfo = typeof(QueryCompiler).GetTypeInfo();

    private static readonly FieldInfo QueryCompilerField = typeof(EntityQueryProvider).GetTypeInfo().DeclaredFields.First(x => x.Name == "_queryCompiler");

    private static readonly FieldInfo QueryModelGeneratorField = QueryCompilerTypeInfo.DeclaredFields.First(x => x.Name == "_queryModelGenerator");

    private static readonly FieldInfo DataBaseField = QueryCompilerTypeInfo.DeclaredFields.Single(x => x.Name == "_database");

    private static readonly PropertyInfo DatabaseDependenciesField = typeof(Database).GetTypeInfo().DeclaredProperties.Single(x => x.Name == "Dependencies");

    public static string ToSqld<TEntity>(this IQueryable<TEntity> query) where TEntity : class
    {
        var queryCompiler = (QueryCompiler)QueryCompilerField.GetValue(query.Provider);
        var modelGenerator = (QueryModelGenerator)QueryModelGeneratorField.GetValue(queryCompiler);
        var queryModel = modelGenerator.ParseQuery(query.Expression);
        var database = (IDatabase)DataBaseField.GetValue(queryCompiler);
        var databaseDependencies = (DatabaseDependencies)DatabaseDependenciesField.GetValue(database);
        var queryCompilationContext = databaseDependencies.QueryCompilationContextFactory.Create(false);
        var modelVisitor = (RelationalQueryModelVisitor)queryCompilationContext.CreateQueryModelVisitor();
        modelVisitor.CreateQueryExecutor<TEntity>(queryModel);
        var sql = modelVisitor.Queries.First().ToString();

        return sql;
    }
}

public class NoLockInterceptor //: IDbInterceptor
{
    public bool ApplyNoLock = true;
}

public enum ViewDateFormat
{
    Date = 1,
    DateTime = 2,
    Time = 3
}



public interface IViewRenderService
{
    Task<string> RenderToStringAsync(string viewName);
    Task<string> RenderToStringAsync<TModel>(string viewName, TModel model);
    string RenderToString<TModel>(string viewPath, TModel model);
    string RenderToString(string viewPath);
}

public class ViewRenderService : IViewRenderService
{
    private readonly IRazorViewEngine _viewEngine;
    private readonly ITempDataProvider _tempDataProvider;
    private readonly IServiceProvider _serviceProvider;
    private readonly IHttpContextAccessor _httpContextAccessor;

    public ViewRenderService(IRazorViewEngine viewEngine, IHttpContextAccessor httpContextAccessor,
        ITempDataProvider tempDataProvider,
        IServiceProvider serviceProvider)
    {
        _viewEngine = viewEngine;
        _httpContextAccessor = httpContextAccessor;
        _tempDataProvider = tempDataProvider;
        _serviceProvider = serviceProvider;
    }

    public string RenderToString<TModel>(string viewPath, TModel model)
    {
        try
        {
            var viewEngineResult = _viewEngine.GetView("~/", viewPath, false);

            if (!viewEngineResult.Success)
            {
                throw new InvalidOperationException($"Couldn't find view {viewPath}");
            }

            var view = viewEngineResult.View;

            using (var sw = new StringWriter())
            {
                var viewContext = new ViewContext()
                {
                    HttpContext = _httpContextAccessor.HttpContext ?? new DefaultHttpContext { RequestServices = _serviceProvider },
                    ViewData = new ViewDataDictionary<TModel>(new EmptyModelMetadataProvider(), new ModelStateDictionary()) { Model = model },
                    Writer = sw
                };
                view.RenderAsync(viewContext).GetAwaiter().GetResult();
                return sw.ToString();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error ending email.", ex);
        }
    }

    public async Task<string> RenderToStringAsync<TModel>(string viewName, TModel model)
    {
        var httpContext = _httpContextAccessor.HttpContext ?? new DefaultHttpContext { RequestServices = _serviceProvider };
        var actionContext = new ActionContext(httpContext, new RouteData(), new ActionDescriptor());

        using (var sw = new StringWriter())
        {
            var viewResult = _viewEngine.FindView(actionContext, viewName, false);

            // Fallback - the above seems to consistently return null when using the EmbeddedFileProvider
            if (viewResult.View == null)
            {
                viewResult = _viewEngine.GetView("~/", viewName, false);
            }

            if (viewResult.View == null)
            {
                throw new ArgumentNullException($"{viewName} does not match any available view");
            }

            var viewDictionary = new ViewDataDictionary(new EmptyModelMetadataProvider(), new ModelStateDictionary())
            {
                Model = model
            };

            var viewContext = new ViewContext(
                actionContext,
                viewResult.View,
                viewDictionary,
                new TempDataDictionary(actionContext.HttpContext, _tempDataProvider),
                sw,
                new HtmlHelperOptions()
            );

            await viewResult.View.RenderAsync(viewContext);
            return sw.ToString();
        }
    }

    public string RenderToString(string viewPath)
    {
        return RenderToString(viewPath, string.Empty);
    }

    public Task<string> RenderToStringAsync(string viewName)
    {
        return RenderToStringAsync<string>(viewName, string.Empty);
    }
}