﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AJAXPMVC.Ents
{
    [TableAttribute(Scheme = "Ogr.")]
    public class vw_OgrenciBilgileri : Ent
    {
        [TableAttribute(Primary = true, Identity = false)]
        public long KullaniciId { get; set; }
        public string KimlikNo { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Cinsiyet { get; set; }
        public string CinsiyetAdi { get; set; }
        public long? DilId { get; set; }
        public string DilAdi { get; set; }
        public string DilKodu { get; set; }
        public long OgrenciId { get; set; }
        public long? IlkGelisTipiId { get; set; }
        public string IlkGelisTipiAdi { get; set; }
        public string OgrFoto { get; set; }
        public long? YoksisNo { get; set; }
        public Guid GId { get; set; }
        public long? OgrenciBolumId { get; set; }
        public long? DalturuId { get; set; }
        public string DalturuAdi { get; set; }
        public long? AkademikBirimId { get; set; }
        public long? AkademikBirimUstBirimId { get; set; }
        public long? AkademikBirimTipId { get; set; }
        public string AkademikBirimTipAdi { get; set; }
        public string AkademikBirimTipKodu { get; set; }
        public string AkademikBirimAdi { get; set; }
        public string AkademikBirimAdiGlobal { get; set; }
        public string AkademikBirimSiralama { get; set; }
        public long? AkademikBirimAktifId { get; set; }
        public string AkademikBirimAktifAdi { get; set; }
        public string AkademikBirimBirimKodu { get; set; }
        public long? ProgramId { get; set; }
        public long? ProgramUstBirimId { get; set; }
        public long? ProgramTipId { get; set; }
        public string ProgramTipAdi { get; set; }
        public string ProgramTipKodu { get; set; }
        public string ProgramAdi { get; set; }
        public string ProgramAdiGlobal { get; set; }
        public string ProgramSiralama { get; set; }
        public long? ProgramAktifId { get; set; }
        public string ProgramAktifAdi { get; set; }
        public string ProgramBirimKodu { get; set; }
        public long? BirimKodlariId { get; set; }
        public string BildirimOysmKodu { get; set; }
        public string BildirimBirimKodu { get; set; }
        public long? BursTipId { get; set; }
        public string BursAdi { get; set; }
        public decimal? BursOran { get; set; }
        public bool AktifMi { get; set; }
        public long? KayitTipiId { get; set; }
        public string KayitTipiAdi { get; set; }
        public DateTime? KayitTarihi { get; set; }
        public DateTime? KesinKayitTarihi { get; set; }
        public int? DiplomaNo { get; set; }
        public string OgrenciNumarasi { get; set; }
        public long? KayitYilId { get; set; }
        public DateTime? KayitYilBasTarih { get; set; }
        public DateTime? KayitYilBitTarih { get; set; }
        public string KayitYilAdi { get; set; }
        public long? MaliYilId { get; set; }
        public DateTime? MaliYilBasTarih { get; set; }
        public DateTime? MaliYilBitTarih { get; set; }
        public string MaliYilAdi { get; set; }
        public long? EgitimYilId { get; set; }
        public DateTime? EgitimYilBasTarih { get; set; }
        public DateTime? EgitimYilBitTarih { get; set; }
        public string EgitimYilAdi { get; set; }
        public string Sube { get; set; }
        public bool? OzelOgrenciMi { get; set; }
    }



    [TableAttribute(Scheme = "Ogr.")]
    public class vw_Kullanici_OgrenciBilgileri : Ent
    {
        [TableAttribute(Primary = true, Identity = false)]
        public long KullaniciId { get; set; }
        public string KimlikNo { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Cinsiyet { get; set; }
        public string CinsiyetAdi { get; set; }
        public long? DilId { get; set; }
        public string DilAdi { get; set; }
        public string DilKodu { get; set; }
        public long? OgrenciId { get; set; }
        public long? IlkGelisTipiId { get; set; }
        public string IlkGelisTipiAdi { get; set; }
        public string OgrFoto { get; set; }
        public long? YoksisNo { get; set; }
        public Guid? GId { get; set; }
        public long? OgrenciBolumId { get; set; }
        public long? DalturuId { get; set; }
        public string DalturuAdi { get; set; }
        public long? AkademikBirimId { get; set; }
        public long? AkademikBirimUstBirimId { get; set; }
        public long? AkademikBirimTipId { get; set; }
        public string AkademikBirimTipAdi { get; set; }
        public string AkademikBirimTipKodu { get; set; }
        public string AkademikBirimAdi { get; set; }
        public string AkademikBirimAdiGlobal { get; set; }
        public string AkademikBirimSiralama { get; set; }
        public long? AkademikBirimAktifId { get; set; }
        public string AkademikBirimAktifAdi { get; set; }
        public string AkademikBirimBirimKodu { get; set; }
        public long? ProgramId { get; set; }
        public long? ProgramUstBirimId { get; set; }
        public long? ProgramTipId { get; set; }
        public string ProgramTipAdi { get; set; }
        public string ProgramTipKodu { get; set; }
        public string ProgramAdi { get; set; }
        public string ProgramAdiGlobal { get; set; }
        public string ProgramSiralama { get; set; }
        public long? ProgramAktifId { get; set; }
        public string ProgramAktifAdi { get; set; }
        public string ProgramBirimKodu { get; set; }
        public long? BirimKodlariId { get; set; }
        public string BildirimOysmKodu { get; set; }
        public string BildirimBirimKodu { get; set; }
        public long? BursTipId { get; set; }
        public string BursAdi { get; set; }
        public decimal? BursOran { get; set; }
        public bool AktifMi { get; set; }
        public long? KayitTipiId { get; set; }
        public string KayitTipiAdi { get; set; }
        public DateTime? KayitTarihi { get; set; }
        public DateTime? KesinKayitTarihi { get; set; }
        public int? DiplomaNo { get; set; }
        public string OgrenciNumarasi { get; set; }
        public long? KayitYilId { get; set; }
        public DateTime? KayitYilBasTarih { get; set; }
        public DateTime? KayitYilBitTarih { get; set; }
        public string KayitYilAdi { get; set; }
        public long? MaliYilId { get; set; }
        public DateTime? MaliYilBasTarih { get; set; }
        public DateTime? MaliYilBitTarih { get; set; }
        public string MaliYilAdi { get; set; }
        public long? EgitimYilId { get; set; }
        public DateTime? EgitimYilBasTarih { get; set; }
        public DateTime? EgitimYilBitTarih { get; set; }
        public string EgitimYilAdi { get; set; }
        public string Sube { get; set; }
        public bool? OzelOgrenciMi { get; set; }
        public bool? OgrenciMi { get; set; }
    }


    [TableAttribute(Scheme = "Ogr.")]
    public class vw_Kullanici_OgrenciBilgileri_GroupList : vw_Kullanici_OgrenciBilgileri
    {
        public vw_Kullanici_OgrenciBilgileri[] Bolumler;
    }
    [TableAttribute(Scheme = "ogr.")]
    public class Ogrenci : Ent
    {
        [TableAttribute(Primary = true, Identity = false)]
        public long Id { get; set; }
        public long? IlkGelisTipiId { get; set; }
        public string OgrFoto { get; set; }
        public long? YoksisNo { get; set; }
        public Guid GId { get; set; }
    }

    [TableAttribute(Scheme = "ogr.")]
    public class OgrenciDonemKayit : Ent
    {
        [TableAttribute(Primary = true, Identity = true)]
        public long Id { get; set; }
        public long DonemId { get; set; }
        public long OgrenciBolumId { get; set; }
        public long KayitTipiId { get; set; }
        public long SinifId { get; set; }
        public long YariYilId { get; set; }
        public long? HazirlikId { get; set; }
        public long? IstegeBagliHazirlikId { get; set; }
        public long? GittigiUniversiteId { get; set; }
        public long? GeldigiUniversiteId { get; set; }
    }


    [TableAttribute(Scheme = "ogr.")]
    public class vw_OgrenciDonemKayit : Ent
    {
        [TableAttribute(Primary = true, Identity = false)]
        public long Id { get; set; }
        public long DonemId { get; set; }
        public string DonemAdi { get; set; }
        public long OgrenciBolumId { get; set; }
        public long KayitTipiId { get; set; }
        public string KayitTipiAdi { get; set; }
        public long SinifId { get; set; }
        public int? Sinif { get; set; }
        public string SinifAdi { get; set; }
        public string SinifAciklama { get; set; }
        public string YoksisKodu { get; set; }
        public long? YariYilId { get; set; }
        public string YariYilAdi { get; set; }
        public long? HazirlikId { get; set; }
        public string HazirlikAdi { get; set; }
        public long? IstegeBagliHazirlikId { get; set; }
        public string IstegeBagliHazirlikAdi { get; set; }
        public long? GittigiUniversiteId { get; set; }
        public string GittigiUniversiteAdi { get; set; }
        public long? GeldigiUniversiteId { get; set; }
        public string GeldigiUniversiteAdi { get; set; }

    }

}