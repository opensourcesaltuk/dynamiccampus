﻿using System;
using System.Collections.Generic;

namespace AJAXPMVC
{
    public class AJAX
    {
        public Dictionary<string, object> java = new Dictionary<string, object>();

        public AJAX()
        {

        }

        public string html(string selector, string html)
        {
            string jsc = "$(" + selector.toSerialize() + ").html("+html.toSerialize()+")";
            this.java.Add(Guid.NewGuid().ToString(), jsc);
            return jsc;
        }     
           
        public string alert(string key, string mesaj, bool rjs = false)
        {
            string msj = "alert(" +  System.Web.HttpUtility.JavaScriptStringEncode( mesaj,true)+");";
            if (rjs)
                return msj;
            else
                this.java.Add(key, msj);
            return msj;
        }

        public string set_combobox(string key,string Event, string id,bool rjs = false,string ObjectId=null,string ObjectText=null, string javafnc = "null",string placeHorder="")
        {
            string m = q.sq.m("AJAXPMVC", "AutoCopleteBox", "CompleteBoxList", "event="+
                
              System.Web.HttpUtility.UrlEncode( q.sessionaes.AES_encrypt(Event))
                
                , mGetPost.GET);

            Newtonsoft.Json.Linq.JObject ob = null;
            if (ObjectId != null || ObjectText != null)
            {
                ob = new Newtonsoft.Json.Linq.JObject();
                ob.Add("id", ObjectId);
                ob.Add("text", ObjectText);
            } 


            string msj = "$$.set_combobox(" 
                + System.Web.HttpUtility.JavaScriptStringEncode(id, true) + "," 
                + System.Web.HttpUtility.JavaScriptStringEncode(m, true) + ","
                + javafnc + ","
                + ob.toSerializeJson() + ",true" + ","
                + System.Web.HttpUtility.JavaScriptStringEncode(placeHorder, true)+"); ";
            if (rjs)
                return msj;
            else
                this.java.Add(key, msj);
            return msj;
        }
       

        public string AddFnc(string key, string Komut, bool rjs = false)
        {
            string msj = Komut;
            if (rjs)
                return msj;
            else
                this.java.Add(key, msj);
            return msj;
        }

        public string newTabId(string key, string id,string tabName, bool rjs = false)
        {
            string msj = "$$.MainTab.add(false,'" + System.Web.HttpUtility.JavaScriptStringEncode(id, false) + "','" + System.Web.HttpUtility.JavaScriptStringEncode(tabName, false) + "');";
            if (rjs)
                return msj;
            else
                this.java.Add(key, msj);
            return msj;
        }
        public bool WriteTextIsResponse { get; set; } = false;
        public string WriteTextResponseBody { get; set; } = "";
        public void WriteText(string v)
        {
            this.WriteTextIsResponse = true;
            this.WriteTextResponseBody = v;
        }

        public string dialogOpen(string key, string id, string title, string html, ViewDialogOzellikler viewDialogOzellikler = null, bool rjs = false)
        {
            //viewDialogOzellikler = viewDialogOzellikler==null?new ViewDialogOzellikler():viewDialogOzellikler;
            string msj = "$$.dialog.CreateDialog('" + System.Web.HttpUtility.JavaScriptStringEncode(id, false) + "','" + System.Web.HttpUtility.JavaScriptStringEncode(title, false) + "','" + System.Web.HttpUtility.JavaScriptStringEncode(html, false) + "'," + viewDialogOzellikler.toSerializeJson() + ");";
            if (rjs)
                return msj;
            else
                this.java.Add(key, msj);
            return msj;
        }
        public string dialogClose(string key, string id, bool rjs = false)
        {
            string msj = "$$.dialog.deleteDialog('" + System.Web.HttpUtility.JavaScriptStringEncode(id, false) + "');";
            if (rjs)
                return msj;
            else
                this.java.Add(key, msj);
            return msj;
        }
        public string TabClose(string key, string id, bool rjs = false)
        {
            string msj = "$$.MainTab.close(" + System.Web.HttpUtility.JavaScriptStringEncode(id, true) + ");";
            if (rjs)
                return msj;
            else
                this.java.Add(key, msj);
            return msj;
        }
        public Newtonsoft.Json.Linq.JObject reArray()
        {
            Newtonsoft.Json.Linq.JObject rearray = new Newtonsoft.Json.Linq.JObject();
            
            foreach (KeyValuePair<string, object> pair in this.java)
            {
                //ar.Add(System.Web.HttpUtility.UrlEncode( pair.Value.ToString()));
                //rearray[i] = ((pair.Value.ToString()));
                if (pair.Value == null)
                {
                    rearray[pair.Key] = null;
                }
                else
                {
                    rearray[pair.Key] = pair.Value.ToString();

                   // rearray.Add(pair.Key, pair.Value as Newtonsoft.Json.Linq.JArray);
                }

            }
            return rearray;
        }

        public string dispose()
        {
            if (this.WriteTextIsResponse)
                return this.WriteTextResponseBody;
            return this.reArray().toSerializeJson();
        }

       
    }


    public class ViewDialogOzellikler
    {
        public string keyboardCloseKey { get; set; } = "999";
        public bool keyboardNavigation { get; set; } = false; 
    }
}