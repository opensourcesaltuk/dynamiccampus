﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc; 

namespace AJAXPMVC.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {

            using (sq c = q.sq)
            {
                c.unsetAjax = true;
                ViewBag.SessionId = q.session.OturumId;
                ViewBag.selam = q.sessionaes.AES_encrypt("selam");
                q.viewData["unload"] = q.sq.m("AJAXPMVC", "LoginController", "unload", "data:data");
                c.ProgramLoad();
                ViewBag.returns = q.ajax.dispose();
                return View();
            } 
        }  
    }
}
