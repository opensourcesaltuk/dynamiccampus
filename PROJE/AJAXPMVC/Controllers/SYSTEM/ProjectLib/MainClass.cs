﻿using AJAXPMVC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public class MainClass
{

    public string m(string FuncName, string data = "", mGetPost p = mGetPost.POST)
    {
        return q.sq.m(oNamespace, oClassname, FuncName, data, p);
    }

    public string viewTabDivId { get { return q.request.ContainsKey("TabDivId") ? q.request["TabDivId"] : ""; } set { q.request["TabDivId"] = value;  } }
    public string this[string name]
    {
        get
        {
            return q.session[oNamespace + ":" + oClassname + ":" + name];
        }
        set
        {
            q.session[oNamespace + ":" + oClassname + ":" + name] = value;
        }
    }
    public string getPageHtml(string v)
    {
        v = "~/Views/" + oTema + "/" + v + ".cshtml";
        return q.viewRenderSvc.RenderToString(v);
    }
    #region Main Class Static Variyable
    public string oNamespace
    {
        get
        {
            //return this.GetType().GetField("NameSpace").GetValue(this).ToString();
            return this.GetType().Namespace;
        }
    }
    public string oTema
    {
        get
        {
            try
            {

                return this.GetType().GetField("oTema").GetValue(this).ToString();
            }
            catch
            {
                throw new Exception(oClassname + " içinde oTema string değişkeni yok");
            }
        }
    }
    public string oClassname
    {
        get
        {
            return this.GetType().Name;
        }
    }
    #endregion
}

public enum mGetPost
{
    GET,
    POST
}