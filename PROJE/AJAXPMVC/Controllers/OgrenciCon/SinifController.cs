﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;

namespace AJAXPMVC
{
    public class SinifController : MainClass
    {
        public new string oTema = "SinifController";
        string mTabId = "OgrenciListesi";
        public void SinifDuzenleme(long id)
        {
            FormModelGenerator frm = new FormModelGenerator();

            Ents.vw_OgrenciBilgileri ogr = OgrenciBolumKayitController.GetVw_OgrenciBilgileri(id);

            string BilgiBas= "#"+frm.FormEkleDuzenle("OgrenciBilgi","");



            frm.FormEkleDuzenle("SinifBelirleme",    "Sınıf Bilgileri Girişi");



            string cacheKey = "DonemSinifView";
            frm.EkleDropDownList("SinifBelirleme", "DonemId", "Dönem Seçiniz", "","", q.sq.getTipData(@"
                SELECT a.DonemId id,a.DonemAdi label FROM tpl.vw_Donem a WHERE a.DonemTipiKodu!='yaz' ORDER BY a.DonemBaslangicTarihi DESC
            ", cacheKey, false), true);

             cacheKey = "Sinif" + ogr.ProgramId.ToString();
            string SinifId= "#"+frm.EkleDropDownList("SinifBelirleme", "SinifId", "Sınıf Seçiniz", "","", q.sq.getTipData(@"
                SELECT  
                        snf.Id id, snf.SinifAdi label, snf.Sinif Ext1
                FROM br.vw_FakulteProgramListesi a
                    INNER JOIN ogr.vw_Sinif snf
                             ON(a.AkademikBirimTipKodu = 'enstitu' AND snf.Sinif > 7) OR(a.AkademikBirimTipKodu = 'fakulte' AND snf.Sinif < 8)
                WHERE a.ProgramId IN("+ ogr.ProgramId.ToString() + @");
            ", cacheKey, false), true);



            cacheKey = "YariYilId" + ogr.ProgramId.ToString();
            frm.EkleDropDownList("SinifBelirleme", "YariYilId", "Yarıyıl Seçiniz", "","", q.sq.getTipData(@"
                SELECT yr.TipId id,yr.Ad AS label 
                FROM br.vw_FakulteProgramListesi a
                    INNER JOIN tpl.vw_TipGrupListesi yr
                        ON yr.GrupKodu = 'yariyil' AND yr.BasDec <= a.KacYariYil
                    WHERE a.ProgramId IN (" + ogr.ProgramId.ToString() + @");
            ", cacheKey, false), true);

            string HazirlikForm = "#"+ frm.FormEkleDuzenle("HazirlikDetay", "Hazırlık Bilgileri");






            frm.EkleDropDownList("HazirlikDetay", "HazirlikId", "Hazirlik Modülü", "","", q.sq.getvwtipGrupListesi("hazirlikmodul"), false);

            frm.EkleDropDownList("HazirlikDetay", "IstegeBagliHazirlikId", "İsteğe Bağlı Mı?", "", "", q.sq.getvwtipGrupListesi("evethayir"), false);


            // frm.EkleDropDownList("vw_Kullanici_OgrenciBilgileri",   "Cinsiyet", "Cinsiyet", null, "",q.sq.getTipData, false);






            frm.FormEkleDuzenle("SinifKaydetUstForm", "");


            string Id = frm.FormEkleDuzenle("SinifBelirleme_list", "Öğrenci Arama Listesi Sonucu");
            Id += " #Nesneler";
            Id = "#" + Id;
            frm.EkleButton("SinifKaydetUstForm", this.oNamespace, this.oClassname, "SinifKaydet", "data","OgrenciBolumId:"+id.ToString()+", ListeId:'"+Id+"'", "Kaydet", ViewBtnClass.btn_success,fa_icons.fa_fa_save);


          


            q.ajax.newTabId("SinifDuzenlemeTab", "SinifDuzenlemeTab", "Sınıf Düzenleme");
            q.ajax.html("#SinifDuzenlemeTab", frm.Olustur());


            this.ListeYenile(Id,id);


            q.ajax.AddFnc("SinifDegisimi", @"  $('"+ SinifId + @"').bind('change',function(){   if($$.get_dropdownlist($('" + SinifId + @"')).originalItem.Ext1==0){ $('"+ HazirlikForm + "').show(); } else { $('" + HazirlikForm + "').hide();  }  });   ");
            OgrenciBolumKayitController.GetOgrenciBilgileri(id, false, BilgiBas);

        }
        public void SinifKaydet()
        {



            var Sinif = q.request["data"].GetJObject();
            var SinifBelirleme = Sinif["SinifBelirleme"];
            var HazirlikDetay = Sinif["HazirlikDetay"];


            int SinifNo = q.con.CollectionFromSql("SELECT a.Sinif FROM ogr.vw_Sinif a WHERE a.Id=" + SinifBelirleme["SinifId"].ToString(), null).First().Sinif;

            if (SinifNo == 0 && (HazirlikDetay["HazirlikId"].IsNull() || HazirlikDetay["IstegeBagliHazirlikId"].IsNull()))
            {

                q.ajax.alert("hata", "Sinif Hazırlık ise Hazırlık Modül Bilgileri Seçilmelidir.");
                return;
            }
            else
            {
                Ent ent = Ent.get("ogr.OgrenciDonemKayit", "Id", true).Where("OgrenciBolumId", q.request["OgrenciBolumId"]).Where("DonemId", SinifBelirleme["DonemId"].toNullStringValue()).FirstOrDefault<Ent>();
                if (ent==null)
                {

                    Ents.vw_OgrenciBilgileri ogr = OgrenciBolumKayitController.GetVw_OgrenciBilgileri(long.Parse(q.request["OgrenciBolumId"]));

                    ent = Ent.get("ogr.OgrenciDonemKayit", "Id", true).Create(); 
                    ent["OgrenciBolumId"] = q.request["OgrenciBolumId"].toNullStringValue(); 
                    ent["DonemId"] = SinifBelirleme["DonemId"].toNullStringValue();
                    ent["KayitTipiId"] = ogr.KayitYilId;

                }
                 
                ent["SinifId"] =SinifBelirleme["SinifId"].toNullStringValue();
                ent["YariYilId"] = SinifBelirleme["YariYilId"].toNullStringValue();
                ent["HazirlikId"] = HazirlikDetay["HazirlikId"].toNullStringValue();
                ent["IstegeBagliHazirlikId"] = HazirlikDetay["IstegeBagliHazirlikId"].toNullStringValue();


                ent.Save();

                this.ListeYenile(q.request["ListeId"], long.Parse(q.request["OgrenciBolumId"]));

            }


        }

        public void ListeYenile(string id,long OgrenciBolumId)
        {
            jqw liste = new jqw(true);

            liste.data = Ent.get<Ents.vw_OgrenciDonemKayit>().Where("OgrenciBolumId", OgrenciBolumId).ToList<Ents.vw_OgrenciDonemKayit>().getJsonArray();
            liste.AddDataColumns("DonemAdi", "string");
            liste.AddColumn("Dönem Adı", "DonemAdi", "200px");
            liste.AddDataColumns("SinifAdi", "string");
            liste.AddColumn("Sınıf Adı", "SinifAdi", "100px");
            liste.AddDataColumns("YariYilAdi", "string");
            liste.AddColumn("Yarıyıl", "YariYilAdi", "100px");
            liste.AddDataColumns("HazirlikAdi", "string");
            liste.AddColumn("Hazırlık Adı", "HazirlikAdi", "200px");
            liste.AddDataColumns("IstegeBagliHazirlikAdi", "string");
            liste.AddColumn("İsteğe Bağli Hazırlık Mı?", "IstegeBagliHazirlikAdi", "200px");

             
            liste.Ozellikler.height = "620px";
            liste.Ozellikler.disabled = false;
            liste.Ozellikler.filterMode = jqfilterMode.Advanced;
            liste.Ozellikler.columnsResize = true;
            liste.Ozellikler.enableHover = true;
            liste.Ozellikler.rowdetails = false;


            q.ajax.html(id, liste.Olustur());
        }



    }
}

