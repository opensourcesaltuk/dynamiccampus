﻿using AJAXPMVC;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web; 

public class OturumNew :IDisposable
{
    public memsnew mems;
    protected string _OturumId = "";
    private double oturumDakika = 60*4;
    public bool unLoad = false;
    public string OturumId
    {
        set
        {
            _OturumId = value;
        }
        get
        {
            return _OturumId;
        }
    }
    Dictionary<string, string> SessionStringCache = new Dictionary<string, string>();
    public OturumNew(memsnew mem, string id)
    {
        this.mems = mem;
        this.mems.setdb(4);
        this.mems.keyFirst = "Oturum:" + id + ":";
        this.OturumId = id;
        string CacheKey = this.mems.keyFirst + "STRINGS:*";

        List<string> keylist = this.mems.cilent.SearchKeys(CacheKey);
        if (keylist.Count > 0)
        {
            this.SessionStringCache = this.mems.cilent.GetAll<string>(keylist).ToDictionary(ent => ent.Key, ent => ent.Value);
        } 
        
        this["OturumSuresi"] = this["OturumSuresi"] ?? DateTime.Now.Ticks.ToString();
    }

    public string this[string name]
    {
        get
        {
            name = "STRINGS:"+name;
            if (this.SessionStringCache.ContainsKey(this.mems.keyFirst + name))
                return this.SessionStringCache[this.mems.keyFirst + name];
            return this.get<string>(name);
        }
        set
        {
            name = "STRINGS:" + name;
            this.set<string>(name, value);
            this.SessionStringCache[this.mems.keyFirst + name] = value;
        }
    }
    public T get<T>(string name)
    {
        T o = this.mems.CacheOku<T>(name);
        return o;
    }
    public bool set<T>(string name, T value)
    {
        if(!this.unLoad)
            return this.mems.CacheYaz<T>(value, name, DateTime.Now.AddMinutes(this.oturumDakika));
        return true;
    }

    #region IDisposable Support
    private bool disposedValue = false; // To detect redundant calls

    protected virtual void Dispose(bool disposing)
    {

        if (!q.session.unLoad)
        {
            q.session.set<UserStatic>("q:UserStatic-userdata", q.userdata);
             

            List<string> keylist = this.mems.cilent.SearchKeys(this.mems.keyFirst+"*");
            DateTime time = DateTime.Now.AddMinutes(this.oturumDakika);
            foreach (string item in keylist)
            {
                this.mems.cilent.ExpireEntryAt(item, time);
            }            
            q.session.mems.cilent.Dispose();
            q.session.mems.cil.Dispose();
        }
         
        if (!disposedValue)
        {
            if (disposing)
            {
                // TODO: dispose managed state (managed objects).
            }

            // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
            // TODO: set large fields to null.

            disposedValue = true;
        }
    }

    // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
    // ~OturumNew() {
    //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
    //   Dispose(false);
    // }

    // This code added to correctly implement the disposable pattern.
    public void Dispose()
    {
        // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        Dispose(true);
        // TODO: uncomment the following line if the finalizer is overridden above.
        // GC.SuppressFinalize(this);
    }
    #endregion
}
