﻿using AJAXPMVC.Ents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AJAXPMVC
{
    public class LoginController : MainClass
    {
        public new string oTema = "LoginController";
        public void load()
        {
            if (q.userdata.logindurum)
            {
                this.Giris();
            }
            else
            {
                this.LoginPage();
            }
        }
        public void unload()
        {
            q.session.mems.CacheClear(q.session.mems.keyFirst);
            q.session.mems.cil.Dispose();
            q.session.mems.cilent.Dispose();
            q.session.unLoad = true;
        }
        public void LoginPage()
        {
            q.viewData.Add("func", this.m("GirisKontrol", "user:user,pas:pas"));
            q.ajax.html("#BODY", this.getPageHtml("Login"));
        }
        public void GirisKontrol()
        {
            string user = q.request["user"];
            string pas = q.request["pas"];
            try
            {

                string srg = "SELECT a.SifreHatasi,ISNULL(a.Id,0) Id,a.KimlikNo,a.Ad,a.Soyad,a.Cinsiyet,ISNULL(a.DilId,0) DilId,a.SonSifreHataliMi FROM [sis].[KullaniciDogrulama](@p0,@p1) a";
                var param = new Dictionary<string, object>();
                param.Add("p0", user);
                param.Add("p1", pas);
                var row = q.con.CollectionFromSql(srg, param).Select(p => new
                {
                    SifreHatasi = (bool)p.SifreHatasi,
                    Id = (int?)p.Id,
                    KimlikNo = (string)p.KimlikNo,
                    Ad = (string)p.Ad,
                    Soyad = (string)p.Soyad,
                    Cinsiyet = (string)p.Cinsiyet,
                    DilId = (int?)p.DilId,
                    SonSifreHataliMi = (bool?)p.SonSifreHataliMi
                }).FirstOrDefault();

                if (row.SifreHatasi == true)
                {
                    if (row.SonSifreHataliMi == true)
                    {
                        q.ajax.alert("KullaniciAdiSifrehatasi", "Lütfen Son Şifrenizi Giriniz.");
                    }
                    else
                        q.ajax.alert("KullaniciAdiSifrehatasi", "Kullanıcı Adı ve Şifre Hatalıdır.");
                }
                else
                {
                    q.userdata.user = Ent.get<Kullanici>().Where("Id", row.Id).FirstOrDefault<Kullanici>();
                    q.userdata.logindurum = true;

                    this.Giris();

                }
            }
            catch (Exception ex)
            {

                ex.ToString();
            }
        }
        public void Giris()
        {

            this.MenuOlustur();


            q.ajax.html("#BODY", this.getPageHtml("giris"));
        }
        List<MenuView> MenuKullanici;
        private void MenuOlustur()
        {
            string srg = @"SELECT a.Id, ISNULL(a.UstId,0) UstId, a.MenuAdi, a.TabName,ct.Namespaces Controller, ct.ClassName ClassName, a.FuncName, a.PostData, a.Durum, a.Dil FROM sis.Menu a WITH(NOLOCK,READUNCOMMITTED,NOWAIT) LEFT JOIN sis.Controller ct WITH(NOLOCK,READUNCOMMITTED,NOWAIT) ON ct.Id=a.ControllerId  WHERE a.Durum='A'";

            List<MenuView> FULLMENU = q.con.CollectionFromSql(srg).Select(r => new MenuView() { Id = (int)r.Id, UstId = (int)r.UstId, MenuAdi = (string)r.MenuAdi, TabName = (string)r.TabName, Controller = (string)r.Controller, ClassName = (string)r.ClassName, FuncName = (string)r.FuncName, PostData = (string)r.PostData, Durum = (string)r.Durum, Dil = (bool)r.Dil }).ToList();


            this.MenuKullanici = new List<MenuView>();
            this.MenuKullanici.Clear();

            Dictionary<int, MenuView>  MenuKullaniciJson = new Dictionary<int,MenuView>();
            string Menu = this.MenuKullaniciOlustur(ref this.MenuKullanici, FULLMENU, 0,ref MenuKullaniciJson);


            q.viewData["Menu"] = Menu;

            q.viewData["MenuList"] = MenuKullaniciJson.toSerializeJson();


        }

        private string MenuKullaniciOlustur(ref List<MenuView> MenuKullanici, List<MenuView> fULLMENU, int UstId,ref Dictionary<int, MenuView> MenuKullaniciJson,string classname= "sidebar-menu")
        {
            MenuKullanici = MenuKullanici ?? new List<MenuView>();
            string MenuHtml = "<ul class=\""+ classname + "\">";
            switch (classname)
            {
                case "sidebar-menu": classname = "treeview-menu"; break;
                case "treeview-menu": classname = "treeview-menu"; break; 
            }

            var rows = fULLMENU.Where(t => t.UstId == UstId).ToArray();
            for (int i = 0; i < rows.Length; i++)
            {

                MenuKullanici.Add(rows[i]);

                if (rows[i].Controller != null)
                {

                    rows[i].TabName = (rows[i].TabName == null || rows[i].TabName == "") ? ("var id=$$.MainTab.add(true, '', " + rows[i].MenuAdi.toSerializeJson() + ");") : (" var id=$$.MainTab.add(false, " + rows[i].TabName.toSerializeJson() + " , " + rows[i].MenuAdi.toSerializeJson() + ");");

                    rows[i].mFunc = rows[i].TabName + q.sq.m(rows[i].Controller, rows[i].ClassName, rows[i].FuncName, "TabDivId:id," + rows[i].PostData);
                    MenuKullaniciJson.Add(rows[i].Id, rows[i]);

                    MenuHtml += "<li class=\"treeview\" onclick=\"$$.MENU.RUN(" + rows[i].Id + ")\"><a href=\"javascript:void(0)\"><i class=\"fa fa-link\"></i> <span>" + rows[i].MenuAdi + "</span></a>";

                }
                else
                {
                    MenuHtml += "<li class=\"treeview\"><a href=\"javascript:void(0)\"><i class=\"fa fa-link\"></i> <span>" + rows[i].MenuAdi + "</span></a>";

                }
               
                MenuHtml+=this.MenuKullaniciOlustur(ref MenuKullanici[i].SubMenu, fULLMENU, rows[i].Id,ref MenuKullaniciJson, classname);

                MenuHtml += "</li>";
            }
            MenuHtml += "</ul>";

            return MenuHtml;
        }
    }
}