﻿using System;
using System.Linq;
using System.Reflection;
using System.ComponentModel;
using System.Collections.Generic;
using System.Data.SqlClient;
using Newtonsoft.Json.Linq;

namespace AJAXPMVC
{
    public enum FilterOperators
    {
        [Description("#ALAN IN (#SART1)")]
        IN = -1,

        [Description("#ALAN  NOT IN (#SART1)")]
        NOT_IN = 1,

        [Description("#ALAN BETWEEN #SART1 AND #SART2")]
        BETWEEN = 2,

        [Description("#ALAN LIKE '%'+ #SART1 + '%'")]
        LIKE = 3,

        [Description("#ALAN LIKE #SART1 + '%'")]
        STARTSWITH = 4,

        [Description("#ALAN LIKE '%' + #SART1")]
        ENDWITH = 5,

        [Description("#ALAN > #SART1")]
        BIGWITH = 6,

        [Description("#ALAN < #SART1")]
        SMALLWITH = 7,

        [Description("#ALAN <= #SART1")]
        SMALLEQQUALSWITH = 8,

        [Description("#ALAN >= #SART1")]
        BIGEQQUALSWITH = 9,

        [Description("#ALAN = #SART1")]
        EQUALS = 0,

        [Description("#ALAN != #SART1")]
        NOTEQUALS = -3
    }

    public enum FilterOperatorLogics
    {
        [Description("AND")]
        AND = 1,

        [Description("OR")]
        OR = 2
    }
    public enum JOINTYPE
    {
        [Description("INNER JOIN")]
        INNER_JOIN = 1, 
        [Description("LEFT JOIN")]
        LEFT_JOIN = 2, 
        [Description("RIGHT JOIN")]
        RIGHT_JOIN = 3, 
        [Description("CROSS APPLY")]
        CROSS_APPLY = 4, 
        [Description("OUTER APPLY")]
        OUTER_APPLY = 5
    }
    public class JoinTab
    {
        public string TabloAdi, Sart;
        public JOINTYPE type;
    }
    public class Ent
    {
        [Ent(IsBaseClass = true)]
        protected Type _ObjectType;

        [Ent(IsBaseClass = true)]
        protected string _Scheme;

        [Ent(IsBaseClass = true)]
        private Dictionary<string, object> _Parameters = new Dictionary<string, object>();

        [Ent(IsBaseClass = true)]
        protected List<FilterObject> _Filter = new List<FilterObject>();

        [Ent(IsBaseClass = true)]
        protected string _Where;

        [Ent(IsBaseClass = true)]
        protected string _FullTable = string.Empty;

        [Ent(IsBaseClass = true)]
        protected string _OrderBy = string.Empty;

        [Ent(IsBaseClass = true)]
        protected string _Order { set { if (value.Length > 0) { _OrderBy = " ORDER BY " + value; } else { _OrderBy = ""; } } get { return _OrderBy; } }

        [Ent(IsBaseClass = true)]
        protected Dictionary<string, object> _record;

        [Ent(IsBaseClass = true)]
        protected List<JoinTab> _JoinTab;

        [Ent(IsBaseClass = true)]
        protected bool _Identity = true;

        [Ent(IsBaseClass = true)]
        protected string _IdCol = "Id";

        [Ent(IsBaseClass = true)]
        protected bool _NewRecord = false;

        public T Create<T>()
        {
            var entity = Activator.CreateInstance<T>();
            this.SetValues(entity, new Dictionary<string, object>());
            ((Ent)(object)entity)._NewRecord = true;
            return entity;
        }

        public Ent Create()
        {
            this._NewRecord = true;
            return this;
        }
        
        public static Ent get<T>(string tableFullName = "")
        {
            Ent t = new Ent();  
            t._ObjectType = typeof(T);
            t.setType(t,tableFullName);
            return t;
        }
        private void setType(Ent t, string tableFullName)
        {
            t._FullTable = t.Scheme + t._ObjectType.Name;
            if (tableFullName != "")
                t._FullTable = tableFullName;
            PropertyInfo id = t._ObjectType.GetProperties().Where(a => a.GetCustomAttributes<TableAttribute>().FirstOrDefault() != null && a.GetCustomAttributes<TableAttribute>().FirstOrDefault().Primary == true).FirstOrDefault();
            if (id == null)
                throw new Exception("Class Tipi Geçersiz Atribute Tanımsız");

            t._Identity = id.GetCustomAttributes<TableAttribute>().FirstOrDefault().Identity;
            t._IdCol = id.Name;

            t.Colls = t._ObjectType.GetProperties().Where(s => s.GetCustomAttributes<EntAttribute>().Count() == 0).Select(s => s.Name).ToArray(); 
        }

        [Ent(IsBaseClass = true)]
        public static T LoadExtend<T>(string str)
        {
            T f = str.unSerialize<T>();
            Ent fs = f as Ent;
            fs.SetObjectData(typeof(T));
            return f;

        }

    

        public static Ent get(string tableFullName, string IdColl = "Id", bool Identity = true, string[] colls = null)
        {
            Ent t = new Ent();
            t._ObjectType = null;
            t._FullTable = tableFullName;
            t.Colls = colls;
            t._Identity = Identity;
            t._record = new Dictionary<string, object>();
            t._lastdata = new Dictionary<string, object>();
            t._IdCol = IdColl;
            return t;
        }

        [Ent(IsBaseClass = true)]
        private string Scheme
        {
            get
            {
                if (this._Scheme != null)
                    return this._Scheme;

                this._Scheme = (string)this._ObjectType.GetCustomAttributesData()
                    .Where(a => a.AttributeType.Name == typeof(TableAttribute).Name)
                    .FirstOrDefault().NamedArguments.Where(t => t.MemberName == "Scheme").First().TypedValue.Value;

                return this._Scheme;
            }
        }

        public Ent Where(string coll, object value, FilterOperatorLogics logic = FilterOperatorLogics.AND)
        {
            this._Filter.Add(new FilterObject() { Coll = coll, Value = value, Operator = FilterOperators.EQUALS, Logic = logic });
            return this;
        }

        public Ent WhereNotEquals(string coll, object value, FilterOperatorLogics logic = FilterOperatorLogics.AND)
        {
            this._Filter.Add(new FilterObject() { Coll = coll, Value = value, Operator = FilterOperators.NOTEQUALS, Logic = logic });
            return this;
        }
        public Ent WhereIn(string coll, object value, FilterOperatorLogics logic = FilterOperatorLogics.AND)
        {
            this._Filter.Add(new FilterObject() { Coll = coll, Value = value, Operator = FilterOperators.IN, Logic = logic });
            return this;
        }
        public Ent WhereNotIn(string coll, object value, FilterOperatorLogics logic = FilterOperatorLogics.AND)
        {
            this._Filter.Add(new FilterObject() { Coll = coll, Value = value, Operator = FilterOperators.NOT_IN, Logic = logic });
            return this;
        }
        public Ent WhereLike(string coll, string value, FilterOperatorLogics logic = FilterOperatorLogics.AND)
        {
            this._Filter.Add(new FilterObject() { Coll = coll, Value = value, Operator = FilterOperators.LIKE, Logic = logic });
            return this;
        }
        public Ent WhereStartWith(string coll, string value, FilterOperatorLogics logic = FilterOperatorLogics.AND)
        {
            this._Filter.Add(new FilterObject() { Coll = coll, Value = value, Operator = FilterOperators.STARTSWITH, Logic = logic });
            return this;
        }
        public Ent WhereEndWith(string coll, string value, FilterOperatorLogics logic = FilterOperatorLogics.AND)
        {
            this._Filter.Add(new FilterObject() { Coll = coll, Value = value, Operator = FilterOperators.ENDWITH, Logic = logic });
            return this;
        }
        public Ent WhereBigWith(string coll, object value, FilterOperatorLogics logic = FilterOperatorLogics.AND)
        {
            this._Filter.Add(new FilterObject() { Coll = coll, Value = value, Operator = FilterOperators.BIGWITH, Logic = logic });
            return this;
        }
        public Ent WhereSmallWith(string coll, object value, FilterOperatorLogics logic = FilterOperatorLogics.AND)
        {
            this._Filter.Add(new FilterObject() { Coll = coll, Value = value, Operator = FilterOperators.SMALLWITH, Logic = logic });
            return this;
        }
        public Ent WhereSmallEqualsWith(string coll, object value, FilterOperatorLogics logic = FilterOperatorLogics.AND)
        {

            this._Filter.Add(new FilterObject() { Coll = coll, Value = value, Operator = FilterOperators.SMALLEQQUALSWITH, Logic = logic });
            return this;
        }
        public Ent WhereBigEqualsWith(string coll, object value, FilterOperatorLogics logic = FilterOperatorLogics.AND)
        {

            this._Filter.Add(new FilterObject() { Coll = coll, Value = value, Operator = FilterOperators.BIGEQQUALSWITH, Logic = logic });
            return this;
        }
        public Ent WhereBetween(string coll, object firstValue, object endValue, FilterOperatorLogics logic = FilterOperatorLogics.AND)
        {
            this._Filter.Add(new FilterObject() { Coll = coll, Value = firstValue, Value2 = endValue, Operator = FilterOperators.BETWEEN, Logic = logic });
            return this;
        }

        [Ent(IsBaseClass = true)]
        protected string getFilter
        {
            get
            {
                if (_Filter.Count == 0) return "";

                string sart = "";

                this._Parameters.Clear();

                for (int i = 0; i < _Filter.Count; i++)
                {
                    FilterObject r = _Filter[i];
                    if (r.Operator == FilterOperators.EQUALS && r.Value == null)
                    {
                        sart = (sart.Length > 0 ? (" ( " + sart + ") " + r.Logic.ToString()) : "") + " " + r.Coll + " IS NOT NULL";
                    }

                    if (r.Value == null) continue;

                    string sarttip = "";
                    string par = "";
                    switch (r.Operator)
                    {
                        case FilterOperators.BETWEEN:
                            sarttip = r.Operator.GetDescription();
                            if (r.Value.GetType().IsDateTime())
                            {
                                par = "@par_" + i.ToString() + "_p_1";
                                this._Parameters.Add(par, ((DateTime)r.Value).ToString("yyyy-mm-dd hh-mm-ss"));
                                sarttip = sarttip.Replace("#SART1", "" + par + "");
                                par = "@par_" + i.ToString() + "_p_2";
                                this._Parameters.Add(par, ((DateTime)r.Value2).ToString("yyyy-mm-dd hh-mm-ss"));
                                sarttip = sarttip.Replace("#SART2", "" + par + "");
                            }
                            else
                            {
                                par = "@par_" + i.ToString() + "_p_1";
                                this._Parameters.Add(par, r.Value);
                                sarttip = sarttip.Replace("#SART1", "" + par + "");
                                par = "@par_" + i.ToString() + "_p_2";
                                this._Parameters.Add(par, r.Value2);
                                sarttip = sarttip.Replace("#SART2", "" + par + "");
                            }

                            sarttip = sarttip.Replace("#ALAN", r.Coll);
                            sart = (sart.Length > 0 ? (" ( " + sart + ") " + r.Logic.ToString()) : "") + " " + sarttip;
                            break;
                        case FilterOperators.BIGEQQUALSWITH:
                        case FilterOperators.BIGWITH:
                        case FilterOperators.ENDWITH:
                        case FilterOperators.EQUALS:
                        case FilterOperators.NOTEQUALS:
                        case FilterOperators.LIKE:
                        case FilterOperators.SMALLEQQUALSWITH:
                        case FilterOperators.SMALLWITH:
                        case FilterOperators.STARTSWITH:
                            sarttip = r.Operator.GetDescription();
                            par = "@par_" + i.ToString() + "";
                            sarttip = sarttip.Replace("#SART1", "" + par + "");
                            sarttip = sarttip.Replace("#ALAN", r.Coll);
                            sart = (sart.Length > 0 ? (" ( " + sart + ") " + r.Logic.ToString()) : "") + " " + sarttip;
                            this._Parameters.Add(par, r.Value);
                            break;

                        case FilterOperators.IN:
                        case FilterOperators.NOT_IN:
                            dynamic ar = r.Value as dynamic;

                            if (ar.Length < 1) continue;

                            par = "@par_" + i.ToString() + "_";
                            List<string> parameterList = new List<string>();
                            for (int j = 0; j < ar.Length; j++)
                            {
                                parameterList.Add(par + j.ToString());
                                this._Parameters.Add(par + j.ToString(), ar[j]);
                            }
                            par = string.Join(",", parameterList);

                            sarttip = r.Operator.GetDescription();
                            sarttip = sarttip.Replace("#SART1", "" + par + "");

                            sarttip = sarttip.Replace("#ALAN", r.Coll);
                            sart = (sart.Length > 0 ? (" ( " + sart + ") " + r.Logic.ToString()) : "") + " " + sarttip;
                            break;
                        default:
                            break;
                    }
                }

                this._Where = " WHERE " + sart;
                return this._Where;
            }
        }

        public void SetJoject(JToken jToken)
        {
            foreach (Newtonsoft.Json.Linq.JProperty x in jToken)
            {
                this[x.Name] = x.Value.IsNullValueString();
            } 
        }

        [Ent(IsBaseClass = true)]
        public object this[string name]
        {
            get
            {
                try
                {
                    
                    if (this._ObjectType != null)
                    {
                        this._record[name] = this.GetType().GetProperty(name).GetValue(this);
                    }
                    else
                    {
                        if (this._record.ContainsKey(name))
                            return this._record[name];
                        else
                            return null;
                    }
                    return this._record[name];
                }
                catch (Exception)
                {
                    //throw new Exception("Alan geçersiz.");
                    return null;
                }
            }
            set
            {
                try
                {
                    if (this._ObjectType != null)
                    {
                        this.GetType().GetProperty(name).SetValue(this, Convert.ChangeType(value, this._record[name].GetType()));
                        this._record[name] = value;
                    }
                    else
                    {
                        if (this._record.ContainsKey(name))
                            this._record[name] = value;
                        else
                            this._record.Add(name, value);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Alan geçersiz.");
                }
            }
        }

        [Ent(IsBaseClass = true)]
        protected int? Top { get; set; } = null;

        [Ent(IsBaseClass = true)]
        protected string getTop
        {
            get
            {
                if (Top == null) return string.Empty;

                return " TOP (" + Top.ToString() + ") ";
            }
        }

        [Ent(IsBaseClass = true)]
        protected string[] Colls;

        [Ent(IsBaseClass = true)]
        private Dictionary<string, object> _lastdata;

        [Ent(IsBaseClass = true)]
        public string getColl(string id = null)
        {
            if (Colls == null)
            {
                return "*";

            }
            if (id == null)
                return string.Join(",", this.Colls);
            else
                return string.Join(",", this.Colls.Where(t => t != id));

        }

        [Ent(IsBaseClass = true)]
        private void SetValues(object s, Dictionary<string, object> dictionary)
        {
            Ent r = ((Ent)s);

            r._Filter = _Filter;
            r._ObjectType = _ObjectType;
            r._Parameters = _Parameters;
            r._Scheme = _Scheme;
            r._Where = _Where;
            r.Top = Top;
            r._record = dictionary;
            r._FullTable = _FullTable;
            r.Colls = Colls;
            r._Identity = _Identity;
            r._IdCol = _IdCol;
            r._NewRecord = _NewRecord;

            if (r._record != null)
            {
                r._lastdata =  dictionary.ToDictionary(entry => entry.Key, entry => entry.Value);
            }
        }

        [Ent(IsBaseClass = true)]
        public T FirstOrDefault<T>()
        {
            this.Top = 1;
            return this.ToList<T>().FirstOrDefault();
        }

        [Ent(IsBaseClass = true)]
        public List<T> ToList<T>()
        {
            List<T> ret = new List<T>();
            T s;
            var li = q.con.CollectionFromSqlDataReader(this.GetSelectQuery(), this._Parameters).ToList();

            for (int i = 0; i < li.Count; i++)
            {
                s = DynamicToStatic.ToStatic<T>(li[i]);
                this.SetValues(s, li[i]);
                ret.Add(s);
            }
            return ret;
        }

        [Ent(IsBaseClass = true)]
        public IEnumerable<dynamic> ToListDynamic()
        {
            var li = q.con.CollectionFromSql(this.GetSelectQuery(), _Parameters);
            return li;
        }
        [Ent(IsBaseClass = true)]
        public long Count()
        {
            try
            {

                string srg = GetSelectQuery("COUNT(0) AS Sayi");
                long? Ids = q.con.CollectionFromSql(srg, _Parameters).FirstOrDefault().Sayi;
                return Ids ?? 0;
            }
            catch (Exception ex)
            {
                throw new Exception("COUNT HATASI");
            }
        }

        [Ent(IsBaseClass = true)]
        private string GetSelectQuery(string extCol="")
        {
            if (extCol == "")
                extCol = this.getTop + this.getColl();
            return "SELECT " + extCol + " FROM " + this._FullTable + " WITH(NOLOCK,READUNCOMMITTED,NOWAIT) " +this.getJoinTable()+ " " + this.getFilter + " " + this._Order;
        }

        [Ent(IsBaseClass = true)]
        public Ent AddJOIN(string JoinTable, string Sart = "", JOINTYPE Type = JOINTYPE.INNER_JOIN)
        {
            if (this._JoinTab == null)
                this._JoinTab = new List<JoinTab>();
            this._JoinTab.Add(new JoinTab() { Sart=Sart,TabloAdi= JoinTable,type=Type });
            return this;
        }
        [Ent(IsBaseClass = true)]
        protected string getJoinTable()
        {
            if (this._JoinTab == null)
                return "";
            string j = "";
            for (int i = 0; i < this._JoinTab.Count; i++)
            {
                j += " "+this._JoinTab[i].type.GetDescription() + " " + this._JoinTab[i].TabloAdi + " " + this._JoinTab[i].Sart;

            }
            return j;

        }



        [Ent(IsBaseClass = true)]
        public bool Delete()
        {
            this.SetObjectData();
            if (this._IdCol != "")
            {
                try
                {
                    var x = new Dictionary<String, object>();
                    x.Add("p0", this[this._IdCol]);
                    return q.con.ExecuteNonQuery("DELETE FROM " + this._FullTable + " WITH(NOWAIT) WHERE " + this._IdCol + "=@p0", x) ;
                }
                catch
                {
                    return false;
                }

            }
            return false;
        }

        [Ent(IsBaseClass = true)]
        public Ent OrderBy(string _OrderBy)
        {
            this._Order = _OrderBy;
            return this;
        }

        [Ent(IsBaseClass = true)]
        public bool Save()
        {
            bool r = false;
            this.SetObjectData();
            if (this._NewRecord)
            {
                string[] alan = this._record.Keys.Where(t => this._Identity == false || (this._Identity == true && t != this._IdCol)).ToArray();

                string sorgu = "INSERT INTO " + this._FullTable + " "; // + (string.Join(",", alan)) + " VALUES (";
                string ilk = "";
                string _alan = "";
                string _deger = "";
                string p = "";
                Dictionary<string, object> par = new Dictionary<string, object>();
                for (int i = 0; i < alan.Length; i++)
                {
                    _alan += ilk + alan[i];
                    p = "@P" + i.ToString();
                    _deger += ilk + p;
                    par.Add(p, this._record[alan[i]]);
                    ilk = ", ";
                }
                sorgu += "(" + _alan + ")VALUES(" + _deger + ");";


                if (this._Identity)
                {

                    sorgu += "SELECT   CAST(@@IDENTITY AS NVARCHAR(50)) AS Id;";
                    var LastId = q.con.CollectionFromSql(sorgu, par).FirstOrDefault().Id;
                    if (LastId == null)
                        throw new Exception("Identy");

                    var Ids = long.Parse(LastId as string); 
                    if (Ids != null)
                    {
                        this[_IdCol] = Ids;
                        this._lastdata[_IdCol] = Ids;
                        r = true;
                    }
                    else
                        r = false;

                }
                else
                {
                     
                    r = q.con.ExecuteNonQuery(sorgu, par);
                }
                _NewRecord = false;
                /*

                if (r && this._Identity)
                {
                    //this._record[IdCol].GetType().IsNumeric();
                    // Type ts=this._record[IdCol].GetType();
                    var vls= q.con.LastInsertId();

                  
                }*/

            }
            else
            {   
                this._lastdata = this._lastdata ?? new Dictionary<string, object>();
                var li = this._record.Except(this._lastdata).ToDictionary(entry => entry.Key, entry => entry.Value); // Güncellenen Alanlar
                if (this._Identity && Convert.ToDecimal(this._record[this._IdCol]) != Convert.ToDecimal(this._lastdata[this._IdCol]))
                {
                    throw new Exception("Identy Değiştirilemez!!!!");
                }
                string sorgu = "UPDATE " + this._FullTable + " SET\n";
                Dictionary<string, object> par = new Dictionary<string, object>();

                string[] alan = li.Keys.Where(t => this._Identity == false || (this._Identity == true && t != this._IdCol)).ToArray();
                if (alan.Length == 0)
                    return true;
                string p = "";
                for (int i = 0; i < alan.Length; i++)
                {

                    p = "@P" + i.ToString();
                    sorgu += alan[i] + " = " + (li[alan[i]] == null && false ? "NULL" : p) + " ,";
                    //  if (li[alan[i]] == null)
                    //    continue;
                    par.Add(p, li[alan[i]]);
                }
                sorgu = sorgu.TrimEnd(',');
                sorgu += " WHERE " + this._IdCol + "= @P" + (alan.Length).ToString();



                par.Add("@P" + alan.Length.ToString(), this._record[this._IdCol].GetType().IsNumbers() ? Convert.ToDecimal(this._record[this._IdCol]) : this._record[this._IdCol]);
                r = q.con.ExecuteNonQuery(sorgu,par);
            }
            return r;
        }

        [Ent(IsBaseClass = true)]
        private void SetObjectData(Type t=null)
        {
            if (this._record == null)
                this._record = new Dictionary<string, object>(); 
            if (t != null && this._ObjectType==null &&t.Name!="Ent" && this._Identity)
            {
                this._ObjectType = t;
                this.setType(this,""); 
                if (this._lastdata == null)
                {
                    this._lastdata = new Dictionary<string, object>(); 
                    this._lastdata[this._IdCol] = this.GetType().GetProperty(this._IdCol).GetValue(this);
                }
            }
            if (this._ObjectType != null)
            {
              //  string[] alancol = this.Colls.Where(ts => IdIptal == false || (IdIptal == true && ts != this._IdCol)).ToArray();
                for (int i = 0; i < this.Colls.Length; i++)
                {
                    this._record[this.Colls[i]] = this.GetType().GetProperty(this.Colls[i]).GetValue(this);
                }
            }
        }
    }

    public static class DynamicToStatic
    {
        public static T ToStatics<T>(object expando)
        {
            var entity = Activator.CreateInstance<T>();
            var properties = expando as IDictionary<string, object>;

            if (properties == null) return entity;

            foreach (var entry in properties)
            {
                var propertyInfo = entity.GetType().GetProperty(entry.Key);
                if (propertyInfo != null)
                {
                    try
                    {
                        propertyInfo.SetValue(entity, entry.Value ?? null, null);
                    }
                    catch { }
                }
            }
            return entity;
        }

        public static T ToStatic<T>(Dictionary<string, object> dat)
        {
            var entity = Activator.CreateInstance<T>();

            if (dat == null) return entity;

            foreach (KeyValuePair<string, object> pair in dat)
            {
                var propertyInfo = entity.GetType().GetProperty(pair.Key.ToString());
                if (propertyInfo != null)
                {
                    try
                    {
                        propertyInfo.SetValue(entity, dat[pair.Key.ToString()] ?? null, null);
                    }
                    catch { }
                }
            }

            return entity;
        }
    }

    public class FilterObject
    {
        public string Coll;
        public object Value;
        public object Value2;
        public FilterOperators Operator;
        public FilterOperatorLogics Logic;

    }

    internal class TableAttribute : Attribute
    {
        public bool Identity = false;
        public string Scheme = "dbo.";
        public bool Primary = false;
    }

    internal class EntAttribute : Attribute
    {
        public bool IsBaseClass = false;
    }


   


}