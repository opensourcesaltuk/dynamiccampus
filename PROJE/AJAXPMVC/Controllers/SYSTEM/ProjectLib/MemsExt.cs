﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

public static class MemsExt
{
    private static memsnew _mem = new memsnew(conf.StaticMemIp, conf.StaticMemKeyFrist);

    public static bool CacheClear(this string key)
    {        
        return _mem.CacheClear(key);
    }

    public static bool CacheYaz(this object data, string o, DateTime time)
    { 
        return _mem.CacheYaz(data,o,time); 
    } 
    public static bool CacheYaz<T>(this T data, string o)
    {
        return _mem.CacheYaz<T>(data, o,DateTime.Now.AddHours(1));
    }
    public static T CacheOku<T>(this string key, bool ornekle = false)
    {
        return _mem.CacheOku<T>(key, ornekle);

    }

}