﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AJAXPMVC.DataBase;


namespace AJAXPMVC
{
    public class q
    {
        

        /// <summary>
        /// Global Context;
        /// </summary>
        public static HttpContext getContext { get { return System.Web.HttpContext.Current; } }

        public static string getDomanin
        {
            get
            {

                return "//" + getContext.Request.Host.ToUriComponent() + "/";

                  
            }
        }

        public static Dictionary<string,object> viewData
        {
            get
            {
                  
                if (!getContext.Items.ContainsKey("ViewDataDictionary-ViewData"))
                {
                    getContext.Items["ViewDataDictionary-ViewData"] = new Dictionary<string, object>();
                }
                return getContext.Items["ViewDataDictionary-ViewData"] as Dictionary<string, object>;
            }
            set
            {
                getContext.Items["ViewDataDictionary-ViewData"] = value;

            }
        }

        public static dbCon con
        {
            get
            {
               
                if (!getContext.Items.ContainsKey("dbCon-con"))
                {
                    getContext.Items["dbCon-con"] = conf.getEntities<dbCon>();
                }
                return getContext.Items["dbCon-con"] as dbCon;
            }
        }


        public static sq sq
        {
            get
            { 
                if (!getContext.Items.ContainsKey("sq-sq"))
                {
                    getContext.Items["sq-sq"] = new sq(getContext);
                }
                return getContext.Items["sq-sq"] as sq;
            }
        }

        public static UserStatic userdata
        {
            get
            { 
                if (!getContext.Items.ContainsKey("UserStatic-userdata"))
                {
                    getContext.Items["UserStatic-userdata"] = q.session.get<UserStatic>("q:UserStatic-userdata") ?? new UserStatic();
                }
                return getContext.Items["UserStatic-userdata"] as UserStatic;
            }
        }

        public static AJAX ajax
        {
            get
            { 
                if (!getContext.Items.ContainsKey("AJAX-AJAX"))
                {
                    getContext.Items["AJAX-AJAX"] = new AJAX();
                }
                return getContext.Items["AJAX-AJAX"] as AJAX;
            }
        }

        public static CryptoAes sessionaes
        {

            get
            {
                 
                if (!getContext.Items.ContainsKey("CryptoAes-sessionaes"))
                {

                    AES_DEF def = null;

                    def = session.get<AES_DEF>("user_AES_DEF");
                    if (def == null)
                        def = Crypto.getKey();

                    session.set<AES_DEF>("user_AES_DEF", def);

                    ///session.SetObject(def, "user_AES_DEF");
                    /*
                    if (q.session["user_AES_DEF"] != null)
                        def = conf.unSerialize<AES_DEF>((string)session["user_AES_DEF"]);
                    if (def==null && q.session["user_AES_DEF"] != null)
                        def = conf.unSerialize<AES_DEF>((string)session["user_AES_DEF"]);
                    if (def == null) 
                    {
                        def = Crypto.getKey();
                        session["user_AES_DEF"] = def.toSerialize();

                    }*/
                    getContext.Items["CryptoAes-sessionaes"] = new CryptoAes(def.Key, def.IV);
                }
                return getContext.Items["CryptoAes-sessionaes"] as CryptoAes;
            }
        }

        public static Dictionary<string, string> request
        {
            get
            { 
                if (!getContext.Items.ContainsKey("Dictionary-rq"))
                {
                    getContext.Items["Dictionary-rq"] = new Dictionary<string, string>();
              

                    foreach (var item in getContext.Request.Query)
                    {
                        try
                        {
                            (getContext.Items["Dictionary-rq"] as Dictionary<string, string>).Add(item.Key, item.Value);
                        }
                        catch { }
                    }

                    if (getContext.Request.Method!= "GET")
                    foreach (var item in getContext.Request.Form)
                    {
                        try
                        {
                            (getContext.Items["Dictionary-rq"] as Dictionary<string, string>).Add(item.Key, item.Value);
                        }
                        catch { }
                    }

                   
                }
                return getContext.Items["Dictionary-rq"] as Dictionary<string, string>;
            }
        }

        public static OturumNew session
        {
            get
            { 
                if (!getContext.Items.ContainsKey("Oturum-Session"))
                {
                    string id = "";
                    if (q.request.ContainsKey("sayfa"))
                    {
                        string[] sayfa = q.request["sayfa"].Split('/');
                        if (sayfa.Length > 1)
                        {
                            id = sayfa[1];
                            Guid gnd;
                            if (!Guid.TryParse(id, out gnd))
                            {
                                id = "";
                            }
                        }
                    }
                    else if (q.request.ContainsKey("ASPSESSID"))
                    {
                        id = q.request["ASPSESSID"];
                        Guid gnd;
                        if (!Guid.TryParse(id, out gnd))
                        {
                            id = "";
                        }
                    }



                    id = (id == "" ? Guid.NewGuid().ToString() : id).ToLower().Replace("-", "");
                    getContext.Items["Oturum-Session"] = new OturumNew(new memsnew("127.0.0.1", ""), id);
                }
                return getContext.Items["Oturum-Session"] as OturumNew;
            }
        }

        public static IViewRenderService viewRenderSvc { get; set; }
    }
}
