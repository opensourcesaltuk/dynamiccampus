﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AJAXPMVC
{
    public class BorcController : MainClass
    {
        public new string oTema = "BorcController";
        public void BorcTanimOgrenciLoad()
        {
            ParcaEkran.OgrenciSec("#OgrenciBorcTanimView", this.oNamespace, this.oClassname, "BorcTanimlaOgrenci", "PostData", "Öğrenci Borç Tanımlama", false);
        }

        public void BorcTanimlaOgrenci(long KullaniciId=0)
        {
            if (KullaniciId == 0)
                KullaniciId = (long)q.request["PostData"].GetJObject()["KullaniciId"].toNullGetDecimalValue();
            Ents.vw_KullaniciBilgileri kisi = Ent.get<Ents.vw_KullaniciBilgileri>().Where("Id", KullaniciId).FirstOrDefault<Ents.vw_KullaniciBilgileri>();
            FormModelGenerator frm = new FormModelGenerator();
            string ModelAdi = "OgrBorcListesi";
            frm.FormEkleDuzenle(ModelAdi, "Kişi Bilgisi");
            frm.EkleLabelBox(ModelAdi, "Kimlik No", kisi.KimlikNo);
            frm.EkleLabelBox(ModelAdi, "Pasaport No", kisi.PasaportNo);
            frm.EkleLabelBox(ModelAdi, "Ad - Soyad", kisi.Ad + " - " + kisi.Soyad);


            ModelAdi = "OgrOdemelerListesi";

            string jqxGridFromId = "#" + frm.FormEkleDuzenle(ModelAdi, "Borç Listesi") + " #Nesneler";
            string BorcGrid = "FRM_" + jqxGridFromId.Md5Sum();

            ModelAdi = "BorcKaydet";

            jqw liste = new jqw(false, BorcGrid);

            liste.data = q.con.CollectionFromSql("SELECT * FROM brc.vw_Borc WHERE KullaniciId=" + KullaniciId.ToString()).ToArray().getJsonArray();

            liste.AddColumn("Düzenle", "Edit", "100px", jqcolumntype.button, "", null, this.m("OgrenciBorcTanimiDuzenlemeView", "KullaniciId:" + KullaniciId.ToString() + ",datarow:datarow"), false);
            liste.AddDataColumns("Id", "int");
            liste.AddColumn("Id", "Id", "50px"); 
            liste.AddDataColumns("DonemId", "long");
            liste.AddDataColumns("DonemAdi", "string"); 
            liste.AddColumn("Dönem", "DonemAdi", "50px");
            liste.AddDataColumns("YilId", "long");
            liste.AddDataColumns("YilAdi", "string"); 
            liste.AddColumn("Yıl", "YilAdi", "50px");
            liste.AddDataColumns("UcretTipiId", "int");
            liste.AddDataColumns("UcretTipiKodu", "string");
            liste.AddDataColumns("UcretTipiAdi", "string");
            liste.AddColumn("Üçret Tipi", "UcretTipiAdi", "50px");
            liste.AddDataColumns("UcretTipiZorunluPesinMi", "bool");
            liste.AddDataColumns("UstUcretTipiId", "int?");
            liste.AddDataColumns("UstUcretTipiKodu", "string");
            liste.AddDataColumns("UstUcretTipiAdi", "string");
            liste.AddDataColumns("UstYilBazliKontrolEdilsinMi", "bool");
            liste.AddDataColumns("KullaniciId", "long"); 
            liste.AddColumn("Kullanını Id", "KullaniciId", "50px");
            liste.AddDataColumns("KimlikNo", "string");
            liste.AddColumn("Kimlik No", "KimlikNo", "100px");
            liste.AddDataColumns("Ad", "string");
            liste.AddColumn("Ad", "Ad", "100px");
            liste.AddDataColumns("Soyad", "string");
            liste.AddColumn("Soyad", "Soyad", "100px");
            liste.AddDataColumns("Cinsiyet", "string");
            liste.AddDataColumns("OgrenciBolumId", "long?");
            liste.AddDataColumns("OgrenciNumarasi", "string");
            liste.AddColumn("Ogr. Numrası", "OgrenciNumarasi", "120px");
            liste.AddDataColumns("AkademikBirimId", "long?");
            liste.AddDataColumns("AkademikBirimTipId", "long?");
            liste.AddDataColumns("AkademikBirimTipKodu", "string");
            liste.AddDataColumns("AkademikBirimAdi", "string");
            liste.AddColumn("Akademik Birim", "AkademikBirimAdi", "120px");
            liste.AddDataColumns("AkademikBirimAktifId", "long?");
            liste.AddDataColumns("ProgramId", "long?");
            liste.AddDataColumns("ProgramAdi", "string");
            liste.AddColumn("Program Adı", "ProgramAdi", "120px");
            liste.AddDataColumns("ProgramTipAdi", "string");
            liste.AddDataColumns("ProgramBirimKodu", "string");
            liste.AddDataColumns("OgrenciDonemKayitId", "long?");
            liste.AddDataColumns("Sinif", "int?");
            liste.AddDataColumns("SinifAdi", "string");
            liste.AddColumn("Sınıf", "SinifAdi", "120px");
            liste.AddDataColumns("SinifAciklama", "string");
            liste.AddDataColumns("SonOdemeTarihi", "DateTime?");
            liste.AddDataColumns("PesinOdenir", "bool"); 
            liste.AddDataColumns("BrutUcret", "decimal");
            liste.AddColumn("Brüt", "BrutUcret", "80px");
            liste.AddDataColumns("ToplamIndirimOrani", "float?"); 
            liste.AddColumn("Toplam İnd. Oranı", "ToplamIndirimOrani", "80px");
            liste.AddDataColumns("NakitIndirimTutari", "decimal");
            liste.AddColumn("Nakit İndirim", "NakitIndirimTutari", "80px");
            liste.AddDataColumns("NetUcret", "float?");
            liste.AddColumn("Net Ücret", "NetUcret", "80px");
            liste.AddDataColumns("EnBuyukTaksitSayisi", "int");
            liste.AddDataColumns("ParaBirimiId", "int");
            liste.AddDataColumns("ParaBirimiKodu", "string");
            liste.AddColumn("Para Birimi", "ParaBirimiKodu", "80px");
            liste.AddDataColumns("ParaBirimiSembolu", "string");
            liste.AddDataColumns("ParaBirimiAktifMi", "bool");
            liste.AddDataColumns("ParaBirimiEntegrasyonKodu", "int");



            q.ajax.newTabId("dataNewBorc", "OgrenciBorcListesi", "Öğrenci Borç Listesi");
            q.ajax.html("#OgrenciBorcListesi", frm.Olustur());

            liste.Ozellikler.height = "500px";
            liste.Ozellikler.pageable = true;
            liste.Ozellikler.editable = false;
            liste.Ozellikler.autoRowHeight = false;
            liste.PostRowDetailsFunc(oNamespace, oClassname, "OgrenciBursDetay", "Detay");

            liste.Ozellikler.columnsResize = true;
            liste.Ozellikler.enableHover = true;
            liste.Ozellikler.filterable = true;
            liste.Ozellikler.filterMode = jqfilterMode.Advanced;
            liste.Ozellikler.rowdetails = true; 
            liste.Ozellikler.columnsResize = true;
            liste.Ozellikler.enableHover = true;
            liste.AddToolbarButton("Ekle", fa_icons.fa_fa_plus, this.m("OgrenciBorcTanimiDuzenlemeView", "KullaniciId:" + KullaniciId.ToString()));
            q.ajax.html(jqxGridFromId, liste.Olustur());

            //this.BorcTanimlamaOgrenciView(Id,0);
        }
        public void OgrenciBursDetay() {

            var x = jqw.getRowDetails("Detay");

            string Id = "#"+x["MaterParentGridId"].ToString() + " #"+x["id"].ToString();
            int BorcId = (int)x["data"]["Id"].toNullGetDecimalValue();
            jqw liste = new jqw(true);

            liste.data = q.con.CollectionFromSql("SELECT BursAdi,BursOran,Tutar,Sira FROM brc.vw_BorcBursOran WHERE BorcId="+ BorcId + " AND SilDurum=0 ORDER BY Sira ASC").ToArray().getJsonArray();

            liste.AddColumn("Sıra", "Sira", "80px");
            liste.AddDataColumns("Sira", "string");
            liste.AddColumn("Burs Adı", "BursAdi", "250px");
            liste.AddDataColumns("BursAdi", "string");
            liste.AddColumn("Oran %", "BursOran", "80px",jqcolumntype.numberinput);
            liste.AddDataColumns("BursOran", "decimal");
            liste.AddColumn("İnd.Tutarı", "Tutar", "100px", jqcolumntype.numberinput);
            liste.AddDataColumns("Tutar", "decimal");

            liste.Ozellikler.pageable = false;

            liste.Ozellikler.columnsResize = true;

            liste.Ozellikler.autoRowHeight = false;
            liste.Ozellikler.height = "300px";

            q.ajax.html(Id, liste.Olustur());
        }
        public void OgrenciBorcTanimiDuzenlemeView()
        {

            long KullaniciId = (long)q.request["KullaniciId"].toNullGetDecimalValue();
            int BorcId = 0;
            if (q.request.ContainsKey("datarow"))
            {
                BorcId = (int)q.request["datarow"].GetJObject()["Id"].toNullGetDecimalValue();
            }
            this.BorcTanimlamaOgrenciView(KullaniciId, BorcId);
        }
        public void BorcTanimlamaOgrenciView(long KullaniciId,int BorcId)
        {
            Ents.vw_KullaniciBilgileri kisi = Ent.get<Ents.vw_KullaniciBilgileri>().Where("Id", KullaniciId).FirstOrDefault<Ents.vw_KullaniciBilgileri>();
            FormModelGenerator frm = new FormModelGenerator();
            string ModelAdi = "BorcDuzenlenenBilgi";
            frm.FormEkleDuzenle(ModelAdi, "Kişi Bilgisi");
            frm.EkleLabelBox(ModelAdi, "Kimlik No", kisi.KimlikNo);
            frm.EkleLabelBox(ModelAdi, "Pasaport No", kisi.PasaportNo);
            frm.EkleLabelBox(ModelAdi, "Ad - Soyad", kisi.Ad +" - "+ kisi.Soyad); 
            ModelAdi = "BorcDuzenleme";

            Ent BorcView = Ent.get("Brc.vw_Borc").Where("Id", BorcId);
            if (BorcId > 0)
            {
                BorcView = BorcView.FirstOrDefault<Ent>();

            }


            frm.FormEkleDuzenle(ModelAdi, BorcId==0?"Yeni Borç Kaydı":"Borç Düzenleme");

            string ViewBorcId = frm.EkleTextBox(ModelAdi, "BorcId", "Düzenlen Kayıt Id", BorcId.ToString(), 10, true);

            frm.EkleDropDownList(ModelAdi, "DonemId", "Dönem", BorcView["DonemId"].toNullStringValue(), BorcView["DonemAdi"].toNullStringValue(), q.sq.getTipData("SELECT a.DonemId id, a.DonemAdi label, a.YilAdi AS 'group' FROM tpl.vw_Donem a", "vw_Donem_Group", false),true);
            frm.EkleDropDownList(ModelAdi, "UcretTipiId", "Ücret Tipi", BorcView["UcretTipiId"].toNullStringValue(), BorcView["UcretTipiAdi"].toNullStringValue(), q.sq.getTipData("SELECT a.Id id,a.Adi label FROM mh.vw_UcretTipi a", "vw_UcretTipi", false),true);


            frm.EkleDropDownList(ModelAdi, "OgrenciDonemKayitId", "Öğrencinin Bölümü", BorcView["OgrenciDonemKayitId"].toNullStringValue(), 
                
                
                BorcView["SinifAdi"].toNullStringValue() +" - " + BorcView["OgrenciDonemKayitDonemAdi"].toNullStringValue() , 
                
                q.sq.getTipData(@"
                    SELECT blg.OgrenciNumarasi +' - '+ blg.ProgramAdi+' - OgrBolumId: '+CAST(blg.OgrenciBolumId AS NVARCHAR(MAX)) AS 'group', dk.Id AS id,dk.SinifAdi +' - '+dk.DonemAdi AS label,dk.SinifAdi +' - '+dk.DonemAdi AS html 
                        FROM ogr.vw_OgrenciBilgileri blg
                        INNER JOIN ogr.vw_OgrenciDonemKayit dk ON dk.OgrenciBolumId = blg.OgrenciBolumId
                WHERE blg.KullaniciId = " + KullaniciId.ToString(), "vw_OgrenciBilgileriDonemKayit_Borclar_"+KullaniciId.ToString(), false),true);

            frm.EkleTextBoxDate(ModelAdi, "SonOdemeTarihi", "Son Ödeme Tarihi", BorcView["SonOdemeTarihi"].toSerialize(), ViewDateFormat.DateTime, false);
            frm.EkleCheckBox(ModelAdi, "PesinOdenir", "Tahsilat Peşin Mi Yapılsın?", BorcView["PesinOdenir"].BoolNullValue(false), true);
            frm.EkleTextBoxDec(ModelAdi, "BrutUcret", "Brüt Ücret", BorcView["BrutUcret"].toNullGetDecimalValue(), 10, 2, 0, 150000, true);
            frm.EkleTextBoxDec(ModelAdi, "NakitIndirimTutari", "Nakit İndirim", BorcView["NakitIndirimTutari"].toNullGetDecimalValue(), 10, 2, 0, 150000, true);
            frm.EkleDropDownList(ModelAdi, "ParaBirimiId", "Para Birimi", BorcView["ParaBirimiId"].toNullStringValue(), BorcView["ParaBirimiSembolu"].toNullStringValue(), q.sq.getTipData("SELECT ID id,Sembolu label FROM mh.ParaBirimi (NOLOCK) ORDER BY id", "ParaBirimi_Sembol", false),true);

            frm.EkleTextBoxDec(ModelAdi, "OdenmesiGereken", "Ödenmesi Gereken", BorcView["NetUcret"].toNullGetDecimalValue(), 10, 2, 0, 150000, true);

            ModelAdi = "BursOran";
            string jqxGridFromId= "#"+frm.FormEkleDuzenle(ModelAdi, "Burs Oranlari") + " #Nesneler";
            string BursOranFormId = "FRM_"+jqxGridFromId.Md5Sum();

            ModelAdi = "BorcKaydet";

            jqw liste = new jqw(false, BursOranFormId);
            frm.FormEkleDuzenle(ModelAdi,"");
            frm.EkleButton(ModelAdi, oNamespace, oClassname, "BorcTanimlamaOgrenciKaydet", "FormData", "BursOranlari:" + liste.getDataJs() + ",SilBursOranlari:" + liste.getDeleteRowsJs(), "Kaydet", ViewBtnClass.btn_danger, fa_icons.fa_fa_plus_circle);


            liste.AddDataColumns("Sira", "int");
            liste.AddColumn("Sıra No.", "Sira", "100px", jqcolumntype.numberinput,"", null, "", true);
            liste.AddDataColumns("BursAdi", "string");
            var BursOranData = q.sq.getTipData("SELECT Id AS id,BursAdi label,BursOran AS Ext1 FROM  mh.BursOran (NOLOCK)", "mh.BursOran", false);
            liste.AddColumn("Burs Adı", "BursOranId", "250px", jqcolumntype.combobox, "BursAdi", BursOranData, "", true);


            liste.AddDataColumns("Tutar", "decimal");
            liste.AddColumn("Tutar", "Tutar", "100px", jqcolumntype.numberinput, "", null, "", false); 


            //q.ajax.newTabId("dataNewBorc", "OgrenciBorcDuzenleme", "Öğrenci Borcu Düzenleme");
            //q.ajax.html("#OgrenciBorcDuzenleme", frm.Olustur());
            q.ajax.dialogOpen("OgrenciBorcDuzenleme", "OgrenciBorcDuzenleme", "Öğrenci Borcu Düzenleme", frm.Olustur());


            liste.AddDataColumns("Id", "int");
            liste.AddDataColumns("BursOranId", "int"); 
            liste.AddColumn("Sil", "Sil", "100px", jqcolumntype.button, "", null, this.m("BorcTanimlamaOgrenciSil", "GridId:'" + BursOranFormId + "',datarow:datarow"), false);

            liste.data = q.con.CollectionFromSql(@"SELECT Id ,
              BorcId ,
              BursOranId ,
              BursAdi ,
              BursOran ,
              Sira ,
              Tutar ,
              OlusturmaTarihi ,
              SilmeTarihi ,
              SilDurum FROM brc.vw_BorcBursOran
			  WHERE SilDurum=0 AND BorcId=" + BorcId.ToString() + @"
			  ORDER BY Sira ASC").ToArray().getJsonArray();

            liste.Ozellikler.height = "300px";
            liste.Ozellikler.pageable = false;
            liste.Ozellikler.editable = true;
            liste.Ozellikler.autoRowHeight = false;

            liste.Ozellikler.selectionmode = jqselectionmode.singlecell;
            liste.Ozellikler.columnsResize = true;
            liste.Ozellikler.enableHover = true; 
            liste.AddToolbarButton("Ekle", fa_icons.fa_fa_plus, this.m("BorcTanimlamaOgrenciEkle", "GridId:'#"+ BursOranFormId + "',Row:" + liste.getRowDatajs()));
            q.viewData["IdForm"] = frm.Id;
            q.viewData["IdGrid"] = liste.Id;
            q.viewData["BursOran"] = BursOranData;
            string AppendHtml = this.getPageHtml("OgrenciBorcTanimi/BorcDuzenlemeJs");
            q.ajax.html(jqxGridFromId, liste.Olustur()+"\n"+AppendHtml);
        }
        public void BorcTanimlamaOgrenciEkle()
        {
            jqw.EkleNewRow(q.request["GridId"],false); 
        }
        public void BorcTanimlamaOgrenciSil()
        {
            jqw.DeleteRow(q.request["GridId"], q.request["datarow"].GetJObject()["uid"].toNullStringValue());
        }
        public void BorcTanimlamaOgrenciKaydet()
        {
            var ViewData = q.request["FormData"].GetJObject();

            var Burslar = q.request["BursOranlari"].GetJArray().OrderBy(t => t["Sira"].toNullGetDecimalValue()).ToArray() ;
            var SilBursOranlari = q.request["SilBursOranlari"].GetJArray();

            var BorcDuzenlemeView = ViewData["BorcDuzenleme"];

            Ent OgrenciDonemKayitRow = Ent.get("ogr.OgrenciDonemKayit").Where("Id", BorcDuzenlemeView["OgrenciDonemKayitId"].toNullStringValue()).FirstOrDefault<Ent>();
            Ents.vw_OgrenciBilgileri ogrBilgi = OgrenciBolumKayitController.GetVw_OgrenciBilgileri((long)OgrenciDonemKayitRow["OgrenciBolumId"].toNullGetDecimalValue());
            //var ctx = q.con.TransactionSql();
            try
            {
                Ent BorcTablo = Ent.get("brc.Borc");
                if (BorcDuzenlemeView["BorcId"].toNullGetDecimalValue() == 0)
                {
                    BorcTablo.Create();
                }
                else
                {
                    BorcTablo= BorcTablo.Where("Id", BorcDuzenlemeView["BorcId"].toNullGetDecimalValue()).FirstOrDefault<Ent>();
                }
                
                BorcTablo["DonemId"] = BorcDuzenlemeView["DonemId"].toNullStringValue();
                BorcTablo["UcretTipiId"] = BorcDuzenlemeView["UcretTipiId"].toNullStringValue();
                BorcTablo["OgrenciDonemKayitId"] = BorcDuzenlemeView["OgrenciDonemKayitId"].toNullStringValue();
                BorcTablo["OgrenciDonemKayitId"] = BorcDuzenlemeView["OgrenciDonemKayitId"].toNullStringValue();
                BorcTablo["SonOdemeTarihi"] = BorcDuzenlemeView["SonOdemeTarihi"].toNullStringValue();
                BorcTablo["PesinOdenir"] = BorcDuzenlemeView["PesinOdenir"].toNullStringValue();
                BorcTablo["BrutUcret"] = BorcDuzenlemeView["BrutUcret"].toNullStringValue();
                BorcTablo["NakitIndirimTutari"] = BorcDuzenlemeView["NakitIndirimTutari"].toNullStringValue();
                BorcTablo["ParaBirimiId"] = BorcDuzenlemeView["ParaBirimiId"].toNullStringValue();
                BorcTablo["OgrenciBolumId"] = OgrenciDonemKayitRow["OgrenciBolumId"].toNullStringValue();
                BorcTablo["KullaniciId"] = ogrBilgi.KullaniciId;
                
                using (var ctx = q.con.TransactionSql())
                {


                    if (BorcTablo.Save())
                    {
                        for (int i = 0; i < SilBursOranlari.Count; i++)
                        {
                            if (SilBursOranlari[i]["Id"].toNullStringValue() != "")
                            {
                                Ent sil = Ent.get("brc.BorcBursOran").Where("Id", SilBursOranlari[i]["Id"].toNullGetDecimalValue()).FirstOrDefault<Ent>();
                                sil["SilmeTarihi"] = DateTime.Now;
                                sil["SilDurum"] = true;
                                sil.Save();
                            }
                        }
                        int Sira = 1;
                        for (int i = 0; i < Burslar.Length; i++)
                        {
                            if (Burslar[i]["BursOranId"].toNullStringValue() == null)
                                continue;
                            Ent Brs = Ent.get("brc.BorcBursOran");
                            if (Burslar[i]["Id"].toNullStringValue() == "")
                            {
                                Brs.Create();
                                Brs["OlusturmaTarihi"] = DateTime.Now;
                            }
                            else
                            {
                                Brs = Brs.Where("Id", Burslar[i]["Id"].IsNullValueString()).FirstOrDefault<Ent>();
                            }
                            Brs["BorcId"] = BorcTablo["Id"].toNullGetDecimalValue();
                            Brs["BursOranId"] = Burslar[i]["BursOranId"].toNullGetDecimalValue();
                            Brs["Tutar"] = Burslar[i]["Tutar"].toNullGetDecimalValue();
                            Brs["Sira"] = Sira;
                            
                            Sira++;
                            Brs.Save();

                        } 
                    }
                    BorcTablo.Save();
                    ctx.Complete();
                }
                //ctx.Complete();
                //q.ajax.TabClose("sad", "OgrenciBorcDuzenleme");
                q.ajax.dialogClose("OgrenciBorcDuzenleme", "OgrenciBorcDuzenleme");
                this.BorcTanimlaOgrenci(ogrBilgi.KullaniciId);
            }
            catch (Exception ex)
            { 
                //ctx.Dispose();
                throw;
            }

        }

    }
}