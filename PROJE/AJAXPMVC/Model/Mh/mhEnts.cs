﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AJAXPMVC.Ents
{ 

    [TableAttribute(Scheme = "mh.")]
    public class vw_ServisIslemHesapKasa : Ent
    {
        [TableAttribute(Primary = true, Identity = false)]
        public int Id { get; set; }
        public int FirmaId { get; set; }
        public string FirmaAdi { get; set; }
        public int SistemTipiId { get; set; }
        public string SistemTipiAdi { get; set; }
        public bool SistemTipiAktifMi { get; set; }
        public int IslemTipiId { get; set; }
        public string IslemTipiAdi { get; set; }
        public int ServisId { get; set; }
        public Guid TanimKodu { get; set; }
        public int HesapKasaId { get; set; }
        public string HesapKasaKodu { get; set; }
        public string HesapKasaAdi { get; set; }
        public int ParaBirimiId { get; set; }
        public string ParaBirimiKodu { get; set; }
        public int EntegrasyonKodu { get; set; }
    }

    [TableAttribute(Scheme = "mh.")]
    public class Odeme : Ent
    {
        [TableAttribute(Primary = true, Identity = true)]
        public long Id { get; set; }
        public int ServisId { get; set; }
        public int HesapKasaId { get; set; }
        public int BorcId { get; set; }
        public long KullaniciId { get; set; }
        public DateTime OlusturulmaTarihi { get; set; }
        public DateTime DokumanTarihi { get; set; }
        public decimal OdemePlaniTutar { get; set; }
        public int SistemTipiId { get; set; }
        public int IslemTipiId { get; set; }
        public int ParaBirimiId { get; set; }
        public int TaksitSayisi { get; set; }
        public decimal TaksitToplamiTutar { get; set; }
        public string ReferansKodu { get; set; }
        public int OdemeSonucId { get; set; }
        public string IslemIp { get; set; }
        public Guid IslemUniq { get; set; }
        public string IslemXMLLog { get; set; }
        public int IslemSuresi { get; set; }
        public decimal IslemKuru { get; set; }
        public string DonenDataXMLlog { get; set; }
        public bool OtomatikPesinIndrimiUygulandiMi { get; set; }
    }

    [TableAttribute(Scheme = "mh.")]
    public class OdemeSonuc : Ent
    {
        [TableAttribute(Primary = true, Identity = false)]
        public int Id { get; set; }
        public string Aciklama { get; set; }
    }

    [TableAttribute(Scheme = "mh.")]
    public class OdemeTaksit : Ent
    {
        [TableAttribute(Primary = true, Identity = true)]
        public long Id { get; set; }
        public long OdemeId { get; set; }
        public decimal Tutar { get; set; }
        public decimal TaksitKomisyonTutar { get; set; }
        public DateTime TaksitTarihi { get; set; }
        public DateTime BlokeTarihi { get; set; }
        public decimal IadeTutar { get; set; }
        public bool IadeMi { get; set; }
        public bool OdemePlanindanDus { get; set; }
        public int? IadeIslemTipiId { get; set; }
        public long? IadeKullaniciId { get; set; }
        public string IadeServisSicilKimlik { get; set; }
    }

    [TableAttribute(Scheme = "mh.")]
    public class Servis : Ent
    {
        [TableAttribute(Primary = true, Identity = true)]
        public int Id { get; set; }
        public Guid TanimKodu { get; set; }
        public int? ParaBirimiId { get; set; }
        public int? BankaId { get; set; }
    }
    [TableAttribute(Scheme = "mh.")]
    public class vw_Servis : Servis
    { 
        public string Kodu { get; set; }
        public int? EntegrasyonKodu { get; set; } 
        public string BankaKodu { get; set; }
        public string BankaAdi { get; set; }
        public bool? OtsAktifMi { get; set; }
        public bool? AktifMi { get; set; }
    }
    [TableAttribute(Scheme = "mh.")]
    public class ServisFirma : Ent
    {
        [TableAttribute(Primary = true, Identity = true)]
        public int Id { get; set; }
        public int FirmaId { get; set; }
        public int ServisId { get; set; }
        public Guid TanimKodu { get; set; }
    }

}