﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;

namespace AJAXPMVC
{
    public class KullanciController : MainClass
    {
        public new string oTema = "KullanciController";
        string mTabId = "KullaniciTanimlariList";
        public void ModulListe()
        {
            FormModelGenerator frm = new FormModelGenerator();
            frm.FormEkleDuzenle("vw_KullaniciBilgileri", "Kullanıcı Bilgileri Arama");
            frm.EkleTextBox("vw_KullaniciBilgileri", "KimlikNo", "Kimlik No", "", 20);
            frm.EkleTextBox("vw_KullaniciBilgileri", "PasaportNo", "Pasaport No", "", 100);
            frm.EkleTextBox("vw_KullaniciBilgileri", "Ad", "Ad", "", 100);
            frm.EkleTextBox("vw_KullaniciBilgileri", "Soyad", "Soyad", "", 100);
            frm.EkleDropDownList("vw_KullaniciBilgileri", "Cinsiyet", "Cinsiyet", "E", "",q.con.CollectionFromSql("SELECT c.Id id,c.CinsiyetAdi label  FROM tpl.vw_Cinsiyet c").ToArray().getJsonArray(), false);
            frm.EkleTextBox("vw_KullaniciBilgileri", "DogumYeri", "Dogum Yeri", "", 100);
            frm.EkleTextBox("vw_KullaniciBilgileri", "BabaAdi", "Baba Adı", "", 100);
            frm.EkleTextBox("vw_KullaniciBilgileri", "AnneAdi", "Anne Adı", "", 100);

            string Id = frm.FormEkleDuzenle("vw_KullaniciBilgileri_list", "Kullanıcı Arama Listesi Sonucu");
            Id = "ListId:'"+Id+"'";
            frm.EkleButton("vw_KullaniciBilgileri", this.oNamespace, this.oClassname, "KullaniciAra", "data", Id, "Ara",  ViewBtnClass.btn_default, fa_icons.fa_fa_search);
            frm.EkleButton("vw_KullaniciBilgileri",this.oNamespace, this.oClassname, "KullaniciEkleDuzenleme", "data", Id, "Ekle",  ViewBtnClass.btn_primary, fa_icons.fa_fa_plus);
            q.ajax.html("#" + mTabId, frm.Olustur());

        }

        public void KullaniciAra()
        {
            Newtonsoft.Json.Linq.JObject data = Newtonsoft.Json.Linq.JObject.Parse(q.request["data"]);

            var sql = Ent.get<Ents.vw_KullaniciBilgileri>();
            //var sql = new Ent();
            if (data["vw_KullaniciBilgileri"].HasValues)
            {
                if (data["vw_KullaniciBilgileri"]["KimlikNo"].ToString()!="")
                {
                    sql = sql.WhereStartWith("KimlikNo", data["vw_KullaniciBilgileri"]["KimlikNo"].ToString());
                }
                if (data["vw_KullaniciBilgileri"]["PasaportNo"].ToString()!="")
                {
                    sql = sql.WhereStartWith("PasaportNo", data["vw_KullaniciBilgileri"]["PasaportNo"].ToString());
                }
                if (data["vw_KullaniciBilgileri"]["Ad"].ToString()!="")
                {
                    sql = sql.WhereStartWith("Ad", data["vw_KullaniciBilgileri"]["Ad"].ToString());
                }
                if (data["vw_KullaniciBilgileri"]["Soyad"].ToString()!="")
                {
                    sql = sql.WhereStartWith("Soyad", data["vw_KullaniciBilgileri"]["Soyad"].ToString());
                }
                if (data["vw_KullaniciBilgileri"]["Cinsiyet"].ToString()!="")
                {
                    sql = sql.Where("Cinsiyet", data["vw_KullaniciBilgileri"]["Cinsiyet"].ToString());
                }
                if (data["vw_KullaniciBilgileri"]["DogumYeri"].ToString()!="")
                {
                    sql = sql.WhereStartWith("DogumYeri", data["vw_KullaniciBilgileri"]["DogumYeri"].ToString());
                }
                if (data["vw_KullaniciBilgileri"]["BabaAdi"].ToString()!="")
                {
                    sql = sql.WhereStartWith("BabaAdi", data["vw_KullaniciBilgileri"]["BabaAdi"].ToString());
                }
                if (data["vw_KullaniciBilgileri"]["AnneAdi"].ToString()!="")
                {
                    sql = sql.WhereStartWith("AnneAdi", data["vw_KullaniciBilgileri"]["AnneAdi"].ToString());
                }
            }

            var datalist = sql.ToList<Ents.vw_KullaniciBilgileri>();


            string Id = "#"+q.request["ListId"]+ " #Nesneler";


            jqw liste = new jqw(false, "FRM_"+("#" + q.request["ListId"] + " #Nesneler").Md5Sum());
            liste.AddColumn("Id", "Id", "80px");
            liste.AddDataColumns("Id", "long");
            liste.AddColumn("Kimlik No", "KimlikNo", "100px");
            liste.AddDataColumns("KimlikNo", "string");

            liste.AddColumn("Pasaport", "PasaportNo", "100px");
            liste.AddDataColumns("PasaportNo", "string");

            liste.AddColumn("Ad", "Ad", "100px");
            liste.AddDataColumns("Ad", "string");

            liste.AddColumn("Soyad", "Soyad", "100px");
            liste.AddDataColumns("Soyad", "string");

            liste.AddColumn("Cinsiyet", "Cinsiyet", "80px");
            liste.AddDataColumns("Cinsiyet", "string");

            liste.data = datalist.getJsonArray<Ents.vw_KullaniciBilgileri>();
            liste.Ozellikler.height = "320px";
            liste.Ozellikler.disabled = false;
            liste.Ozellikler.filterMode = jqfilterMode.Advanced;
            liste.Ozellikler.columnsResize = true;
            liste.Ozellikler.enableHover = true;
            //liste.AddToolbarButton()

            liste.AddToolbarButton("Düzenle", fa_icons.fa_fa_pencil,  this.m("KullaniciEkleDuzenleme", "ekle:false,datarow:" + liste.getRowDatajs()));
            q.ajax.html(Id, liste.Olustur());
        }
        public void KullaniciEkleDuzenleme(bool Ogrenci=false)
        {


            FormModelGenerator frm = new FormModelGenerator();

            string id = q.request.ContainsKey("datarow") ? q.request["datarow"] : "";
            if (id.Length > 0)
            {
                if (Ogrenci == true)
                {
                    id = id.GetJObject()["KullaniciId"].IsNullValueString() == null ? "0" : id.GetJObject()["KullaniciId"].IsNullValueString().ToString();
                }
                else
                    id = id.GetJObject()["Id"].IsNullValueString() == null ? "0" : id.GetJObject()["Id"].IsNullValueString().ToString();
            }
            else
                id = "0";

            var sql = Ent.get<Ents.vw_KullaniciBilgileri>().Where("Id", id).FirstOrDefault<Ents.vw_KullaniciBilgileri>();
            if (sql == null)
                sql = new Ents.vw_KullaniciBilgileri();



            frm.FormEkleDuzenle("Kullanici", "Kullanıcı Genel Bilgileri");
            frm.EkleTextBox("Kullanici", "KimlikNo", "Kimlik No", sql.KimlikNo, 20,true);
            frm.EkleTextBox("Kullanici", "Ad", "Ad", sql.Ad, 20,true);
            frm.EkleTextBox("Kullanici", "Soyad", "Soyad", sql.Soyad, 20, true);
            //frm.EkleTextBox("Kullanici", "Cinsiyet", "Cinsiyet", "", 20);
            frm.EkleDropDownList("Kullanici", "DilId", "Dil Tanımı", sql.DilId.ToString(),sql.DilAdi, q.con.CollectionFromSql("SELECT c.id,c.DilAdi label  FROM tpl.[vw_Diller] c").ToArray().getJsonArray(), true);
            frm.EkleDropDownList("Kullanici", "Cinsiyet", "Cinsiyet", sql.Cinsiyet,sql.Cinsiyet, q.con.CollectionFromSql("SELECT c.Id id,c.CinsiyetAdi label  FROM tpl.vw_Cinsiyet c").ToArray().getJsonArray(), true);
            //frm.EkleTextBox("Kullanici", "DilId", "DilId", "", 20);

            frm.FormEkleDuzenle("KullaniciDetay", "Kullanıcı Detay Bilgileri");

            frm.EkleTextBox("KullaniciDetay", "DogumYeri", "Dogum Yeri", sql.DogumYeri, 100);
            frm.EkleTextBoxDate("KullaniciDetay", "DogumTarihi", "Dogum Tarihi",sql.DogumTarihi.toSerialize(), ViewDateFormat.Date);
            frm.EkleTextBoxDate("KullaniciDetay", "VerilisTarihi", "Yenileme Tarihi", sql.VerilisTarihi.toSerialize(), ViewDateFormat.Date);
            frm.EkleTextBox("KullaniciDetay", "VerilisNedeni", "Veriliş Nedeni", sql.VerilisNedeni, 100);
            frm.EkleTextBox("KullaniciDetay", "BabaAdi", "Baba Adı", sql.BabaAdi, 100);
            frm.EkleTextBox("KullaniciDetay", "AnneAdi", "Anne Adı", sql.AnneAdi, 100);
            frm.EkleTextBox("KullaniciDetay", "NufusCuzdaniSeriNo", "Nufus Cuzdanı Seri No", sql.NufusCuzdaniSeriNo, 100);
            frm.EkleTextBox("KullaniciDetay", "NufusCuzdaniNo", "Nufus Cuzdanı No", sql.NufusCuzdaniNo, 100);
            frm.EkleTextBox("KullaniciDetay", "KoyMah", "Nüfüs Koy Mah", sql.KoyMah, 100);

            string Ids=frm.EkleComboBox("KullaniciDetay", "NufusaKayitliOlduguIl", "Nufusa Kayıtlı Oldugu İl", sql.NufusaKayitliOlduguIl.toNullStringValue(), sql.NufusaKayitliOlduguIlAdi, "il");
            List<ViewComboCascade> cas = new List<ViewComboCascade>();
            cas.Add(new ViewComboCascade() { Id="#"+Ids,Parameter="@ilid",Type=ViewComboCascadeType.ComboBox});
            frm.EkleComboBox("KullaniciDetay", "NufusaKayitliOlduguIlce", "Nufusa Kayıtlı Oldugu İlce", sql.NufusaKayitliOlduguIlce.toNullStringValue(), sql.NufusaKayitliOlduguIlceAdi, "ilce",cas);
            frm.EkleComboBox("KullaniciDetay", "UyrukId", "Uyruk",sql.UyrukId.ToString(),sql.UyrukAdi, "ulke");
            frm.EkleTextBox("KullaniciDetay", "CiltNo", "Cilt No", sql.CiltNo, 100);
            frm.EkleTextBox("KullaniciDetay", "AileSiraNo", "Aile Sıra No", sql.AileSiraNo, 100);
            frm.EkleTextBox("KullaniciDetay", "PasaportNo", "Pasaport No", sql.PasaportNo, 100);
            //frm.EkleDropDownList("KullaniciDetay", "UyrukId", "Uyruk", "", q.con.CollectionFromSql("SELECT a.TipId id,a.Ad label FROM tpl.vw_TipGrupListesi a where a.GrupKodu='ulke'").ToArray().getJsonArray());

            if (Ogrenci == true)
            {
                Ents.vw_Kullanici_OgrenciBilgileri ogrenciVw;
                if (id != "0")
                {
                    ogrenciVw = Ent.get<Ents.vw_Kullanici_OgrenciBilgileri>().Where("KullaniciId", id).FirstOrDefault<Ents.vw_Kullanici_OgrenciBilgileri>();
                }
                else {
                    ogrenciVw = new Ents.vw_Kullanici_OgrenciBilgileri();
                }
                frm.FormEkleDuzenle("Ogrenci", "Öğrenci Genel Bilgileri"); 
                frm.EkleDropDownList("Ogrenci", "IlkGelisTipiId", "Geliş Tipi", ogrenciVw.IlkGelisTipiId.toNullStringValue(), ogrenciVw.IlkGelisTipiAdi.toNullStringValue(), q.sq.getvwtipGrupListesi("gelistipi"),true);
            } 
            frm.FormEkleDuzenle("KullaniciDetaySave","");
            frm.EkleButton("KullaniciDetaySave",this.oNamespace, this.oClassname, "KullaniciKaydet", "FormData", "KullaniciId:"+id, "Kaydet", ViewBtnClass.btn_success, fa_icons.fa_fa_save);

            q.ajax.dialogOpen("KullaniciDuzenleme", "KullaniciDuzenleme", "Kullanıcı Ekleme - Düzenleme", frm.Olustur());
        }
        public void KullaniciKaydet()
        {

            Newtonsoft.Json.Linq.JObject obj = Newtonsoft.Json.Linq.JObject.Parse(q.request["FormData"]);


          

            string Id = q.request.ContainsKey("KullaniciId")?q.request["KullaniciId"]:"0";
             
            Ent klc = Ent.get("kul.Kullanici"); //obj["Kullanici"].ToString().unSerialize<Ents.Kullanici>();
            if (Id!="0")
                klc = klc.Where("Id", Id).FirstOrDefault<Ent>();
            else
                klc = klc.Create();

            Ent ent = Ent.get("kul.Kullanici").Where("KimlikNo", obj["Kullanici"]["KimlikNo"].IsNullValueString()).WhereNotEquals("Id",Id).FirstOrDefault<Ent>();

            if (ent != null)
            {
                q.ajax.alert("Kaydedildi", "İlgili KimlikNo Kullanımda.");
                return;
            }

            klc.SetJoject(obj["Kullanici"]);
            using (TransactionScope ctx = new TransactionScope())
            {

                try
                {
                    if (klc.Save())
                    {
                        Ent klcdetay = Ent.get("kul.KullaniciDetay", "Id", false);
                        if (Id!="0")
                            klcdetay = klcdetay.Where("Id", klc["Id"]).FirstOrDefault<Ent>();
                        else
                        {
                            klcdetay = klcdetay.Create();
                            klcdetay["Id"] = klc["Id"];

                        }
                        klcdetay.SetJoject(obj["KullaniciDetay"]);
                        if (klcdetay.Save())
                        {

                         
                            if (obj["Ogrenci"].toNullStringValue()!=null)
                            {
                                Ent ogr = Ent.get("ogr.Ogrenci", "Id", false).Where("Id", klc["Id"]).FirstOrDefault<Ent>();
                                if (ogr == null)
                                {
                                    ogr = Ent.get("ogr.Ogrenci", "Id", false).Create();
                                    ogr["Id"] = klc["Id"];
                                    ogr["GId"] = Guid.NewGuid().ToString();
                                }
                                ogr["IlkGelisTipiId"] = obj["Ogrenci"]["IlkGelisTipiId"].toNullStringValue();
                                if (ogr.Save())
                                {

                                }
                                else
                                {

                                    q.ajax.alert("Kaydedildi", "Öğrenci Bilgileri Kaydedilmedigi İşlem İptal Edildi.");
                                    ctx.Dispose();
                                    return;
                                }
                            }
                            ctx.Complete();
                            q.ajax.alert("Kaydedildi", "Kaydedildi");
                            q.ajax.dialogClose("close", "KullaniciDuzenleme");
                        }

                    }

                }
                catch (Exception ex)
                {
                    ctx.Dispose();
                    q.ajax.alert("Kaydedildi", "Kayıt sırasında bir problem oluştu.\n" + ex.ToString());
                }

            }
        }

    }
}

