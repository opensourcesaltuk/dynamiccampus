﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AJAXPMVC
{
    public class FormAramaListesiNoktasi :MainClass
    {

        public new string oTema = "System/FormAramaListesiNoktasi";
        public string Sql, ElemanId;
        public List<ListeColumns> ColList = new List<ListeColumns>(); 
        public string AramaListView; 
        public string AramaModel;

        public string NesneBaslik { get; set; }

        internal bool AddCol(string ColKod, string ColName, jqcolumntype type, bool Arama=false, bool Donut =false, bool DonutText= false)
        {
            if (ColList.Where(t => t.ColKod == ColKod).Count() > 0)
            {
                throw new Exception("Aynı ColName İki Tane Olmaz"); 
            }
            ColList.Add(new ListeColumns() { ColKod = ColKod, ColName=ColName,Arama=Arama,type= type,Donut=Donut,DonutText=DonutText });
            return true;
        }
        public void ThreeDotViewList()
        { 
            string data = q.request["aralist"].AES_decrypt();

            FormAramaListesiNoktasi o= data.unSerializeJson<FormAramaListesiNoktasi>();

            FormModelGenerator grn = new FormModelGenerator();
            string Model = "Arama" + Guid.NewGuid().ToString().Md5Sum();
            o.AramaModel = Model;
            grn.FormEkleDuzenle(Model, o.NesneBaslik+" Araması");

            this.ColList = o.ColList.Where(t => t.Arama == true).ToList();

            for (int i = 0; i < this.ColList.Count; i++)
            {
                switch (this.ColList[i].type)
                {
                    case jqcolumntype.textbox:
                        grn.EkleTextBox(Model, this.ColList[i].ColKod, this.ColList[i].ColName, "");
                        break;
                    case jqcolumntype.number:
                        grn.EkleTextBoxDec(Model, this.ColList[i].ColKod, this.ColList[i].ColName, null,null,0, null, null, false);
                        break;
                    case jqcolumntype.numberinput:
                        grn.EkleTextBoxDec(Model, this.ColList[i].ColKod, this.ColList[i].ColName, null, null, null, null, null, false);
                        break;
                }
            }

            o.AramaListView= grn.FormEkleDuzenle(Model + "_Liste","");

            o.viewTabDivId = "DD_" + Guid.NewGuid().ToString().Md5Sum();


            data = "data : " + o.toSerializeJson().AES_encrypt().toJavaScriptValue();


            grn.EkleButtonFnc(Model, q.ajax.dialogClose(o.viewTabDivId, o.viewTabDivId,true),"Kapat", ViewBtnClass.btn_danger, fa_icons.fa_fa_close);
            grn.EkleButton(Model, oNamespace, oClassname, "SonucGetir", "q", data, "Ara", ViewBtnClass.btn_default, fa_icons.fa_fa_search);
          
       

            q.ajax.dialogOpen(o.viewTabDivId, o.viewTabDivId, o.NesneBaslik + " Araması", grn.Olustur());
        }

        public void SonucGetir()
        {

            string data = q.request["data"].AES_decrypt();

            FormAramaListesiNoktasi o = data.unSerializeJson<FormAramaListesiNoktasi>();

            var FormData = q.request["q"].GetJObject()[o.AramaModel];


            this.ColList = o.ColList.Where(t => t.Arama == true).ToList();
            Dictionary<string, object> par = new Dictionary<string, object>();
            o.Sql = o.Sql + " 1=1 ";
            int parNo1 = 0;

            for (int i = 0; i < this.ColList.Count; i++)
            {
                switch (this.ColList[i].type)
                {
                    case jqcolumntype.textbox:

                        if (!FormData[this.ColList[i].ColKod].IsNull())
                        {
                            o.Sql += " AND " + this.ColList[i].ColKod + " LIKE '%'+@" + this.ColList[i].ColKod.ToString() + "+'%'";
                            parNo1++;
                            par.Add(this.ColList[i].ColKod, FormData[this.ColList[i].ColKod].toNullStringValue());
                        }
                        break;
                    case jqcolumntype.number:
                        if (!FormData[this.ColList[i].ColKod].IsNull())
                        {
                            o.Sql += " AND " + this.ColList[i].ColKod + "=@" + this.ColList[i].ColKod;
                            parNo1++;
                            par.Add(this.ColList[i].ColKod, FormData[this.ColList[i].ColKod].toNullGetDecimalValue());
                        }
                        break;
                    case jqcolumntype.numberinput:
                        if (!FormData[this.ColList[i].ColKod].IsNull())
                        {
                            o.Sql += " AND " + this.ColList[i].ColKod + "=" + this.ColList[i].ColKod.ToString();
                            parNo1++;
                            par.Add(this.ColList[i].ColKod, FormData[this.ColList[i].ColKod].toNullGetDecimalValue());
                        }
                        break;
                }
            }

            var xx = q.con.CollectionFromSql(o.Sql, par).ToArray();


           
            jqw liste = new jqw(true);

            string DonutEleman = "''"; 
            o.ColList.Where(t => t.Donut == true).ToList().ForEach(t=>{
                DonutEleman += "+ xxx." + t.ColKod;

            });
            DonutEleman = DonutEleman.Trim(new char[] {',' });
            DonutEleman = DonutEleman.Trim();


            string DonutElemanText = "";
            o.ColList.Where(t => t.DonutText == true).ToList().ForEach(t => {
                DonutElemanText += "+ ', '+ xxx." + t.ColKod+".toString()";

            });
            DonutElemanText = DonutElemanText.Trim("+ ', '+ ".ToArray());
            DonutElemanText = DonutElemanText.Trim();

            
            liste.data = xx.getJsonArray();
            liste.AddColumn("Seç", "Sec", "50px", jqcolumntype.button, "Sec", null, @" debugger;
                    var xxx=JSON.parse(datarow);
                    $('" + o.ElemanId + @"').attr('dvalue'," + DonutEleman + @");
                    $('" + o.ElemanId + @"').val(" + DonutElemanText + @");

            " + q.ajax.dialogClose("asdasd", o.viewTabDivId, true));

            for (int i = 0; i < o.ColList.Count(); i++)
            {
                liste.AddDataColumns(o.ColList[i].ColKod, "string");
                liste.AddColumn(o.ColList[i].ColName, o.ColList[i].ColKod, "auto");
            }
            liste.Ozellikler.height = "500px";
            liste.Ozellikler.pageable = true;
            liste.Ozellikler.editable = false;
            liste.Ozellikler.autoRowHeight = false;
            liste.Ozellikler.columnsResize = true;
            liste.Ozellikler.enableHover = true;
            liste.Ozellikler.filterable = true;
            liste.Ozellikler.filterMode = jqfilterMode.Simple; 
            liste.Ozellikler.columnsResize = true;
            liste.Ozellikler.selectionmode = jqselectionmode.singlecell;
            liste.Ozellikler.enableHover = true;
            liste.Ozellikler.groupable = true;

            string Model = o.AramaListView;



            q.ajax.html("#" + Model, liste.Olustur());
        }
    }

    public class ListeColumns
    {
        public string ColKod, ColName;
        public bool Arama = false;
        public bool Donut = false;
        public bool DonutText = false;
        public jqcolumntype type = jqcolumntype.textbox; 
    }
}