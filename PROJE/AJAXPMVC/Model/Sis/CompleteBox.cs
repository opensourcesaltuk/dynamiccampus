﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AJAXPMVC.Ents
{ 
    [TableAttribute(Scheme = "sis.")]
    public class CompleteBox : Ent
    {
        [TableAttribute(Primary = true, Identity = true)]
        public int Id { get; set; }
        public string Kod { get; set; }
        public string KullanimAmaclari { get; set; }
        public string tSql { get; set; }
        public bool Kullanici { get; set; }
    }
}