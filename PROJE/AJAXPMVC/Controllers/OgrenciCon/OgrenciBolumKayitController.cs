﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;

namespace AJAXPMVC
{
    public class OgrenciBolumKayitController : MainClass
    {
        public new string oTema = "OgrenciBolumKayitController";
        string mTabId = "OgrenciListesi";
        public void load()
        {
            FormModelGenerator frm = new FormModelGenerator();
            frm.FormEkleDuzenle("vw_Kullanici_OgrenciBilgileri",    "Öğrenci Bilgileri Arama");
            frm.EkleTextBox("vw_Kullanici_OgrenciBilgileri",        "KimlikNo", "Kimlik No", "", 20);
            frm.EkleTextBox("vw_Kullanici_OgrenciBilgileri",        "PasaportNo", "Pasaport No", "", 100);
            frm.EkleTextBox("vw_Kullanici_OgrenciBilgileri",        "Ad", "Ad", "", 100);
            frm.EkleTextBox("vw_Kullanici_OgrenciBilgileri",        "Soyad", "Soyad", "", 100);
            frm.EkleDropDownList("vw_Kullanici_OgrenciBilgileri",   "Cinsiyet", "Cinsiyet", null, "",q.con.CollectionFromSql("SELECT c.Id id,c.CinsiyetAdi label  FROM tpl.vw_Cinsiyet c").ToArray().getJsonArray(), false);
     

            string Id = frm.FormEkleDuzenle("vw_Kullanici_OgrenciBilgileri_list", "Öğrenci Arama Listesi Sonucu");
            Id = "ListId:'"+Id+"'";
            frm.EkleButton("vw_Kullanici_OgrenciBilgileri", this.oNamespace, this.oClassname, "OgrenciAra", "data", Id, "Ara",  ViewBtnClass.btn_default, fa_icons.fa_fa_search);
            frm.EkleButton("vw_Kullanici_OgrenciBilgileri", this.oNamespace, this.oClassname, "KullaniciEkleDuzenleme", "data", Id, "Ekle",  ViewBtnClass.btn_primary, fa_icons.fa_fa_plus);
            q.ajax.html("#" + mTabId, frm.Olustur());

        }

        public void ParcaOgrenciSec(string selector, string oNameSpace, string className, string funcName, string postData, string baslik, bool cokluSecim)
        {
            FormModelGenerator frm = new FormModelGenerator();

            frm.FormEkleDuzenle("ParcaEkranArama", baslik);

            frm.EkleTextBox("ParcaEkranArama", "KimlikNo", "Kimlik No", "", 20);
            frm.EkleTextBox("ParcaEkranArama", "PasaportNo", "Pasaport No", "", 100);
            frm.EkleTextBox("ParcaEkranArama", "Ad", "Ad", "", 100);
            frm.EkleTextBox("ParcaEkranArama", "Soyad", "Soyad", "", 100);
            frm.EkleDropDownList("ParcaEkranArama", "Cinsiyet", "Cinsiyet", null, "", q.sq.getvwtipGrupListesi("cinsiyet"), false);

            string IdAkademikBirim = frm.EkleComboBox("ParcaEkranArama", "AkademikBirimId", "Akademik Birim", null, "", "AkademikBirimId");

            List<ViewComboCascade> rep = new List<ViewComboCascade>();
            rep.Add(new ViewComboCascade() { Id = "#"+IdAkademikBirim, Parameter = "@AkademikBirimId", Type = ViewComboCascadeType.ComboBox });

            frm.EkleComboBox("ParcaEkranArama", "ProgramId", "Program", null, "", "ProgramId",rep,false);

            string Id = frm.FormEkleDuzenle("ParcaEkranAramaListesi", baslik + " Sonuçları");
            var x = new { oNameSpace= oNameSpace, className= className, funcName= funcName, postData= postData,date=DateTime.Now.Ticks , cokluSecim = cokluSecim };
            string xx= x.toSerialize().AES_encrypt();

            frm.EkleButton("ParcaEkranArama", this.oNamespace, this.oClassname, "ParcaOgrenciArama", "FormData", "ListeId:'"+Id+"',PasData:" + xx.toJavaScriptValue(), "Ara", ViewBtnClass.btn_warning, fa_icons.fa_fa_search_plus);
            q.ajax.html(selector, frm.Olustur());

        }

        public void ParcaOgrenciArama()
        {
            var x = q.request["FormData"].GetJObject()["ParcaEkranArama"];



            var xPostdata = q.request["PasData"].AES_decrypt().GetJObject();
            Ent ent = Ent.get("ogr.vw_OgrenciBilgileri", "Id");
            string[] filtre = new string[] { "KimlikNo", "PasaportNo", "Ad", "Soyad" };
            foreach (string item in filtre)
            {
                if (!x[item].IsNull() )
                    ent.WhereStartWith(item, x[item].toNullStringValue());
            }


            filtre = new string[] { "Cinsiyet", "AkademikBirimId", "ProgramId" };
            foreach (string item in filtre)
            {
                if (!x[item].IsNull())
                    ent.Where(item, x[item].toNullStringValue());
            }


            jqw liste = new jqw(false, "FRM_" + ("#" + q.request["ListeId"] + " #Nesneler").Md5Sum());
            if (xPostdata["cokluSecim"].BoolNullValue(false) == true)
            {
                liste.Ozellikler.selectionmode = jqselectionmode.checkbox;
            }
            else
            {
                liste.AddColumn("Seç", "Sec", "80px", jqcolumntype.button, "", null, q.sq.m(xPostdata["oNameSpace"].toNullStringValue(), xPostdata["className"].toNullStringValue(), xPostdata["funcName"].toNullStringValue(), xPostdata["postData"].toNullStringValue() + ": datarow"), false);
            }

            liste.AddColumn("Id", "KullaniciId", "80px");
            liste.AddDataColumns("KullaniciId", "long");
            liste.AddColumn("Kimlik No", "KimlikNo", "100px");
            liste.AddDataColumns("KimlikNo", "string");

            liste.AddColumn("Pasaport", "PasaportNo", "100px");
            liste.AddDataColumns("PasaportNo", "string");

            liste.AddColumn("Ad", "Ad", "100px");
            liste.AddDataColumns("Ad", "string");

            liste.AddColumn("Soyad", "Soyad", "100px");
            liste.AddDataColumns("Soyad", "string");

            liste.AddColumn("Cinsiyet", "CinsiyetAdi", "80px");
            liste.AddDataColumns("CinsiyetAdi", "string");
        
            liste.AddColumn("Akademik Birim", "AkademikBirimAdi", "200px");
            liste.AddDataColumns("AkademikBirimAdi", "string");

            liste.AddColumn("Program Adı", "ProgramAdi", "200px");
            liste.AddDataColumns("ProgramAdi", "string");

            liste.AddColumn("Burs Adı", "BursAdi", "80px");
            liste.AddDataColumns("BursAdi", "string");

            liste.AddColumn("Aktif Mi?", "AktifMi", "80px",jqcolumntype.checkbox);
            liste.AddDataColumns("AktifMi", "string");

   

            

            liste.AddDataColumns("OgrenciId", "number");
            liste.data = ent.ToListDynamic().ToArray().getJsonArray();
            liste.Ozellikler.height = "620px";
            liste.Ozellikler.disabled = false;
            liste.Ozellikler.filterMode = jqfilterMode.Default;
            liste.Ozellikler.columnsResize = true;
            liste.Ozellikler.enableHover = true;

         
            



            q.ajax.html("#"+q.request["ListeId"] + " #Nesneler",liste.Olustur());



        }

        public void OgrenciAra()
        {
            Newtonsoft.Json.Linq.JObject data = Newtonsoft.Json.Linq.JObject.Parse(q.request["data"]);

            var sql = Ent.get<Ents.vw_Kullanici_OgrenciBilgileri_GroupList>("ogr.vw_Kullanici_OgrenciBilgileri");
            //var sql = new Ent();
            if (data["vw_Kullanici_OgrenciBilgileri"].HasValues)
            {
                if (data["vw_Kullanici_OgrenciBilgileri"]["KimlikNo"].ToString()!="")
                {
                    sql = sql.WhereStartWith("KimlikNo", data["vw_Kullanici_OgrenciBilgileri"]["KimlikNo"].ToString());
                }
                if (data["vw_Kullanici_OgrenciBilgileri"]["PasaportNo"].ToString()!="")
                {
                    sql = sql.WhereStartWith("PasaportNo", data["vw_Kullanici_OgrenciBilgileri"]["PasaportNo"].ToString());
                }
                if (data["vw_Kullanici_OgrenciBilgileri"]["Ad"].ToString()!="")
                {
                    sql = sql.WhereStartWith("Ad", data["vw_Kullanici_OgrenciBilgileri"]["Ad"].ToString());
                }
                if (data["vw_Kullanici_OgrenciBilgileri"]["Soyad"].ToString()!="")
                {
                    sql = sql.WhereStartWith("Soyad", data["vw_Kullanici_OgrenciBilgileri"]["Soyad"].ToString());
                }
                if (data["vw_Kullanici_OgrenciBilgileri"]["Cinsiyet"].ToString()!="")
                {
                    sql = sql.Where("Cinsiyet", data["vw_Kullanici_OgrenciBilgileri"]["Cinsiyet"].ToString());
                } 
            }

            var datalist = sql.ToList<Ents.vw_Kullanici_OgrenciBilgileri_GroupList>();

            var viewList = datalist.GroupBy(t => t.KullaniciId).Select(t => t.FirstOrDefault()).ToList();

            //viewList.ForEach(t => {

//                t.Bolumler = datalist.Where(a => a.OgrenciBolumId != null && a.KullaniciId == t.KullaniciId).ToArray<Ents.vw_Kullanici_OgrenciBilgileri>();
  //          });
             
            string Id = "#"+q.request["ListId"]+ " #Nesneler";



            jqw liste = new jqw(false, "FRM_"+("#" + q.request["ListId"] + " #Nesneler").Md5Sum());
            liste.AddColumn("Id", "KullaniciId", "80px");
            liste.AddDataColumns("KullaniciId", "long");
            liste.AddColumn("Kimlik No", "KimlikNo", "100px");
            liste.AddDataColumns("KimlikNo", "string");

            liste.AddColumn("Pasaport", "PasaportNo", "100px");
            liste.AddDataColumns("PasaportNo", "string");

            liste.AddColumn("Ad", "Ad", "100px");
            liste.AddDataColumns("Ad", "string");

            liste.AddColumn("Soyad", "Soyad", "100px");
            liste.AddDataColumns("Soyad", "string");

            liste.AddColumn("Cinsiyet", "CinsiyetAdi", "80px");
            liste.AddDataColumns("CinsiyetAdi", "string");
            /*
            liste.AddColumn("Akademik Birim", "AkademikBirimAdi", "100px");
            liste.AddDataColumns("AkademikBirimAdi", "string");

            liste.AddColumn("Program Adı", "ProgramAdi", "100px");
            liste.AddDataColumns("ProgramAdi", "string");

            liste.AddColumn("Burs Adı", "BursAdi", "80px");
            liste.AddDataColumns("BursAdi", "string");

            liste.AddColumn("Aktif Mi?", "AktifMi", "80px",jqcolumntype.checkbox);
            liste.AddDataColumns("AktifMi", "string");

    */
            liste.AddDataColumns("Bolumler", "Array");



            //liste.hierarchy("KullaniciId", "OgrenciId");
             
            liste.AddDataColumns("OgrenciId", "number");
            liste.data = viewList.getJsonArray<Ents.vw_Kullanici_OgrenciBilgileri_GroupList>();
            liste.Ozellikler.height = "620px";
            liste.Ozellikler.disabled = false;
            liste.Ozellikler.filterMode = jqfilterMode.Advanced;
            liste.Ozellikler.columnsResize = true;
            liste.Ozellikler.enableHover = true;
            liste.Ozellikler.rowdetails = true;
            //liste.Ozellikler.selectionmode = jqselectionmode.singlecell;

            liste.PostRowDetailsFunc(this.oNamespace, this.oClassname, "OgrenciBolumDetay", "data");
            //liste.AddToolbarButton()

            liste.AddToolbarButton("Düzenle", fa_icons.fa_fa_pencil,  this.m("KullaniciEkleDuzenleme", "ekle:false,datarow:" + liste.getRowDatajs()));
            q.ajax.html(Id, liste.Olustur());
        }
        public void OgrenciBolumDetay(string id=null,string KullaniciId=null)
        {
            var x=jqw.getRowDetails("data");

            if (x == null && id==null)
            {
                q.ajax.alert("msj", "Post Keys Hatası");
            }else{
                if (id == null)
                {
                    id = "#" + x["MaterParentGridId"].ToString() + " #" + x["id"].ToString();
                    KullaniciId = x["data"]["KullaniciId"].ToObject<long>().ToString();
                }
                

                var sql = Ent.get<Ents.vw_Kullanici_OgrenciBilgileri_GroupList>("ogr.vw_OgrenciBilgileri");
                sql.Where("OgrenciId", KullaniciId);

                var datalist = sql.ToList<Ents.vw_Kullanici_OgrenciBilgileri_GroupList>();

                jqw liste = new jqw(true); 
                liste.AddColumn("Id", "OgrenciBolumId", "80px");
                liste.AddDataColumns("OgrenciBolumId", "long");
               /* liste.AddColumn("Kimlik No", "KimlikNo", "100px");
                liste.AddDataColumns("KimlikNo", "string");

                liste.AddColumn("Pasaport", "PasaportNo", "100px");
                liste.AddDataColumns("PasaportNo", "string");

                liste.AddColumn("Ad", "Ad", "100px");
                liste.AddDataColumns("Ad", "string");

                liste.AddColumn("Soyad", "Soyad", "100px");
                liste.AddDataColumns("Soyad", "string");

                liste.AddColumn("Cinsiyet", "CinsiyetAdi", "80px");
                liste.AddDataColumns("CinsiyetAdi", "string");
            */
                liste.AddColumn("Akademik Birim", "AkademikBirimAdi", "200px");
                liste.AddDataColumns("AkademikBirimAdi", "string");

                liste.AddColumn("Program Adı", "ProgramAdi", "200px");
                liste.AddDataColumns("ProgramAdi", "string");

                liste.AddColumn("Burs Adı", "BursAdi", "100px");
                liste.AddDataColumns("BursAdi", "string");

                liste.AddColumn("Aktif Mi?", "AktifMi", "80px", jqcolumntype.checkbox);
                liste.AddDataColumns("AktifMi", "string");



                liste.AddColumn("Ogrencı Numarası", "OgrenciNumarasi", "100px", jqcolumntype.textbox);
                liste.AddDataColumns("OgrenciNumarasi", "string");
                liste.AddColumn("Dal Türü", "DalturuAdi", "100px", jqcolumntype.textbox);
                liste.AddDataColumns("DalturuAdi", "string");



                liste.AddColumn("Kayıt Tarihi", "KayitTarihi", "100px", jqcolumntype.date);
                liste.AddDataColumns("KayitTarihi", "date");



                //liste.hierarchy("KullaniciId", "OgrenciId");

                liste.AddToolbarButton("Bölüm Ekle", fa_icons.fa_fa_plus, this.m("BolumEkleDuzenleme", "OgrenciId:'"+ KullaniciId.ToString() + "', ekle:false,datarow:null,FormId:"+id.toSerializeJson() ));

                liste.AddToolbarButton("Bölüm Düzenle", fa_icons.fa_fa_edit, this.m("BolumEkleDuzenleme", "OgrenciId:'" + KullaniciId.ToString() + "',ekle:false,datarow:" + liste.getRowDatajs() + ",FormId:" + id.toSerializeJson())); 

                liste.AddToolbarButton("Sınıf Düzenle", fa_icons.fa_fa_empire, this.m("SinifDuzenleme", "datarow:" + liste.getRowDatajs()));


                liste.AddDataColumns("OgrenciId", "number");
                liste.data = datalist.getJsonArray<Ents.vw_Kullanici_OgrenciBilgileri_GroupList>();
                liste.Ozellikler.height = "200px";
                liste.Ozellikler.disabled = false;
                liste.Ozellikler.filterMode = jqfilterMode.Advanced;
                liste.Ozellikler.columnsResize = true;
                liste.Ozellikler.enableHover = true;
                liste.Ozellikler.rowdetails = false;

                //liste.Ozellikler.selectionmode = jqselectionmode.singlecell;



                q.ajax.html(id, liste.Olustur());

            }

        }
        public void SinifDuzenleme()
        {
            if (q.request.ContainsKey("datarow"))
            {
                string id = q.request["datarow"];
                id = id.GetJObject()["OgrenciBolumId"].IsNullValueString() == null ? "0" : id.GetJObject()["OgrenciBolumId"].IsNullValueString().ToString();
                SinifController sinifController = new SinifController();
                sinifController.SinifDuzenleme(long.Parse(id));
            }
            else
            {
                q.ajax.alert("hata", "Bölüm Seçmediniz.");
            }
        }
        public void KullaniciEkleDuzenleme()
        {
            KullanciController knt = new KullanciController();
            knt.KullaniciEkleDuzenleme(true); 
        }
        public void BolumEkleDuzenleme()
        {


            FormModelGenerator frm = new FormModelGenerator();
            string OgrenciId = q.request["OgrenciId"];
            string FormId = q.request["FormId"];
            string id = q.request.ContainsKey("datarow") ? q.request["datarow"] : "";
            if (id.Length > 0)
            {
                id = id.GetJObject()["OgrenciBolumId"].IsNullValueString() == null ? "0" : id.GetJObject()["OgrenciBolumId"].IsNullValueString().ToString();
            }
            else
                id = "0";

            var sql = Ent.get<Ents.vw_OgrenciBilgileri>().Where("OgrenciBolumId", id).FirstOrDefault<Ents.vw_OgrenciBilgileri>();
            if (sql == null)
                sql = new Ents.vw_OgrenciBilgileri();







            frm.FormEkleDuzenle("OgrenciBolum", "Bölüm Bilgileri");


            string Ids = frm.EkleComboBox("OgrenciBolum", "BirimId", "Program Adı", sql.ProgramId.ToString(), sql.AkademikBirimAdi + " - " + sql.ProgramAdi, "OgrenciBirim",null,true

                ); //, q.con.CollectionFromSql("SELECT a.AkademikBirimAdi + ' - '+ a.ProgramAdi AS label,a.ProgramId id FROM br.vw_FakulteProgramListesi a").ToArray().getJsonArray(), true);
             
            List<ViewComboCascade> cas = new List<ViewComboCascade>();

            cas.Add(new ViewComboCascade() { Id = "#" + Ids, Parameter = "@ProgramId", Type = ViewComboCascadeType.ComboBox });

            frm.EkleComboBox("OgrenciBolum", "BirimKodlariId", "Program Burs Oranı", sql.BirimKodlariId.toNullStringValue(), sql.BursAdi, "BirimKodlariId", cas);



            frm.EkleDropDownList("OgrenciBolum", "DalturuId", "Dal Türü", sql.DalturuId.toNullStringValue(),sql.DalturuAdi,q.sq.getvwtipGrupListesi("dalturu"), true);
            frm.EkleDropDownList("OgrenciBolum", "KayitTipiId", "Kayıt Türü", sql.KayitTipiId.toNullStringValue(),sql.KayitTipiAdi,q.sq.getvwtipGrupListesi("kayitturu"), true);


            frm.EkleTextBoxDate("OgrenciBolum", "KayitTarihi", "Kayıt Tarihi", sql.KayitTarihi.toSerialize(), ViewDateFormat.Date,false);

            frm.EkleDropDownList("OgrenciBolum", "KayitYilId", "Kayıt Yılı", sql.KayitYilId.toNullStringValue(),sql.KayitYilAdi,q.sq.getvwtipGrupListesi("yillar"), true);
            frm.EkleDropDownList("OgrenciBolum", "MaliYilId", "Mali Yılı", sql.MaliYilId.toNullStringValue(),sql.MaliYilAdi, q.sq.getvwtipGrupListesi("yillar"), true);
            frm.EkleDropDownList("OgrenciBolum", "EgitimYilId", "Egitim Başlanğıç Yılı", sql.EgitimYilId.toNullStringValue(),sql.EgitimYilAdi, q.sq.getvwtipGrupListesi("yillar"), true);

            frm.EkleTextBox("OgrenciBolum", "Sube", "Şube", sql.Sube, 50);
            string OgrId=frm.EkleTextBox("OgrenciBolum", "OgrenciNumarasi", "Öğrenci Numrası", sql.OgrenciNumarasi, 10,false);
           

            frm.EkleCheckBox("OgrenciBolum", "AktifMi", "Aktif Mi?", sql.AktifMi, true);

            frm.EkleCheckBox("OgrenciBolum", "OzelOgrenciMi", "Özel Ögrenci Mi?", sql.OzelOgrenciMi.BoolNullValue(false), true);

            frm.FormEkleDuzenle("OgrenciBolumSave", "");
            frm.EkleButton("OgrenciBolumSave", this.oNamespace, this.oClassname, "OgrenciNumrasiOlustur", 
                "OgrId: " + OgrId.toSerializeJson() + ",FormData", "KullaniciId:" + OgrenciId +",OgrenciBolumId:"+id + ",FormId : "+ FormId.toSerializeJson(), "Öğrenci Numarası Oluştur", ViewBtnClass.btn_danger, fa_icons.fa_fa_digg);
            frm.EkleButton("OgrenciBolumSave", this.oNamespace, this.oClassname, "BolumEkleDuzenlemeKaydet", "FormData", "KullaniciId:" + OgrenciId + ",OgrenciBolumId:" + id + ",FormId : " + FormId.toSerializeJson(), "Kaydet", ViewBtnClass.btn_success, fa_icons.fa_fa_save);

            q.ajax.dialogOpen("KullaniciDuzenleme", "KullaniciDuzenleme", "Öğrenci Bölüm Ekleme - Düzenleme", frm.Olustur());
        }
        public void OgrenciNumrasiOlustur()
        {
            var x = q.request["FormData"].GetJObject();
            string BirimId = x["OgrenciBolum"]["BirimId"].toNullStringValue();
            string KayitYilId = x["OgrenciBolum"]["KayitYilId"].toNullStringValue();
            if (BirimId == "" || BirimId == null || KayitYilId == null || KayitYilId == "")
            {
                q.ajax.alert("hata", "Bölüm veya Kayıt Yılı Bilgisi Girilmemiştir.");
            }
            else
            {
                var par = new Dictionary<string, object>();
                par.Add("@p1", BirimId);
                par.Add("@p2", KayitYilId);
                string ogr =q.con.CollectionFromSql("SELECT ogr.fn_OgrenciNumarasiOlustur(@p1,@p2) AS OgrNum", par).FirstOrDefault().OgrNum;
                q.ajax.AddFnc("aa", "$('#OgrId').val('@OgrVal')".Replace("OgrId", q.request["OgrId"].ToString()).Replace("@OgrVal", ogr)); 
            }
        }
        public void BolumEkleDuzenlemeKaydet()
        {

            Newtonsoft.Json.Linq.JObject obj = Newtonsoft.Json.Linq.JObject.Parse(q.request["FormData"]);


          

            string Id = q.request.ContainsKey("KullaniciId")?q.request["KullaniciId"]:"0";
            string OgrenciBolumId = q.request["OgrenciBolumId"];


            if (obj["OgrenciBolum"]["BirimId"].IsNull() || obj["OgrenciBolum"]["BirimKodlariId"].IsNull() )
            {
                q.ajax.alert("hata", "Program veya Bursluluk Bilgileri Boş Geçilemez.");
                return;

            }
            if (obj["OgrenciBolum"]["AktifMi"].ToObject<bool>()==true  && obj["OgrenciBolum"]["KayitTarihi"].IsNull())
            {
                q.ajax.alert("hata", "Aktif Bir Programın Kayıt Tarihi Boş Olamaz.");
                return;

            }



            Ent klc = Ent.get("ogr.OgrenciBolum"); //obj["Kullanici"].ToString().unSerialize<Ents.Kullanici>();
            if (OgrenciBolumId != "0")
                klc = klc.Where("Id", OgrenciBolumId).FirstOrDefault<Ent>();
            else
                klc = klc.Create();

      


            klc.SetJoject(obj["OgrenciBolum"]);

            klc["OgrenciId"] = Id;


            using (TransactionScope ctx = new TransactionScope())
            {

                try
                {
                    if (klc.Save())
                    {


                        q.ajax.alert("Kaydedildi", "Kaydedildi");
                        q.ajax.dialogClose("close", "KullaniciDuzenleme");

                        this.OgrenciBolumDetay(q.request["FormId"], Id);
                        ctx.Complete();
                    }

                }
                catch (Exception ex)
                {
                    ctx.Dispose();
                    q.ajax.alert("KaydedildiHata", "Kayıt sırasında bir problem oluştu.\n" + ex.ToString());
                }

            }
        }
        public static Ents.vw_OgrenciBilgileri GetVw_OgrenciBilgileri(long id)
        {
            //Ents.vw_OgrenciBilgileri b = q.con.Database.SqlQuery<Ents.vw_OgrenciBilgileri>("SELECT * FROM  ogr.vw_OgrenciBilgileri WHERE OgrenciBolumId="+id.ToString(), new object[0]).FirstOrDefault(); // Ent.get<Ents.vw_OgrenciBilgileri>().Where("OgrenciBolumId", id).FirstOrDefault<Ents.vw_OgrenciBilgileri>();


            Ents.vw_OgrenciBilgileri b  =Ent.get<Ents.vw_OgrenciBilgileri>().Where("OgrenciBolumId", id).FirstOrDefault<Ents.vw_OgrenciBilgileri>();

            return b;
        }
        public static string GetOgrenciBilgileri(long id, bool html = false,string selector="")
        {
            Ents.vw_OgrenciBilgileri b = GetVw_OgrenciBilgileri(id); //Ent.get<Ents.vw_OgrenciBilgileri>().Where("OgrenciBolumId", id).FirstOrDefault<Ents.vw_OgrenciBilgileri>();

            FormModelGenerator frm = new FormModelGenerator(); 
            frm.FormEkleDuzenle("OgrenciBolumBilgileriViews", "Öğrenci Bölüm Bilgileri");
            frm.EkleLabelBox("OgrenciBolumBilgileriViews", "Adı - Soyadı", b.Ad + " - " + b.Soyad);
            frm.EkleLabelBox("OgrenciBolumBilgileriViews", "Öğrenci Numarası", b.OgrenciNumarasi);
            frm.EkleLabelBox("OgrenciBolumBilgileriViews", "Kimlik No", b.KimlikNo);

            frm.EkleLabelBox("OgrenciBolumBilgileriViews", "Akademik Birim - Program - Burs Adı", b.AkademikBirimAdi +" - "+b.ProgramAdi +" - " + b.BursAdi);
            frm.EkleLabelBox("OgrenciBolumBilgileriViews", "Kayıt Türü", b.KayitTipiAdi);
            frm.EkleLabelBox("OgrenciBolumBilgileriViews", "Kayıt Tarihi", b.KayitTarihi.GetFormatViewDate());
            frm.EkleLabelBox("OgrenciBolumBilgileriViews", "Aktif Mi?", b.AktifMi ? "Evet" : "Hayir");
            frm.EkleLabelBox("OgrenciBolumBilgileriViews", "Özel Öğrenci Mi?", (b.OzelOgrenciMi??false) ? "Evet" : "Hayir");
            frm.EkleLabelBox("OgrenciBolumBilgileriViews", "Dal Türü", b.DalturuAdi);
            
            frm.EkleLabelBox("OgrenciBolumBilgileriViews", "Öğrenci Bölüm Id", b.OgrenciBolumId.ToString());
            if (html)
                return frm.Olustur();

            string htmlView = frm.Olustur();
            q.ajax.html(selector, htmlView);
            return htmlView;
             
        }

    }
}

