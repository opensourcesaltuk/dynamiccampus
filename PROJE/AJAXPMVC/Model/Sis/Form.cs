﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AJAXPMVC.Ents
{
    [TableAttribute(Scheme = "fm.")]
    public class Form : Ent
    {
        [TableAttribute(Primary = true, Identity = true)]
        public long Id { get; set; }
        public string FormAdi { get; set; }
        public bool AktifMi { get; set; }
    }
}