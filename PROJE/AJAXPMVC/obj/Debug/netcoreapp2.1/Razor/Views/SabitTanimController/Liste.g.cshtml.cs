#pragma checksum "D:\FILE\NET\payhs-net-core\PROJE\AJAXPMVC\Views\SabitTanimController\Liste.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2ae3866b8c84cfd044284a84dd48db18f64eb451"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_SabitTanimController_Liste), @"mvc.1.0.view", @"/Views/SabitTanimController/Liste.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/SabitTanimController/Liste.cshtml", typeof(AspNetCore.Views_SabitTanimController_Liste))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "D:\FILE\NET\payhs-net-core\PROJE\AJAXPMVC\Views\_ViewImports.cshtml"
using AJAXPMVC;

#line default
#line hidden
#line 2 "D:\FILE\NET\payhs-net-core\PROJE\AJAXPMVC\Views\_ViewImports.cshtml"
using AJAXPMVC.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2ae3866b8c84cfd044284a84dd48db18f64eb451", @"/Views/SabitTanimController/Liste.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"eb5e7d7ffa74bb0c76c79aeed062832840fb27e3", @"/Views/_ViewImports.cshtml")]
    public class Views_SabitTanimController_Liste : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            BeginContext(0, 402, true);
            WriteLiteral(@"<div class=""col-md-12"">
    <div class=""box box-primary"" id=""SabitTanim"">
        <div class=""box-header with-border"">
            <h3 class=""box-title"">Düzenlemek İstediğiniz Tanımı Listeden Seçiniz.</h3>
        </div>
        <div class=""box-body"">
            <div id=""TanimListesi"">

            </div>
        </div>
    </div>
</div>
<script>
    $$.SabitTanimlar = {
        data:");
            EndContext();
            BeginContext(403, 52, false);
#line 15 "D:\FILE\NET\payhs-net-core\PROJE\AJAXPMVC\Views\SabitTanimController\Liste.cshtml"
        Write(Html.Raw(q.viewData["SabitTanim"].toSerializeJson()));

#line default
#line hidden
            EndContext();
            BeginContext(455, 1879, true);
            WriteLiteral(@",
        id:""#SabitTanim #TanimListesi"",
        load: function () {

            var source =
                { 
                    localdata: $$.SabitTanimlar.data,
                    datatype: ""array"",
                    datafields:
                    [
                        { name: 'Id', type: 'double' },
                        { name: 'SayfaAdi', type: 'string' }
                    ]
                };

            var dataAdapter = new $.jqx.dataAdapter(source);
            $($$.SabitTanimlar.id).jqxGrid(
                {
                    width: '80%',
                    source: dataAdapter,
                    editable: false,
                    enabletooltips: true,
                    selectionmode: 'multiplecellsadvanced',
                    showfilterrow: true,
                    filterable: true,
                    sortable: true,
                    pageable: true,
                    altrows: true,
                    pagesize: 20,
                 ");
            WriteLiteral(@"   pagesizeoptions: ['20', '50', '100'],
                    columns: [
                        {
                            text: 'Sayfa Adı', datafield: 'SayfaAdi', width:'80%'
                        },
                        {
                            text: 'Düzenle', datafield: 'Edit', columntype: 'button', width: '80px',
                            cellsrenderer: function () {
                                return ""Düzenle"";
                            },
                            buttonclick: function (row) { 
                                var dataRecord = $($$.SabitTanimlar.id).jqxGrid('getrowdata', row);
                                var data = dataRecord.Id;
                                var TabDivId = $$.MainTab.add(true, """", ""Sabit Tanım Düzenle - ""+dataRecord.SayfaAdi); 
                                ");
            EndContext();
            BeginContext(2335, 40, false);
#line 58 "D:\FILE\NET\payhs-net-core\PROJE\AJAXPMVC\Views\SabitTanimController\Liste.cshtml"
                           Write(Html.Raw(q.viewData["fncSabitTanimlar"]));

#line default
#line hidden
            EndContext();
            BeginContext(2375, 169, true);
            WriteLiteral(";\r\n                            }\r\n                        }\r\n                    ]\r\n                });\r\n\r\n\r\n\r\n        }\r\n    };\r\n    $$.SabitTanimlar.load();\r\n</script>");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
