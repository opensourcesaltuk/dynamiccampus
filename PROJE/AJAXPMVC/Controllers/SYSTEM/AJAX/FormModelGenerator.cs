﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using System.ComponentModel;
using static conf;

namespace AJAXPMVC
{  
    public class FormModelGenerator : MainClass
    {
        public new string oTema = "System/FromModelGenerator";
        public Dictionary<string, ViewForm> Model = new Dictionary<string, ViewForm>(); 
        public Dictionary<string, ViewNesne> ViewNesne = new Dictionary<string, ViewNesne>();
        public List<ViewButton> btnler = new List<ViewButton>();
        public string Id { get; set; }
        public FormModelGenerator() { this.Id = NewID(); }
        public string NewID()
        {
            return "FRM_"+Guid.NewGuid().ToString().Md5Sum().Replace("-","").ToUpper();
        }
        public string FormEkleDuzenle(string Model, string FormBaslik)
        {
            ViewForm frm = new ViewForm {Model=Model,FormBaslik=FormBaslik,Id=NewID() };
            if (this.Model.Keys.Contains(Model))
            {
                this.Model.Add(Model, frm);
            }
            else
                this.Model[Model] = frm;
            return frm.Id;
        }
        public string EkleTextBox(string model, string key, string baslik, string val, int? maxlenght=null,bool Zorunlu=false)
        {
            ViewNesne nesne = new ViewNesne() { Form = model, datakey = key, baslik = baslik, defaultval = val, maxlenght = maxlenght,Zorunlu= Zorunlu };
            nesne.id = nesne.name = NewID();
            ViewNesne.Add(key, nesne);
            return nesne.id;
        }

        public string EkleLabelBox(string model, string baslik, string val, string key="")
        {
            if (key=="")
            {
                key = NewID();
            }
            ViewNesne nesne = new ViewNesne() { Form = model, datakey = key, baslik = baslik, defaultval = val, maxlenght = 0,Zorunlu= false };
            nesne.id = nesne.name = NewID();
            nesne.Tip = "Label";
        
            ViewNesne.Add(key, nesne);
            return nesne.id;
        }
        internal string EkleCheckBox(string model, string key, string baslik, bool? aktifMi, bool Zorunlu = false)
        {
            ViewNesne nesne = new ViewNesne() { Form = model, datakey = key, baslik = baslik, defaultval = aktifMi.toSerializeJson() ,Zorunlu = Zorunlu };
            nesne.id = nesne.name = NewID();
            nesne.Tip = "CheckBox";
            ViewNesne.Add(key, nesne);
            return nesne.id;
        }
        public string EkleTextBoxDate(string model, string key, string baslik, string val,  ViewDateFormat viewDateFormat=ViewDateFormat.Date, bool Zorunlu = false)
        {
            ViewNesne nesne = new ViewNesne() { Form = model, datakey = key, baslik = baslik, defaultval = val ,Zorunlu=Zorunlu};
            switch (viewDateFormat)
            {
                case ViewDateFormat.DateTime:
                    nesne.format = "DateTime";
                    break;
                case ViewDateFormat.Date:
                    nesne.format = "Date";
                    break;
                case ViewDateFormat.Time:
                    nesne.format = "Time";
                    break;
            }
            nesne.id = nesne.name = NewID();
            nesne.Tip = "TextBoxDate";
            ViewNesne.Add(key, nesne);
            return nesne.id;
        }

       

        public string EkleTextBoxDec(string model, string key, string baslik, decimal? val = null, int? digit = 5, int? dec = 2, int? min = 0, int? max = 10000000, bool Zorunlu = false)
        {
            ViewNesne nesne = new ViewNesne() { Form = model, datakey = key, baslik = baslik, defaultval = val.HasValue ? val.ToString() : null, digit = digit, dec = dec, min = min, max = max, Zorunlu = Zorunlu };
            nesne.id = nesne.name = NewID();
            nesne.Tip = "TextBoxDec";
            ViewNesne.Add(key, nesne);
            return nesne.id;
        }
        public string EkleDropDownList(string model, string key, string baslik, string val, string valuetext, Newtonsoft.Json.Linq.JArray liste, bool Zorunlu = false)
        {
            ViewNesne nesne = new ViewNesne() { Form = model, datakey = key, baslik = baslik, defaultval = val, defaulttext = valuetext, liste = liste, Zorunlu = Zorunlu };
            nesne.id = nesne.name = NewID();
            nesne.Tip = "DropDownList";
            ViewNesne.Add(key, nesne);
            return nesne.id;
        }
        public string EkleComboBox(string model, string key, string baslik, string val, string valuetext,string ComboBoxEvent,List<ViewComboCascade> list=null, bool Zorunlu = false)
        {

            ViewNesne nesne = new ViewNesne() { Form = model, datakey = key, baslik = baslik, defaultval = val,defaulttext= valuetext, Zorunlu = Zorunlu };
            nesne.id = nesne.name = NewID();
            string fnc = "null";
            string extBind = "";

            if (list != null)
            {

                fnc = "";
                for (int i = 0; i < list.Count; i++)
                {
                    switch(list[i].Type)
                    {
                        default:
                            list[i].Parameter = q.sessionaes.AES_encrypt(list[i].Parameter);
                            fnc += "{Par:" + list[i].Parameter.toSerializeJson() + ", Val : $(" + list[i].Id.toSerializeJson() + ").val()},";
                            break;
                    }
                    extBind += @"
                            $('"+list[i].Id+ @"').unbind('change close').bind('change close',function(){
                                $('#" + nesne.id + @"').jqxComboBox('clearSelection'); 
                                $('#" + nesne.id + @"').jqxComboBox('clear'); 
                            });

                        ";

                }
                fnc = @"function(){
                        return JSON.stringify(["+ fnc + @"])
                     }";
            }
            nesne.fncComboBox= q.ajax.set_combobox("asdf", ComboBoxEvent, "#"+nesne.id, true, val, valuetext,fnc,baslik);
            nesne.fncComboBoxBind = extBind;
            nesne.Tip = "ComboBox";
            ViewNesne.Add(key, nesne);
            return nesne.id;
        }

        public string EkleAramaListesiNokta(string Model, string Key, string Text, string Value, string ValueText, bool Zorunlu, FormAramaListesiNoktasi araList)
        {

            ViewNesne nesne = new ViewNesne() { Form = Model, datakey = Key, baslik = Text, defaultval = Value, defaulttext = ValueText, Zorunlu = Zorunlu };
            nesne.id = nesne.name = NewID();
            nesne.Tip = "ThreeDot";
            araList.ElemanId = "#" + nesne.id;
            araList.NesneBaslik = Text;
            string Data = "aralist : " + araList.toSerializeJson().AES_encrypt().toJavaScriptValue();

            nesne.fncComboBox = araList.m("ThreeDotViewList", Data); //q.sq.m(oNamespace, "FormAramaListesiNoktasi", "asdf", "", "#" + nesne.id, true, val, valuetext, fnc, baslik);

            ViewNesne.Add(Key, nesne);
            return nesne.id;



        }

        public void EkleButton(string Model,string oNamespace, string oClassname, string func, string postKey,string PostData, string btnname, ViewBtnClass btntip, fa_icons? btnfaicon = null)
        {
            if (PostData.Length > 0)
            {
                PostData = ", " + PostData;
            }
            ViewButton button = new ViewButton()
            {
                func = q.sq.m(oNamespace, oClassname, func, postKey + ": FromData, Id:'" + this.Id + "' "+PostData,mGetPost.POST,"btnObject"),
                bntclass = btntip.GetDescription(),
                btnicon = btnfaicon.HasValue ? btnfaicon.GetDescription() : "",
                btnPostKey= postKey,
                btnname=btnname,
                Model=Model
                
            };
            btnler.Add(button);
        }
        public void EkleButtonFnc(string Model, string func, string btnname, ViewBtnClass btntip, fa_icons? btnfaicon = null)
        {
             
            ViewButton button = new ViewButton()
            {
                func = func,
                bntclass = btntip.GetDescription(),
                btnicon = btnfaicon.HasValue ? btnfaicon.GetDescription() : "",
                btnPostKey= "",
                btnname=btnname,
                Model= Model

            };
            btnler.Add(button);
        }
        public string Olustur()
        {
            string[] keylist = Model.Keys.ToArray();
            for (int i = 0; i < keylist.Length; i++)
            {
                Model[keylist[i]].Nesneler = ViewNesne.Values.Where(t => t.Form == keylist[i]).ToArray();
                Model[keylist[i]].Buttonlar = btnler.Where(t => t.Model == keylist[i]).ToList();
            }
            q.viewData["Model"] = Model.Values.ToArray();
            q.viewData["Buttons"] = null;
            q.viewData["Id"] = this.Id;
            string html = this.getPageHtml("model");
            return html;

        }

    }
    public class ViewForm
    {
        public string Model, FormBaslik, Id;
        public ViewNesne[] Nesneler;
        public List<ViewButton> Buttonlar;
    }
   
    public class ViewComboCascade
    {
        public string Parameter, Id;
        public ViewComboCascadeType Type;
    }
    public enum ViewComboCascadeType
    {
        [Description("ComboBox")]
        ComboBox = 1,
        [Description("DropDownList")]
        DropDownList = 2 
    }
    public enum ViewBtnClass
    {
        [Description("btn-default")]
        btn_default = 1,
        [Description("btn-primary")]
        btn_primary = 2,
        [Description("btn-success")]
        btn_success = 3,
        [Description("btn-info")]
        btn_info = 4,
        [Description("btn-danger")]
        btn_danger = 5,
        [Description("btn-warning")]
        btn_warning = 6
    }
    public class ViewNesne
    {
        public string Form, datakey, defaultval, format, baslik, id, name;
        public int? maxlenght, digit, dec, min, max;
        public string Tip = "TextBox";  
        public JArray liste;
        public string defaulttext;
        public string fncComboBox { get; internal set; }
        public string fncComboBoxBind { get; internal set; }
        public bool Zorunlu = false;
    }
    public class ViewButton
    {
        public string func, btnname, btntip, btnId, btnPostKey, btnicon,Model;
        public string bntclass;
        public string icon;
    }
}