﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AJAXPMVC.Ents
{
    [TableAttribute(Scheme = "tpl.")]
    public class SabitTanim : Ent
    {
        [TableAttribute(Primary = true, Identity = true)]
        public int Id { get; set; }
        public string SayfaAdi { get; set; }
        public int TipGrupId { get; set; }
        public bool SuperUser { get; set; }
        public string AdColAdi { get; set; }
        public int AdColUzunluk { get; set; }
        public string ExtraKodColAdi { get; set; }
        public int ExtraKodColUzunluk { get; set; }
        public string ExtraKod2ColAdi { get; set; }
        public int ExtraKod2ColUzunluk { get; set; }
        public string BasTarihColAdi { get; set; }
        public bool BasTarihColUzunluk { get; set; }
        public string BitTarihColAdi { get; set; }
        public bool BitTarihColUzunluk { get; set; }
        public string BasDecColAdi { get; set; }
        public string BasDecColUzunluk { get; set; }
        public string BitDecColAdi { get; set; }
        public string BitDecColUzunluk { get; set; }
        public bool AktifMi { get; set; }
        public bool TipUstGrupAktifMi { get; set; }
        public int? BasDecColTipGrupId { get; set; }
        public int? BitDecColTipGrupId { get; set; } 
        public string AciklamaColAdi { get; set; }
    }

    [TableAttribute(Scheme = "tpl.")]
    public class Tip : Ent
    {
        [TableAttribute(Primary = true, Identity = true)]
        public long Id { get; set; }
        public long? TipId { get; set; }
        public int TipGrupId { get; set; }
        public string Ad { get; set; }
        public string Aciklama { get; set; }
        public string ExtraKod { get; set; }
        public string ExtraKod2 { get; set; }
        public DateTime? BasTarih { get; set; }
        public DateTime? BitTarih { get; set; }
        public long KullaniciId { get; set; }
        public decimal? BasDec { get; set; }
        public decimal? BitDec { get; set; }
    }

    public class vw_TipUst : Tip
    { 
        public string UstTipAdi { get; set; }
        public string BasDecAd { get; set; }
        public string BitDecAd { get; set; }
        public bool Edit = false;

    }

    [TableAttribute(Scheme = "tpl.")]
    public class TipGrup : Ent
    {
        [TableAttribute(Primary = true, Identity = true)]
        public int Id { get; set; }
        public int? TipGrupId { get; set; }
        public string GrupAdi { get; set; }
        public string GrupKodu { get; set; }
        public string GrupAciklamasi { get; set; }
        public long? KullaniciId { get; set; }
        public int GrupSira { get; set; }
        public bool AktifMi { get; set; }
    }


}