﻿


var Base64 = {

    // private property
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

    // public method for encoding
    encode: function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = Base64._utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
                this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
                this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

        }

        return output;
    },

    // public method for decoding
    decode: function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

        }

        output = Base64._utf8_decode(output);

        return output;

    },

    // private method for UTF-8 encoding
    _utf8_encode: function (string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },

    // private method for UTF-8 decoding
    _utf8_decode: function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while (i < utftext.length) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }

        }

        return string;
    }

}
/**
* Tarih Formatları
*/
Date.prototype.format = function (format) {
    format = format || "yyyy.MM.dd";
    var o = {
        "M+": this.getMonth() + 1,
        "d+": this.getDate(),
        "h+": this.getHours(),
        "m+": this.getMinutes(),
        "s+": this.getSeconds(),
        "q+": Math.floor((this.getMonth() + 3) / 3),
        "S": this.getMilliseconds()
    }

    if (/(y+)/.test(format)) format = format.replace(RegExp.$1,
        (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o) if (new RegExp("(" + k + ")").test(format))
        format = format.replace(RegExp.$1,
            RegExp.$1.length == 1 ? o[k] :
                ("00" + o[k]).substr(("" + o[k]).length));
    return format;
} 
Date.prototype.getDayNumbers = function (getDayNumbers) {
    var weekday = new Array(7);
    weekday[0] = 7;
    weekday[1] = 1;
    weekday[2] = 2;
    weekday[3] = 3;
    weekday[4] = 4;
    weekday[5] = 5;
    weekday[6] = 6;
    return weekday[this.getDay()];
}

$$.dialog = {
    dialogsayac: 0,
    dialog: {},
    dialogDiv: null,
    gvBaslat: null,
    gvList: null,
    gvBaslatYuk: 35,
    gvHeaderBaslik: 50,
    gvHeaderBaslikSelector: 50,
    TumuIndir: false,
    SonIndex: 9000,
    leftObject: null,
    getId: function (id) {
        return 'd_' + Base64.encode(id).replace(/=/g, '_') + '_d';
    },
    load: function (gorev, header, footer, leftObject) { 
        if (!$(header)) {
            header = $('<div></div>');
        }
        try {
            $$.dialog.leftObject = $(leftObject);
        } catch (e) {

        }
        $$.dialog.gvHeaderBaslikSelector = header;
        $$.dialog.gvHeaderBaslik = $($$.dialog.gvHeaderBaslikSelector).height() || 0;
        this.dialogDiv = $('<div style="display: none;"></div>');
        this.gvBaslat = $('<div id="gvBaslat" style="overflow-y: auto ;padding:0 0 0 0 !important;">' +
            '<div class="col-md-12" id="gvList"></div>' +
            '<div style="position: fixed;right: 10px;">' +
            '<i class="fa fa-minus" aria-hidden="true" id="gvBaslatKucult" style="color: white">&nbsp;</i>' +
            '<i class="fa fa-times" aria-hidden="true" id="gvBaslatKapat" style="color: white">&nbsp;</i>' +
            '</div></div>');
        this.gvList = this.gvBaslat.find('#gvList');
        this.gvBaslat.find('#gvBaslatKucult').hide();
        this.gvBaslat.find('#gvBaslatKucult').bind('click', function () {
            //setTimeout($$.dialog.TumuIndirKaldir,100);

            $.when($$.dialog.TumuIndirKaldir()).done($.fn.delay('', 150));
        })
        this.gvBaslat.find('#gvBaslatKapat').bind('click', function () {
            $$.dialog.TumuKapat();
        })

        $('body').append(this.dialogDiv);
        $(footer).append(this.gvBaslat);
        //gorev=gorev||true;
        if (gorev == false) {
            this.gvBaslat.hide();
            $$.dialog.gvBaslatYuk = 0;
        }
        $$.dialog.gvBaslatYuk = $('#mainfooter').outerHeight() + 2;

        //$(this.gvList).jqxSortable();
    },
    deleteDialog: function (id) {
        try {
            var gid = this.getId(id);
            $$.dialog.dialog[gid].nesne.remove();
            $$.dialog.dialog[gid].gorev.remove();
            delete $$.dialog.dialog[gid];
        } catch (e) {

        }
    },
    getKullanilirAlan: function () {
        return { width: ($(document).width()) - (4) - ($($$.dialog.leftObject).width()), height: $(window).height() - ($$.dialog.gvBaslatYuk + ($$.dialog.gvHeaderBaslik || 0)) };
    },
    SetFullScreen: function (obj) {
        //$$.dialog.SetFullScreen($$.dialog.dialog[gid]);

        obj.fullscreen = !obj.fullscreen;

        if (obj.fullscreen) {
            obj.getLastScreen = { width: $(obj.nesne).jqxWindow("width"), height: $(obj.nesne).jqxWindow("height") };
            var x = this.getKullanilirAlan();
            x.position = 'center';
            $(obj.nesne).jqxWindow(x);

        } else {
            var x = this.getKullanilirAlan();
            var y = obj.getLastScreen;
            if (x.width > y.width) { x.width = y.width; }
            if (x.height > y.height) { x.height = y.height; };
            x.position = 'center';
            x.maxHeight = y.maxHeight;
            x.maxWidth = y.maxWidth;
            $(obj.nesne).jqxWindow(x);
        }
    },
    CreateDialog: function (id, title, html, object) {
        var gid = this.getId(id);
        if (($$.dialog.dialog[gid] || null) != null) {
            this.deleteDialog(id);
        }
        $$.dialog.dialog[gid] = { id: id, title: title, html: html, object: object, nesne: {}, nesneTitle: null, nesnePanel: null, gorev: null };

        $$.dialog.dialog[gid].nesne = $('<div id="' + id + '" dialogName="' + id + '"><div id="windowHeader"><span><span id="dialogTitleSBaslik"></span></span></div><div style=" padding: 0 !important; margin-right: 4px;" id="windowContent"><div id="dialogTitleIcerik" class="col-md-12" style="padding-top: 15px;"></div></div></div>');

        object = object || {};
        var x = this.getKullanilirAlan();
        //  alert($(document).width());
        object.width = object.width || x.width;
        object.height = object.height || x.height;
        object.fullscreenClick = object.fullscreenClick || false;
        object.keyboardCloseKey = 99999;
        object.fullscreen = object.fullscreen || false;
        object.autoOpen = typeof (object.autoOpen) == "undefined" ? true : false;
        object.addGorev = typeof (object.addGorev) == "undefined" ? true : false;
        $$.dialog.dialog[gid].nesneTitle = $($$.dialog.dialog[gid].nesne).find('#dialogTitleSBaslik');
        $$.dialog.dialog[gid].nesnePanel = $($$.dialog.dialog[gid].nesne).find('#dialogTitleIcerik');
        if (object.fullscreenClick === true)
            $($$.dialog.dialog[gid].nesne).find('#windowHeader').bind('dblclick', function () {
                $$.dialog.SetFullScreen($$.dialog.dialog[gid]);
            });


        this.setTitle(id, title);

        this.setHtml(id, html);

        $(this.dialogDiv).append($$.dialog.dialog[gid].nesne);

        $($$.dialog.dialog[gid].nesne).jqxWindow({
            showCollapseButton: true,
            maxHeight: x.height,
            maxWidth: x.width,
            height: object.height,
            keyboardCloseKey: 255,
            keyboardNavigation:false,
            width: object.width,
            animationType: 'none',
            collapseAnimationDuration: 0,
            closeAnimationDuration: 0,
            zIndex: 17000,
            showAnimationDuration: 0,
            position: 'center',
            theme: 'highcontrast',
            dragArea: {
                left: ($($$.dialog.leftObject).width()),
                top: $$.dialog.gvHeaderBaslik || 0,
                width: x.width,
                height: x.height
            },
            initContent: function () {
                //setTimeout(fnc,100);

            }
        });


        $($$.dialog.dialog[gid].nesne).bind('close', function (event) {
            //return ;
            if ((window.event || -3) !== -3) {
                $$.dialog.deleteDialog($$.dialog.dialog[gid].id);
                // return;
            } else {
                //  alert(1);
                $($$.dialog.dialog[gid].gorev).find('#btntitle').html(" [ " + $$.dialog.dialog[gid].title + "]");

            }


        });
        debugger;
        $($$.dialog.dialog[gid].nesne).find('.jqx-window-close-button').bind('click', function (event) {
            $$.dialog.deleteDialog($$.dialog.dialog[gid].id);


        });
        if (object.addGorev == true) {

            $($$.dialog.dialog[gid].nesne).bind('collapse', function (event) {

                window.event = -3;
                $(this).jqxWindow('close');
            });
            $($$.dialog.dialog[gid].nesne).bind('expand', function (event) {
                // $($$.dialog.dialog[gid].gorev).find('#btntitle').html(" "+$$.dialog.dialog[gid].title);

                window.event = -4;
            });
        }

        if (object.autoOpen == true && object.addGorev == true) {
            //console.log($($$.dialog.dialog[gid].nesne).jqxWindow('zIndex'));

            $$.dialog.SonIndex += 1;//$($$.dialog.dialog[gid].nesne).jqxWindow('zIndex');
            $($$.dialog.dialog[gid].nesne).jqxWindow({ zIndex: $$.dialog.SonIndex });
        }
        this.setGorev(id);

        if (object.fullscreen)
            $$.dialog.SetFullScreen($$.dialog.dialog[gid]);
    },
    setTitle: function (id, title) {
        var gid = this.getId(id);
        $$.dialog.dialog[gid].nesneTitle.html(title);
    },
    setHtml: function (id, html) {
        var gid = this.getId(id);
        $$.dialog.dialog[gid].nesnePanel.html(html);
    },
    setGorev: function (id) {
        var gid = this.getId(id);
        if ($$.dialog.dialog[gid].gorev == null) {
            $$.dialog.dialog[gid].gorev = $('<div class="gv col-md-1 col-sx-12 col-sm-4 col-lg-1" id="row"><button style="overflow: hidden" class="btn btn-md btn-sm btn-inverse btn-block"><i class="fa fa-wpbeginner"></i><span id="btntitle"></span></button></div>');
            $(this.gvList).append($$.dialog.dialog[gid].gorev);
            $($$.dialog.dialog[gid].gorev).find('button').bind('click', function (event) {



                var zIndex = $($$.dialog.dialog[gid].nesne).jqxWindow('zIndex');


                var isopn = $($$.dialog.dialog[gid].nesne).jqxWindow('isOpen');
                window.event = -3;
                if (isopn == true && zIndex == $$.dialog.SonIndex) {

                    $($$.dialog.dialog[gid].nesne).jqxWindow({ zIndex: 9000 });
                    $($$.dialog.dialog[gid].nesne).jqxWindow('collapse');

                } else {

                    $$.dialog.SonIndex++;
                    $($$.dialog.dialog[gid].nesne).jqxWindow({ zIndex: $$.dialog.SonIndex });
                    $($$.dialog.dialog[gid].nesne).jqxWindow('open');
                    var fnc = function () {
                        $($$.dialog.dialog[gid].nesne).jqxWindow('expand');
                        $($$.dialog.dialog[gid].nesne).jqxWindow('bringToFront');
                        $($$.dialog.dialog[gid].gorev).find('#btntitle').html(" " + $$.dialog.dialog[gid].title);
                    };
                    setTimeout(fnc, 500);

                }


            });
            $($$.dialog.dialog[gid].gorev).find('#btntitle').html(" " + $$.dialog.dialog[gid].title);
            $($$.dialog.dialog[gid].gorev).attr("title", $$.dialog.dialog[gid].title);
        }

    },
    seTResize: function () {

        $$.dialog.gvHeaderBaslik = $($$.dialog.gvHeaderBaslikSelector).height() || 0;
        var sy = Object.keys($$.dialog.dialog).length;
        var y = $$.dialog.getKullanilirAlan();
        for (var i = 0; i < sy; i++) {
            var gid = Object.keys($$.dialog.dialog)[i];
            $($$.dialog.dialog[gid].nesne).jqxWindow({
                dragArea: {
                    left: ($($$.dialog.leftObject).width()),
                    top: $$.dialog.gvHeaderBaslik,
                    width: y.width,
                    height: y.height
                }
            });

            var x = {
                width: $($$.dialog.dialog[gid].nesne).jqxWindow('width'),
                height: $($$.dialog.dialog[gid].nesne).jqxWindow('height')
            };
            if (x.width > y.width) { x.width = y.width; x.position = 'center' }
            if (x.height > y.height) { x.height = y.height; x.position = 'center' };
            x.maxHeight = y.maxHeight;
            x.maxWidth = y.maxWidth;


            $($$.dialog.dialog[gid].nesne).jqxWindow(x);
        }


    },
    TumuIndirKaldir: function () {
        this.TumuIndir = !this.TumuIndir;
        var islem = '';
        var objectKey = Object.keys($$.dialog.dialog);
        window.event = -3;
        if (this.TumuIndir == true) {
            for (var i = 0; i < objectKey.length; i++) {
                $($$.dialog.dialog[objectKey[i]].nesne).jqxWindow('close');//.delay( 800 );
            }
        } else {
            var i = 0;
            for (i = 0; i < objectKey.length; i++) {
                $($$.dialog.dialog[objectKey[i]].nesne).jqxWindow('open');
                $($$.dialog.dialog[objectKey[i]].gorev).find('#btntitle').html(" " + $$.dialog.dialog[objectKey[i]].title);
            }
            if (i > 0) {

                $($$.dialog.dialog[objectKey[i - 1]].nesne).jqxWindow('bringToFront');
            }
        }

    },
    TumuKapat: function () {
        var islem = '';
        var objectKey = Object.keys($$.dialog.dialog);
        for (var i = 0; i < objectKey.length; i++) {
            $$.dialog.deleteDialog($$.dialog.dialog[objectKey[i]].id);
        }
    }

};




$(window).resize(function () {
    setTimeout($$.dialog.seTResize, 1000);
});





$$.set_combobox = function (id, m, datafnc, value, shrow, placeHorder) {
    if (placeHorder == undefined || placeHorder.length < 0)
        placeHorder = "";
    if (datafnc == undefined)
        datafnc = function () {
            return null;
        };
    if (shrow == undefined)
        shrow = true;
    var source =
        {
            datatype: "json",
            datafields: [
                { name: 'text' },
                { name: 'id' }
            ],
            url: m,
            data: {}
        };
    var dataAdapter = new $.jqx.dataAdapter(source,
        {
            formatData: function (data) {
                if ($(id).jqxComboBox('searchString') != undefined) {
                    data.q = $(id).jqxComboBox('searchString');
                    /* aranan deger */
                    data.data = {};
                    try {
                        if (typeof(datafnc) == "function")
                            data.data = datafnc();
                        else if (typeof(datafnc) == "object") {
                            data.data = JSON.stringify(datafnc.start());
                        }
                    } catch (e) {
                    }
                    return data;
                }
            },
            loadComplete: function (data) {
                if (data.complete == null) {
                    data.complete = "[]";
                }

                return JSON.parse(data['complete']);
            }
        });
    $(id).jqxComboBox(
        {
            popupZIndex: 999999,
            source: dataAdapter, width: '100%', height: '30px', 
            remoteAutoComplete: true, autoComplete: true, showArrow: shrow, autoDropDownHeight: false, multiSelect: false, selectedIndex: -1,
            displayMember: "text", valueMember: "id", placeHolder: placeHorder, remoteAutoCompleteDelay: 0, minLength: -1,
            renderer: function (index, label, value) {
                try {
                    var item = dataAdapter.records[index];
                    if (item != null) {
                        if (item.id == null || item.text == null) {
                            item = { id: null, text: "Kayıt Bulunamadı" };
                        }
                        var label = item.text;
                        return label;
                    }
                } catch (e) {
                }
            },
            renderSelectedItem: function (index, item) {
                try {
                    
                    var item = dataAdapter.records[index];
                    if (item != null) {
                        if (item.id == null || item.text == null) {
                            item = { id: null, text: "Kayıt Bulunamadı" };
                        }
                        var label = item.text;
                        return label;
                    }
                } catch (e) {
                }
            },
            search: function (searchString) {
                dataAdapter.dataBind();
            }
        });
      /*  $(id).find('.jqx-combobox-arrow-normal').bind('click', function () {

        debugger;
        dataAdapter.dataBind();
        $(this).parent().parent().parent().jqxComboBox(
            { 
                source: dataAdapter
            });
    })

    $(id).bind('open', function (event) {
        debugger;
        dataAdapter.dataBind();
    });
    */
    if (typeof(id) != "object") {
        var fnc = function () {
            $(id + ' div[id*=dropdownlistContent]').width('100%');
            $(id + ' div[id*=dropdownlistContent]').css('right', '0px');
        }
        fnc();

        if (value !== null) {
            $$.combo.set_value(id, value);

        }
        return $(id);
    }
}
$$.combo = {
    set_value: function (id, value) {

        $(id).jqxComboBox({ remoteAutoComplete: false });
        $(id).jqxComboBox('source').get_last_value = { ID: value.id, value: value.id, text: value.text };
        $(id).jqxComboBox('addItem', value);
        $(id).jqxComboBox('selectedIndex', 0);
        $(id).jqxComboBox({ remoteAutoComplete: true });
    }
};

$$.get_combobox = function (id) {
    var items = $(id).jqxComboBox('getSelectedItem');
    data = { label: '', value: null };
    if (items != null) {
        data.value = items.value;
        data.text = items.label;
    } else if ($(id).jqxComboBox('source').get_last_value != undefined) {
        if ($(id).jqxComboBox('source').get_last_value.text == $(id).jqxComboBox('val'))
            data = $(id).jqxComboBox('source').get_last_value;
    }
    return data;
};



$$.set_dropdownlist = function (id, value, text)
{
    var obj = { text: text, value: value, id: value }; 
    $(id).jqxDropDownList('selectItem', obj); 
    return obj;
};


$$.get_dropdownlist = function (id) {
    var obj = $(id).jqxDropDownList('getSelectedItem');
    return obj;
};
