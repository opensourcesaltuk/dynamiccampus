﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
public class CryptoAes
{
    string AES_Key = "";
    string AES_IV = "";
    public CryptoAes(string AES_Key, string AES_IV)
    {
        this.AES_IV = AES_IV;
        this.AES_Key = AES_Key;
    }
    public string AES_encrypt(string Input)
    {
        try
        {

            var aes = new RijndaelManaged();
            aes.KeySize = 256;
            aes.BlockSize = 128;
            aes.Padding = PaddingMode.PKCS7;
            aes.Mode = CipherMode.ECB;
            aes.Key = Convert.FromBase64String(AES_Key);
            aes.IV = Convert.FromBase64String(AES_IV);

            var encrypt = aes.CreateEncryptor(aes.Key, aes.IV);
            byte[] xBuff = null;
            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, encrypt, CryptoStreamMode.Write))
                {
                    byte[] xXml = Encoding.UTF8.GetBytes(Input);
                    cs.Write(xXml, 0, xXml.Length);
                }

                xBuff = ms.ToArray();
            }

            String Output = Convert.ToBase64String(xBuff);
            return Output;

        }
        catch (Exception ex)
        {

            throw;
        }
    }

    public string AES_decrypt(string Input)
    {
        try
        {

            RijndaelManaged aes = new RijndaelManaged();
            aes.KeySize = 256;
            aes.BlockSize = 128;
            aes.Padding = PaddingMode.PKCS7;
            aes.Mode = CipherMode.ECB;
            aes.Key = Convert.FromBase64String(AES_Key);
            aes.IV = Convert.FromBase64String(AES_IV);

            var decrypt = aes.CreateDecryptor();
            byte[] xBuff = null;
            using (var ms = new MemoryStream())
            {
                using (var cs = new CryptoStream(ms, decrypt, CryptoStreamMode.Write))
                {
                    byte[] xXml = Convert.FromBase64String(Input);
                    cs.Write(xXml, 0, xXml.Length);
                }

                xBuff = ms.ToArray();
            }

            String Output = Encoding.UTF8.GetString(xBuff); 
            return Output;
        }
        catch (Exception ex)
        {

            throw;
        }
    }

    public string Md5Sum(string strToEncrypt)
    {
        UTF8Encoding ue = new UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);
        MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);
        string hashString = "";
        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }
        return hashString.PadLeft(32, '0').ToUpper();
    }
    private readonly byte[] Salt = new byte[] { 101, 202, 130, 43, 54, 61, 71, 87 };
    public string CreateKey(string password, int keyBytes = 32)
    {
        const int Iterations = 9999;
        var keyGenerator = new Rfc2898DeriveBytes(Md5Sum(password), Salt, Iterations);
        return Convert.ToBase64String(keyGenerator.GetBytes(keyBytes));
    }
}

public class AES_DEF
{
    public string IV { get; set; }
    public string Key { get; set; }
}