﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;

namespace AJAXPMVC
{
    public class jqw:MainClass
    {

        public new string oTema = "System/jqxListe";
        Dictionary<string, DataColumns> datcol = new Dictionary<string, DataColumns>();
        public jqwSubProp Ozellikler = new jqwSubProp();
        public jqhierarchy jqhierarchy;
        public string SourceId = "EmployeeID";
        public JArray data = null;
        public JArray Columns = new JArray();
        public JObject SubData = new JObject();
        public string Id { get; private set; }
        public string PostRowDetailsFnc { get; set; }

        public JArray btnList = new JArray();
        public jqw(bool idOlustur,string id="")
        {
            if (idOlustur)
            {
                id = "jqxGrid_" + (DateTime.Now.ToString() + (Guid.NewGuid().ToString())).Md5Sum();
            }
            this.Id = id;
            this.SourceId = "";
        }
        public void AddDataColumns(string name, string type)
        {
            if (datcol.ContainsKey(name))
                datcol[name].type = type;
            datcol.Add(name, new DataColumns() { name = name, type = type });
        }
        public void hierarchy(string keyDataField, string parentDataField)
        {
            jqhierarchy = new jqhierarchy() { keyDataField = new jqkeyField() { name = keyDataField }, parentDataField = new jqkeyField() { name = parentDataField } };
        }
        public void AddToolbarButton(string btnName, fa_icons iClass,string Function, ViewBtnClass btnClass=ViewBtnClass.btn_primary)
        {
            JObject obj = new JObject();
            obj.Add("btnName", btnName);
            obj.Add("btnClass", btnClass.GetDescription());
            obj.Add("iClass", iClass.GetDescription());
            if (Function.StartsWith("$$.AJAXP.POST") && Function.EndsWith(",null);"))
            {
                Function= Function.Substring(0, Function.Length - 7)+",this);";
            }
            obj.Add("Function", Function);
            btnList.Add(obj);
        }
        /// <summary>
        /// Eklenen Columnslar
        /// </summary>
        /// <param name="text"> text </param>
        /// <param name="datafield"> hangi alan</param>
        /// <param name="width"> genişlik</param>
        /// <param name="columntype">colum tipi</param>
        /// <param name="displayfield">combo ve dropdown için Görüntü adı</param>
        /// <param name="SubData">combo ve dropdown için liste</param>
        /// <param name="mFunc"> POST VALUE datarow</param>
        public void AddColumn(string text, string datafield, string width, jqcolumntype columntype = jqcolumntype.textbox, string displayfield = "", JArray SubData = null,string mFunc="",bool editable=true,bool showAggregates=false)
        {
            JObject obj = new JObject();
            obj.Add("text", text);
            obj.Add("datafield", datafield);
            obj.Add("width", width);
            if (columntype == jqcolumntype.date)
            {
                obj.Add("cellsformat", "dd.MM.yyyy");
                columntype = jqcolumntype.date;
            }
            if (columntype == jqcolumntype.datetime)
            {
                obj.Add("cellsformat", "dd.MM.yyyy hh:mm");
                columntype = jqcolumntype.datetimeinput;
            }

            obj.Add("columntype", columntype.ToString());
          
            obj.Add("editable", editable);
            if(displayfield.Length>0)
                obj.Add("displayfield", displayfield);


            if (columntype == jqcolumntype.numberinput)
            { 
                obj.Add("showAggregates", showAggregates); 
            }

            if (columntype == jqcolumntype.button && mFunc.Length > 0)
            {
                /*
                obj.Add("buttonclick", @"function (row) { 
                                                var datarow = $('#" + this.Id + @"').jqxGrid('getrowdata', row);
                                                " + mFunc + @"
                                            }");
                                            */
                obj.Add("buttonclick", mFunc);
            }
            if (this.SubData[datafield] == null && SubData!=null)
                this.SubData[datafield] = SubData;
            Columns.Add(obj);
        }

        public string Olustur(string CookieName="")
        {
            string retdata = ""; 
            q.viewData["Ozellikler"] = Ozellikler;
            q.viewData["data"] = data;
            q.viewData["CookieName"] = CookieName;
            q.viewData["jqhierarchy"] = jqhierarchy;
            q.viewData["Columns"] = Columns;
            q.viewData["datcol"] = datcol.Select(t => t.Value).ToArray();
            q.viewData["SubData"] = SubData;
            q.viewData["Id"] = Id;
            q.viewData["jqhierarchyId"] = SourceId;
            q.viewData["btnList"] = btnList;
            q.viewData["PostRowDetailsFnc"] = PostRowDetailsFnc;
            retdata = getPageHtml("liste");
            return retdata;
        }

        public string getRowDatajs()
        {
            return @""+ this.Id + ".getSelectValue()";             
        }

        public string getDataJs()
        {
            return @"JSON.stringify(" + this.Id + ".getData())";
        }

        public string getDeleteRowsJs()
        {
            return @"JSON.stringify(" + this.Id + ".getDeleteData())";
        }

        public void PostRowDetailsFunc(string oNamespace, string oClassname, string funcName, string PostAdi)
        {
            this.PostRowDetailsFnc = q.sq.m(oNamespace, oClassname, funcName, PostAdi + ":SendData");
        }

        public static JObject getRowDetails(string PostAdi)
        {
            if (q.request.ContainsKey(PostAdi))
            {
                return JObject.Parse(q.request[PostAdi]);
            }
            else
                return null;
        }

        public static string EkleNewRow(string GridId, bool js=false)
        {
            GridId= "$("+ GridId .toJavaScriptValue()+ ").jqxGrid('addrow', null, {});";
            if (js)
                return GridId;
            else
            {
                q.ajax.AddFnc("fnc" + DateTime.Now.Ticks.ToString(), GridId);
                return GridId;
            }
        }
        public static string DeleteRow(string GridId, string uid, bool js=false)
        {
            GridId = GridId + ".DeleteRow('"+uid+"')"; // "$("+ GridId .toJavaScriptValue()+ ").jqxGrid('deleterow', "+ uid + ");";
            if (js)
                return GridId;
            else
            {
                q.ajax.AddFnc("fnc" + DateTime.Now.Ticks.ToString(), GridId);
                return GridId;
            }
        }
    }
    public class jqwSubProp
    {
        public bool editable = false, groupable=false;
        public bool altRows = true, autoRowHeight= true,  columnsResize= true, disabled=false, enableHover= true, enableBrowserSelection= true , filterable= true, rowdetails=false, showAggregates=false;
        public jqfilterMode filterMode = jqfilterMode.Default;
        public jqselectionmode selectionmode = jqselectionmode.singlerow;
        public string height;
        public string width = "100%";
        public int pageSize = 20;
        public int toolbarheight = 38;
        public int[] pageSizeOptions = new int[] { 15, 20, 30 };
        public bool pageable = true;
        public bool sortable = true;
    }

    public enum jqcolumntype
    { 
        textbox =0,
        number=1,
        checkbox=2,
        numberinput=3,
        numberinputdec5=10,
        dropdownlist =4,
        combobox=5,
        datetimeinput=6,
        button=7,
        date = 8,
        datetime = 9,

    }
    public enum jqfilterMode
    {
        Default = 1,
        Simple = 2,
        Advanced = 3
    }

    public enum jqselectionmode
    {
        none = 0,
        singlerow = 1,
        multiplerows = 2,
        multiplerowsextended = 3,
        singlecell = 4,
        multiplecells = 5,
        multiplecellsextended = 6,
        multiplecellsadvanced = 7,
        checkbox = 8
    }
    public class jqhierarchy
    {
        public jqkeyField keyDataField, parentDataField;
       
    } 
    public class jqkeyField { public string name = ""; } 
    public class DataColumns
    {
        public string name = "";
        public string type = "";
    }
}