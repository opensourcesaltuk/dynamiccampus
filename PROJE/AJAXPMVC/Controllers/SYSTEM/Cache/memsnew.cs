﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ServiceStack.Redis;

public class memsnew
{
    public string keyFirst = "";
    public ServiceStack.Redis.IRedisClient cilent;
    public ServiceStack.Redis.PooledRedisClientManager cil; 
    public memsnew(string ip, string keyFirst)
    {
        this.keyFirst = keyFirst;
        this.cil = new ServiceStack.Redis.PooledRedisClientManager(ip);
        cil.ConnectTimeout = -1;
        cil.PoolTimeout = -1;
        cil.SocketReceiveTimeout = -1;
        cil.SocketSendTimeout = -1;
        cil.IdleTimeOutSecs = -1;
        cilent = cil.GetClient();
         
    }

    public bool CacheClear(string key)
    {

        List<string> keys = cilent.SearchKeys("*" + key + "*");
        for (int i = 0; i < keys.Count; i++)
        {
            cilent.Remove(keys[i]);
        }
        return true;
    }

    public void setdb(int v)
    { 
        this.cilent.Db = v;
    }

    public bool CacheYaz<T>(T data, string o, DateTime time)
    {
        try
        {
            if (data == null)
            {
                cilent.Remove(keyFirst + o);
                return true;
            }
            cilent.Set<T>(keyFirst + o, data);
            cilent.ExpireEntryAt(keyFirst + o, time);
        }
        catch
        {
            return false;
        }
        return true;
    }

    public T CacheOku<T>(string key, bool ornekle = false)
    {
        try
        {
            T s = cilent.Get<T>(keyFirst + key);                      
            if (ornekle && s == null)
                s = (T)Activator.CreateInstance(typeof(T));
            return s;
        }
        catch
        {
            return default(T);
        }

    }

}