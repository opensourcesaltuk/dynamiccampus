﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AJAXPMVC.Ents
{
    [TableAttribute(Scheme = "br.")]
    public class Birim : Ent
    {
        [TableAttribute(Primary = true, Identity = true)]
        public long Id { get; set; }
        public long? UstBirimId { get; set; }
        public long BirimTipId { get; set; }
        public string BirimAdi { get; set; }
        public string BirimAdiGlobal { get; set; }
        public string Siralama { get; set; }
        public long AktifId { get; set; }
        public string BirimKodu { get; set; }
        public long? FakulteId { get; set; }
    }
    [TableAttribute(Scheme = "br.")]
    public class BirimKodlari : Ent
    {
        [TableAttribute(Primary = true, Identity = true)]
        public long Id { get; set; }
        public long BirimId { get; set; }
        public string AktarimOsymKodu { get; set; }
        public string AktarimBirimKodu { get; set; }
        public string BildirimOysmKodu { get; set; }
        public string BildirimBirimKodu { get; set; }
        public long AktifId { get; set; }
        public long WebGorunsunId { get; set; }
        public long BursTipId { get; set; }
    }
    [TableAttribute(Scheme = "br.")]
    public class BirimGenelOzellik : Ent
    {
        [TableAttribute(Primary = true, Identity = false)]
        public long Id { get; set; }
        public int AzamiYariYil { get; set; }
        public int KacYariYil { get; set; }
        public long? WebGorunsunId { get; set; }
        public bool HazirlikVarMi { get; set; }
        public long? HazirlikModulId { get; set; }
        public int? IsyeriId { get; set; }
    }
    public class vw_BirimKodlari : BirimKodlari
    {
        public string AktifAdi { get; set; }
        public string WebGorunsunAdi { get; set; }
        public string BursTipAdi { get; set; }
    }
    public class vw_BirimGenelOzellik : BirimGenelOzellik
    {
        public string WebGorunsunAdi { get; set; }
        public string HazirlikModulAdi { get; set; }
        public string IsyeriAdi { get; set; }
    }
    public class vw_Birim : Birim
    {
        public string UstBirimAdi { get; set; }
        public string BirimTipAdi { get; set; }
        public string AktifAdi { get; set; }
    }
    public class vw_FakulteProgramListesi
    {
        public long AkademikBirimId { get; set; }
        public long AkademikBirimUstBirimId { get; set; }
        public long AkademikBirimTipId { get; set; }
        public string AkademikBirimTipAdi { get; set; }
        public string AkademikBirimTipKodu { get; set; }
        public string AkademikBirimAdi { get; set; }
        public string AkademikBirimAdiGlobal { get; set; }
        public string AkademikBirimSiralama { get; set; }
        public long AkademikBirimAktifId { get; set; }
        public string AkademikBirimAktifAdi { get; set; }
        public string AkademikBirimBirimKodu { get; set; }
        public long ProgramId { get; set; }
        public long ProgramUstBirimId { get; set; }
        public long ProgramTipId { get; set; }
        public string ProgramTipAdi { get; set; }
        public string ProgramTipKodu { get; set; }
        public string ProgramAdi { get; set; }
        public string ProgramAdiGlobal { get; set; }
        public string ProgramSiralama { get; set; }
        public long ProgramAktifId { get; set; }
        public string ProgramAktifAdi { get; set; }
        public string ProgramBirimKodu { get; set; }
    }
    public class vw_ModelBirim
    {
        public vw_Birim Birim;
        public vw_BirimGenelOzellik BirimOzellikleri;
        public vw_BirimKodlari[] BirimKodlari; 
        public vw_TipUst[] BirimTipi { get; set; }
    }
}