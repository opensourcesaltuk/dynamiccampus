﻿using AJAXPMVC.Ents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AJAXPMVC
{
    public class SabitTanimController : MainClass
    {

        public new string oTema = "SabitTanimController";
        public void load()
        {
            q.viewData["SabitTanim"] = GetSabitTanimSayfa();
            string TabId = q.request["TabDivId"];
            q.ajax.newTabId("SabitTanimControllerTab", TabId, "Sabit Tanımlar");
            q.viewData["fncSabitTanimlar"] = this.m("Duzenleme", "TabDivId:TabDivId, datas:data");
            string html = this.getPageHtml("Liste");
            q.ajax.html("#" + TabId, html);
        }

        public vw_TipUst[] getTipler(int grpId)
        {
            var data = q.con.CollectionFromSql(@"SELECT    tip.Id ,
                                                               tip.TipId ,  
                                                               tipUst.Ad UstTipAdi ,
                                                               ISNULL(tip.TipGrupId,grp.Id) TipGrupId,
                                                               tip.Ad ,
                                                               tip.Aciklama ,
                                                               tip.ExtraKod ,
                                                               tip.ExtraKod2 ,
                                                               tip.BasTarih ,
                                                               tip.BitTarih ,
                                                               tip.KullaniciId ,
				                                               tip.BasDec ,
				                                               tipUstbas.Ad as BasDecAd,
				                                               tip.BitDec ,
				                                               tipUstBit.Ad as BitDecAd,
				                                               CAST(0 AS BIT) AS  Edit
	                                            FROM   tpl.TipGrup grp WITH ( NOLOCK )
				                                                INNER JOIN tpl.Tip tip WITH ( NOLOCK ) ON tip.TipGrupId = grp.Id
				                                                LEFT JOIN tpl.Tip tipUst WITH ( NOLOCK ) ON tipUst.Id = tip.TipId 
				                                                INNER JOIN tpl.SabitTanim sbt WITH(NOLOCK) ON sbt.TipGrupId=grp.Id
				                                                LEFT JOIN tpl.Tip tipUstbas WITH ( NOLOCK ) ON tipUstbas.Id = tip.BasDec  AND sbt.BasDecColTipGrupId IS NOT NULL
				                                                LEFT JOIN tpl.Tip tipUstBit WITH ( NOLOCK ) ON tipUstBit.Id = tip.BitDec AND sbt.BitDecColTipGrupId IS NOT NULL
                                                        WHERE grp.Id =" + grpId.ToString()).ToArray().toSerialize().unSerialize<vw_TipUst[]>();
            return data;

        }
        public void Duzenleme()
        {
            try
            {
                string TabDivId = q.request["TabDivId"];
                int id = int.Parse(q.request["datas"]);
                SabitTanim row = GetSabitTanimSayfa(id).First();
                TipGrup rows = Ent.get<TipGrup>().Where("Id", row.TipGrupId).FirstOrDefault<TipGrup>();
                q.viewData["ekrantipi"] = row;
                q.viewData["sabit"] = rows;


                TipGrup UstTip = Ent.get<TipGrup>().Where("Id", rows.TipGrupId ?? 0).FirstOrDefault<TipGrup>();

                q.viewData["sabitust"] = UstTip;
                string UstId = (UstTip != null ? UstTip.Id.ToString() : "0");

                q.viewData["ComboData"] = q.con.CollectionFromSql(@"SELECT a.TipId AS id,a.Ad AS label FROM  tpl.vw_TipGrupListesi a WHERE a.GrupId=" + UstId).ToArray().Select(p => new { id = (long)p.id, label = (string)p.label }).ToArray();
                q.viewData["BasDecColTipGrupId"] = null;
                if (row.BasDecColTipGrupId.HasValue)
                {
                    UstId = (row.BasDecColTipGrupId.Value).ToString();
                    q.viewData["BasDecColTipGrupId"] = q.con.CollectionFromSql(@"SELECT a.TipId AS id,a.Ad AS label FROM  tpl.vw_TipGrupListesi a WHERE a.GrupId=" + UstId).ToArray().Select(p => new { id = (long)p.id, label = (string)p.label }).ToArray();

                }
                else
                {
                    row.BasDecColTipGrupId = 0;
                }
                q.viewData["BitDecColTipGrupId"] = null;
                if (row.BitDecColTipGrupId.HasValue)
                {
                    UstId = (row.BitDecColTipGrupId.Value).ToString();
                    q.viewData["BitDecColTipGrupId"] = q.con.CollectionFromSql(@"SELECT a.TipId AS id,a.Ad AS label FROM  tpl.vw_TipGrupListesi a WHERE a.GrupId=" + UstId).ToArray().Select(p => new { id = (long)p.id, label = (string)p.label }).ToArray();

                }
                else
                { 
                    row.BitDecColTipGrupId = 0;
                }
                #region sql 
                /*
                var data = q.con.CollectionFromSql(@"SELECT    tip.Id ,
                                                               tip.TipId ,  
                                                               tipUst.Ad UstTipAdi ,
                                                               ISNULL(tip.TipGrupId,grp.Id) TipGrupId,
                                                               tip.Ad ,
                                                               tip.Aciklama ,
                                                               tip.ExtraKod ,
                                                               tip.ExtraKod2 ,
                                                               tip.BasTarih ,
                                                               tip.BitTarih ,
                                                               tip.KullaniciId ,
                                                               tip.BasDec ,
                                                               tip.BitDec 
                                                        FROM   tpl.TipGrup grp WITH ( NOLOCK )
                                                               INNER JOIN tpl.Tip tip WITH ( NOLOCK ) ON tip.TipGrupId = grp.Id
                                                               INNER JOIN tpl.Tip tipUst WITH ( NOLOCK ) ON tipUst.TipId = tip.Id
                                                               INNER JOIN tpl.TipGrup grpUst WITH ( NOLOCK ) ON grpUst.Id = tipUst.TipGrupId  
                                                        WHERE grp.Id ="+ rows.Id.ToString()).ToArray().Select(t => new Tip() {  Id=(long)t.Id, Aciklama = (string)t.Aciklama, Ad = (string)t.Ad, BasDec = (decimal?)t.BasDec, BasTarih = (DateTime?)t.BasTarih, BitDec = (decimal?)t.BitDec, BitTarih = (DateTime?)t.BitTarih, ExtraKod = (string)t.ExtraKod, ExtraKod2 = (string)t.ExtraKod2, KullaniciId = (long)t.KullaniciId, TipGrupId = (int)t.TipGrupId, TipId = (long?)t.TipId }).ToArray();
                                                        */
                #endregion
                var data = getTipler(rows.Id);

                q.viewData["data"] = data;
                q.viewData["TabDivId"] = TabDivId;



                q.viewData["fntipsil"] = this.m("Sil", "data:data, objId:xx.objId");
                q.viewData["fntipKaydet"] = this.m("Kaydet", "data:data, objId:xx.objId,sabit:sabit");
                string html = this.getPageHtml("Duzenleme");
                q.ajax.html("#" + TabDivId, html);
            }
            catch (Exception ex)
            {

            }

        }

        public void Sil()
        {
            try
            {

                vw_TipUst data = q.request["data"].unSerialize<vw_TipUst>();
                Tip tpl = Ent.LoadExtend<Tip>(q.request["data"]);
                if (tpl.Delete())
                {
                    q.ajax.AddFnc("silmetamamla", q.request["objId"] + ".Sil(" + q.request["data"] + ",true);");
                }
                else
                {
                    q.ajax.alert("silmedi", "Değer Kullanımda");

                }
            }
            catch (Exception ex)
            {
                q.ajax.alert("ex", "Değer Kullanımda");
            }
        }
        public void Kaydet()
        {

            try
            {
                vw_TipUst[] data = q.request["data"].unSerialize<vw_TipUst[]>();

                for (int i = 0; i < data.Length; i++)
                {
                    if (!data[i].Edit)
                        continue;
                    Tip tpl = Ent.LoadExtend<Tip>(data[i].toSerialize());
                    if (tpl.Id == 0)
                        tpl.Create();

                    tpl.Save();
                }
                TipGrup rows = q.request["sabit"].unSerialize<TipGrup>();
                var retData = getTipler(rows.Id);
                q.ajax.AddFnc("retData", q.request["objId"] + ".loadNewData(" + retData.toSerializeJson() + ");");

            }
            catch (Exception ex)
            {

            }

        }


        public SabitTanim[] GetSabitTanimSayfa(int? id = null)
        {

            string sart = "";
            if (id.HasValue)
                sart = " AND sbt.Id=" + id.Value.ToString();
            SabitTanim[] tanim = q.con.CollectionFromSql(@"SELECT 
                                                    sbt.Id, sbt.SayfaAdi, sbt.TipGrupId, sbt.SuperUser, sbt.AdColAdi, sbt.AdColUzunluk, sbt.ExtraKodColAdi, sbt.ExtraKodColUzunluk, sbt.ExtraKod2ColAdi, sbt.ExtraKod2ColUzunluk, sbt.BasTarihColAdi, sbt.BasTarihColUzunluk, sbt.BitTarihColAdi, sbt.BitTarihColUzunluk, sbt.BasDecColAdi, sbt.BasDecColUzunluk, sbt.BitDecColAdi, sbt.BitDecColUzunluk, sbt.AktifMi, sbt.TipUstGrupAktifMi,sbt.BasDecColTipGrupId,sbt.BitDecColTipGrupId, sbt.AciklamaColAdi
                                                FROM tpl.SabitTanim sbt WITH (NOLOCK)
                                                INNER JOIN tpl.TipGrup grp WITH (NOLOCK)
	                                                ON grp.Id = sbt.TipGrupId " + sart + @"
                                                ORDER BY  grp.GrupSira , ISNULL(grp.TipGrupId, grp.Id)").ToArray().toSerialize().unSerialize<SabitTanim[]>();
            return tanim;
        }

    }
}