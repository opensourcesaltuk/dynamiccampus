﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace AJAXPMVC
{ 
    public enum fa_icons
    {
        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-address-book")]
        fa_fa_address_book = 56,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-address-book-o")]
        fa_fa_address_book_o = 57,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-address-card")]
        fa_fa_address_card = 58,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-address-card-o")]
        fa_fa_address_card_o = 59,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-bandcamp")]
        fa_fa_bandcamp = 781,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-bath")]
        fa_fa_bath = 81,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-bathtub")]
        fa_fa_bathtub = 82,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-drivers-license")]
        fa_fa_drivers_license = 183,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-drivers-license-o")]
        fa_fa_drivers_license_o = 184,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-eercast")]
        fa_fa_eercast = 814,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-envelope-open")]
        fa_fa_envelope_open = 190,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-envelope-open-o")]
        fa_fa_envelope_open_o = 191,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-etsy")]
        fa_fa_etsy = 817,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-free-code-camp")]
        fa_fa_free_code_camp = 832,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-grav")]
        fa_fa_grav = 853,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-handshake-o")]
        fa_fa_handshake_o = 254,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-id-badge")]
        fa_fa_id_badge = 274,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-id-card")]
        fa_fa_id_card = 275,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-id-card-o")]
        fa_fa_id_card_o = 276,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-imdb")]
        fa_fa_imdb = 857,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-linode")]
        fa_fa_linode = 868,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-meetup")]
        fa_fa_meetup = 873,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-microchip")]
        fa_fa_microchip = 313,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-podcast")]
        fa_fa_podcast = 351,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-quora")]
        fa_fa_quora = 892,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-ravelry")]
        fa_fa_ravelry = 894,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-s15")]
        fa_fa_s15 = 374,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-shower")]
        fa_fa_shower = 391,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-snowflake-o")]
        fa_fa_snowflake_o = 400,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-superpowers")]
        fa_fa_superpowers = 923,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-telegram")]
        fa_fa_telegram = 924,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-thermometer")]
        fa_fa_thermometer = 438,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-thermometer-0")]
        fa_fa_thermometer_0 = 439,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-thermometer-1")]
        fa_fa_thermometer_1 = 440,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-thermometer-2")]
        fa_fa_thermometer_2 = 441,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-thermometer-3")]
        fa_fa_thermometer_3 = 442,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-thermometer-4")]
        fa_fa_thermometer_4 = 443,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-thermometer-empty")]
        fa_fa_thermometer_empty = 444,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-thermometer-full")]
        fa_fa_thermometer_full = 445,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-thermometer-half")]
        fa_fa_thermometer_half = 446,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-thermometer-quarter")]
        fa_fa_thermometer_quarter = 447,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-thermometer-three-quarters")]
        fa_fa_thermometer_three_quarters = 448,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-times-rectangle")]
        fa_fa_times_rectangle = 458,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-times-rectangle-o")]
        fa_fa_times_rectangle_o = 459,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-user-circle")]
        fa_fa_user_circle = 483,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-user-circle-o")]
        fa_fa_user_circle_o = 484,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-user-o")]
        fa_fa_user_o = 485,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-vcard")]
        fa_fa_vcard = 490,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-vcard-o")]
        fa_fa_vcard_o = 491,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-window-close")]
        fa_fa_window_close = 501,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-window-close-o")]
        fa_fa_window_close_o = 502,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-window-maximize")]
        fa_fa_window_maximize = 503,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-window-minimize")]
        fa_fa_window_minimize = 504,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-window-restore")]
        fa_fa_window_restore = 505,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-wpexplorer")]
        fa_fa_wpexplorer = 950,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-adjust")]
        fa_fa_adjust = 60,

        /// <summary>
        /// Accessibility Icons
        /// </summary>
        [Description("fa fa-american-sign-language-interpreting")]
        fa_fa_american_sign_language_interpreting = 507,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-anchor")]
        fa_fa_anchor = 62,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-archive")]
        fa_fa_archive = 63,

        /// <summary>
        /// Chart Icons
        /// </summary>
        [Description("fa fa-area-chart")]
        fa_fa_area_chart = 621,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-arrows")]
        fa_fa_arrows = 721,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-arrows-h")]
        fa_fa_arrows_h = 723,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-arrows-v")]
        fa_fa_arrows_v = 724,

        /// <summary>
        /// Accessibility Icons
        /// </summary>
        [Description("fa fa-asl-interpreting")]
        fa_fa_asl_interpreting = 508,

        /// <summary>
        /// Accessibility Icons
        /// </summary>
        [Description("fa fa-assistive-listening-systems")]
        fa_fa_assistive_listening_systems = 509,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-asterisk")]
        fa_fa_asterisk = 70,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-at")]
        fa_fa_at = 71,

        /// <summary>
        /// Accessibility Icons
        /// </summary>
        [Description("fa fa-audio-description")]
        fa_fa_audio_description = 510,

        /// <summary>
        /// Transportation Icons
        /// </summary>
        [Description("fa fa-automobile")]
        fa_fa_automobile = 544,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-balance-scale")]
        fa_fa_balance_scale = 74,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-ban")]
        fa_fa_ban = 75,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-bank")]
        fa_fa_bank = 76,

        /// <summary>
        /// Chart Icons
        /// </summary>
        [Description("fa fa-bar-chart")]
        fa_fa_bar_chart = 622,

        /// <summary>
        /// Chart Icons
        /// </summary>
        [Description("fa fa-bar-chart-o")]
        fa_fa_bar_chart_o = 623,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-barcode")]
        fa_fa_barcode = 79,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-bars")]
        fa_fa_bars = 80,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-battery")]
        fa_fa_battery = 83,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-battery-0")]
        fa_fa_battery_0 = 84,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-battery-1")]
        fa_fa_battery_1 = 85,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-battery-2")]
        fa_fa_battery_2 = 86,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-battery-3")]
        fa_fa_battery_3 = 87,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-battery-4")]
        fa_fa_battery_4 = 88,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-battery-empty")]
        fa_fa_battery_empty = 89,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-battery-full")]
        fa_fa_battery_full = 90,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-battery-half")]
        fa_fa_battery_half = 91,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-battery-quarter")]
        fa_fa_battery_quarter = 92,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-battery-three-quarters")]
        fa_fa_battery_three_quarters = 93,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-bed")]
        fa_fa_bed = 94,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-beer")]
        fa_fa_beer = 95,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-bell")]
        fa_fa_bell = 96,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-bell-o")]
        fa_fa_bell_o = 97,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-bell-slash")]
        fa_fa_bell_slash = 98,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-bell-slash-o")]
        fa_fa_bell_slash_o = 99,

        /// <summary>
        /// Transportation Icons
        /// </summary>
        [Description("fa fa-bicycle")]
        fa_fa_bicycle = 545,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-binoculars")]
        fa_fa_binoculars = 101,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-birthday-cake")]
        fa_fa_birthday_cake = 102,

        /// <summary>
        /// Accessibility Icons
        /// </summary>
        [Description("fa fa-blind")]
        fa_fa_blind = 511,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-bluetooth")]
        fa_fa_bluetooth = 788,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-bluetooth-b")]
        fa_fa_bluetooth_b = 789,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-bolt")]
        fa_fa_bolt = 106,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-bomb")]
        fa_fa_bomb = 107,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-book")]
        fa_fa_book = 108,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-bookmark")]
        fa_fa_bookmark = 109,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-bookmark-o")]
        fa_fa_bookmark_o = 110,

        /// <summary>
        /// Accessibility Icons
        /// </summary>
        [Description("fa fa-braille")]
        fa_fa_braille = 512,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-briefcase")]
        fa_fa_briefcase = 112,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-bug")]
        fa_fa_bug = 113,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-building")]
        fa_fa_building = 114,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-building-o")]
        fa_fa_building_o = 115,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-bullhorn")]
        fa_fa_bullhorn = 116,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-bullseye")]
        fa_fa_bullseye = 117,

        /// <summary>
        /// Transportation Icons
        /// </summary>
        [Description("fa fa-bus")]
        fa_fa_bus = 546,

        /// <summary>
        /// Transportation Icons
        /// </summary>
        [Description("fa fa-cab")]
        fa_fa_cab = 547,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-calculator")]
        fa_fa_calculator = 120,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-calendar")]
        fa_fa_calendar = 121,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-calendar-check-o")]
        fa_fa_calendar_check_o = 122,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-calendar-minus-o")]
        fa_fa_calendar_minus_o = 123,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-calendar-o")]
        fa_fa_calendar_o = 124,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-calendar-plus-o")]
        fa_fa_calendar_plus_o = 125,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-calendar-times-o")]
        fa_fa_calendar_times_o = 126,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-camera")]
        fa_fa_camera = 127,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-camera-retro")]
        fa_fa_camera_retro = 128,

        /// <summary>
        /// Transportation Icons
        /// </summary>
        [Description("fa fa-car")]
        fa_fa_car = 548,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-caret-square-o-down")]
        fa_fa_caret_square_o_down = 728,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-caret-square-o-left")]
        fa_fa_caret_square_o_left = 729,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-caret-square-o-right")]
        fa_fa_caret_square_o_right = 730,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-caret-square-o-up")]
        fa_fa_caret_square_o_up = 731,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-cart-arrow-down")]
        fa_fa_cart_arrow_down = 134,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-cart-plus")]
        fa_fa_cart_plus = 135,

        /// <summary>
        /// Accessibility Icons
        /// </summary>
        [Description("fa fa-cc")]
        fa_fa_cc = 513,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-certificate")]
        fa_fa_certificate = 137,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-check")]
        fa_fa_check = 138,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-check-circle")]
        fa_fa_check_circle = 139,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-check-circle-o")]
        fa_fa_check_circle_o = 140,

        /// <summary>
        /// Form Control Icons
        /// </summary>
        [Description("fa fa-check-square")]
        fa_fa_check_square = 598,

        /// <summary>
        /// Form Control Icons
        /// </summary>
        [Description("fa fa-check-square-o")]
        fa_fa_check_square_o = 599,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-child")]
        fa_fa_child = 143,

        /// <summary>
        /// Form Control Icons
        /// </summary>
        [Description("fa fa-circle")]
        fa_fa_circle = 600,

        /// <summary>
        /// Form Control Icons
        /// </summary>
        [Description("fa fa-circle-o")]
        fa_fa_circle_o = 601,

        /// <summary>
        /// Spinner Icons
        /// </summary>
        [Description("fa fa-circle-o-notch")]
        fa_fa_circle_o_notch = 593,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-circle-thin")]
        fa_fa_circle_thin = 147,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-clock-o")]
        fa_fa_clock_o = 148,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-clone")]
        fa_fa_clone = 149,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-close")]
        fa_fa_close = 150,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-cloud")]
        fa_fa_cloud = 151,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-cloud-download")]
        fa_fa_cloud_download = 152,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-cloud-upload")]
        fa_fa_cloud_upload = 153,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-code")]
        fa_fa_code = 154,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-code-fork")]
        fa_fa_code_fork = 155,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-coffee")]
        fa_fa_coffee = 156,

        /// <summary>
        /// Spinner Icons
        /// </summary>
        [Description("fa fa-cog")]
        fa_fa_cog = 594,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-cogs")]
        fa_fa_cogs = 158,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-comment")]
        fa_fa_comment = 159,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-comment-o")]
        fa_fa_comment_o = 160,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-commenting")]
        fa_fa_commenting = 161,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-commenting-o")]
        fa_fa_commenting_o = 162,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-comments")]
        fa_fa_comments = 163,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-comments-o")]
        fa_fa_comments_o = 164,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-compass")]
        fa_fa_compass = 165,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-copyright")]
        fa_fa_copyright = 166,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-creative-commons")]
        fa_fa_creative_commons = 167,

        /// <summary>
        /// Payment Icons
        /// </summary>
        [Description("fa fa-credit-card")]
        fa_fa_credit_card = 617,

        /// <summary>
        /// Payment Icons
        /// </summary>
        [Description("fa fa-credit-card-alt")]
        fa_fa_credit_card_alt = 618,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-crop")]
        fa_fa_crop = 170,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-crosshairs")]
        fa_fa_crosshairs = 171,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-cube")]
        fa_fa_cube = 172,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-cubes")]
        fa_fa_cubes = 173,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-cutlery")]
        fa_fa_cutlery = 174,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-dashboard")]
        fa_fa_dashboard = 175,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-database")]
        fa_fa_database = 176,

        /// <summary>
        /// Accessibility Icons
        /// </summary>
        [Description("fa fa-deaf")]
        fa_fa_deaf = 514,

        /// <summary>
        /// Accessibility Icons
        /// </summary>
        [Description("fa fa-deafness")]
        fa_fa_deafness = 515,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-desktop")]
        fa_fa_desktop = 179,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-diamond")]
        fa_fa_diamond = 180,

        /// <summary>
        /// Form Control Icons
        /// </summary>
        [Description("fa fa-dot-circle-o")]
        fa_fa_dot_circle_o = 602,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-download")]
        fa_fa_download = 182,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-edit")]
        fa_fa_edit = 185,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-ellipsis-h")]
        fa_fa_ellipsis_h = 186,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-ellipsis-v")]
        fa_fa_ellipsis_v = 187,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-envelope")]
        fa_fa_envelope = 188,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-envelope-o")]
        fa_fa_envelope_o = 189,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-envelope-square")]
        fa_fa_envelope_square = 192,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-eraser")]
        fa_fa_eraser = 664,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-exchange")]
        fa_fa_exchange = 741,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-exclamation")]
        fa_fa_exclamation = 195,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-exclamation-circle")]
        fa_fa_exclamation_circle = 196,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-exclamation-triangle")]
        fa_fa_exclamation_triangle = 197,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-external-link")]
        fa_fa_external_link = 198,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-external-link-square")]
        fa_fa_external_link_square = 199,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-eye")]
        fa_fa_eye = 200,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-eye-slash")]
        fa_fa_eye_slash = 201,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-eyedropper")]
        fa_fa_eyedropper = 202,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-fax")]
        fa_fa_fax = 203,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-feed")]
        fa_fa_feed = 204,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-female")]
        fa_fa_female = 205,

        /// <summary>
        /// Transportation Icons
        /// </summary>
        [Description("fa fa-fighter-jet")]
        fa_fa_fighter_jet = 549,

        /// <summary>
        /// File Type Icons
        /// </summary>
        [Description("fa fa-file-archive-o")]
        fa_fa_file_archive_o = 576,

        /// <summary>
        /// File Type Icons
        /// </summary>
        [Description("fa fa-file-audio-o")]
        fa_fa_file_audio_o = 577,

        /// <summary>
        /// File Type Icons
        /// </summary>
        [Description("fa fa-file-code-o")]
        fa_fa_file_code_o = 578,

        /// <summary>
        /// File Type Icons
        /// </summary>
        [Description("fa fa-file-excel-o")]
        fa_fa_file_excel_o = 579,

        /// <summary>
        /// File Type Icons
        /// </summary>
        [Description("fa fa-file-image-o")]
        fa_fa_file_image_o = 580,

        /// <summary>
        /// File Type Icons
        /// </summary>
        [Description("fa fa-file-movie-o")]
        fa_fa_file_movie_o = 581,

        /// <summary>
        /// File Type Icons
        /// </summary>
        [Description("fa fa-file-pdf-o")]
        fa_fa_file_pdf_o = 583,

        /// <summary>
        /// File Type Icons
        /// </summary>
        [Description("fa fa-file-photo-o")]
        fa_fa_file_photo_o = 584,

        /// <summary>
        /// File Type Icons
        /// </summary>
        [Description("fa fa-file-picture-o")]
        fa_fa_file_picture_o = 585,

        /// <summary>
        /// File Type Icons
        /// </summary>
        [Description("fa fa-file-powerpoint-o")]
        fa_fa_file_powerpoint_o = 586,

        /// <summary>
        /// File Type Icons
        /// </summary>
        [Description("fa fa-file-sound-o")]
        fa_fa_file_sound_o = 587,

        /// <summary>
        /// File Type Icons
        /// </summary>
        [Description("fa fa-file-video-o")]
        fa_fa_file_video_o = 590,

        /// <summary>
        /// File Type Icons
        /// </summary>
        [Description("fa fa-file-word-o")]
        fa_fa_file_word_o = 591,

        /// <summary>
        /// File Type Icons
        /// </summary>
        [Description("fa fa-file-zip-o")]
        fa_fa_file_zip_o = 592,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-film")]
        fa_fa_film = 221,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-filter")]
        fa_fa_filter = 222,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-fire")]
        fa_fa_fire = 223,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-fire-extinguisher")]
        fa_fa_fire_extinguisher = 224,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-flag")]
        fa_fa_flag = 225,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-flag-checkered")]
        fa_fa_flag_checkered = 226,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-flag-o")]
        fa_fa_flag_o = 227,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-flash")]
        fa_fa_flash = 228,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-flask")]
        fa_fa_flask = 229,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-folder")]
        fa_fa_folder = 230,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-folder-o")]
        fa_fa_folder_o = 231,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-folder-open")]
        fa_fa_folder_open = 232,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-folder-open-o")]
        fa_fa_folder_open_o = 233,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-frown-o")]
        fa_fa_frown_o = 234,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-futbol-o")]
        fa_fa_futbol_o = 235,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-gamepad")]
        fa_fa_gamepad = 236,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-gavel")]
        fa_fa_gavel = 237,

        /// <summary>
        /// Spinner Icons
        /// </summary>
        [Description("fa fa-gear")]
        fa_fa_gear = 595,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-gears")]
        fa_fa_gears = 239,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-gift")]
        fa_fa_gift = 240,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-glass")]
        fa_fa_glass = 241,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-globe")]
        fa_fa_globe = 242,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-graduation-cap")]
        fa_fa_graduation_cap = 243,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-group")]
        fa_fa_group = 244,

        /// <summary>
        /// Hand Icons
        /// </summary>
        [Description("fa fa-hand-grab-o")]
        fa_fa_hand_grab_o = 526,

        /// <summary>
        /// Hand Icons
        /// </summary>
        [Description("fa fa-hand-lizard-o")]
        fa_fa_hand_lizard_o = 527,

        /// <summary>
        /// Hand Icons
        /// </summary>
        [Description("fa fa-hand-paper-o")]
        fa_fa_hand_paper_o = 532,

        /// <summary>
        /// Hand Icons
        /// </summary>
        [Description("fa fa-hand-peace-o")]
        fa_fa_hand_peace_o = 533,

        /// <summary>
        /// Hand Icons
        /// </summary>
        [Description("fa fa-hand-pointer-o")]
        fa_fa_hand_pointer_o = 534,

        /// <summary>
        /// Hand Icons
        /// </summary>
        [Description("fa fa-hand-rock-o")]
        fa_fa_hand_rock_o = 535,

        /// <summary>
        /// Hand Icons
        /// </summary>
        [Description("fa fa-hand-scissors-o")]
        fa_fa_hand_scissors_o = 536,

        /// <summary>
        /// Hand Icons
        /// </summary>
        [Description("fa fa-hand-spock-o")]
        fa_fa_hand_spock_o = 537,

        /// <summary>
        /// Hand Icons
        /// </summary>
        [Description("fa fa-hand-stop-o")]
        fa_fa_hand_stop_o = 538,

        /// <summary>
        /// Accessibility Icons
        /// </summary>
        [Description("fa fa-hard-of-hearing")]
        fa_fa_hard_of_hearing = 516,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-hashtag")]
        fa_fa_hashtag = 256,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-hdd-o")]
        fa_fa_hdd_o = 257,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-headphones")]
        fa_fa_headphones = 258,

        /// <summary>
        /// Medical Icons
        /// </summary>
        [Description("fa fa-heart")]
        fa_fa_heart = 966,

        /// <summary>
        /// Medical Icons
        /// </summary>
        [Description("fa fa-heart-o")]
        fa_fa_heart_o = 967,

        /// <summary>
        /// Medical Icons
        /// </summary>
        [Description("fa fa-heartbeat")]
        fa_fa_heartbeat = 968,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-history")]
        fa_fa_history = 262,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-home")]
        fa_fa_home = 263,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-hotel")]
        fa_fa_hotel = 264,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-hourglass")]
        fa_fa_hourglass = 265,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-hourglass-1")]
        fa_fa_hourglass_1 = 266,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-hourglass-2")]
        fa_fa_hourglass_2 = 267,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-hourglass-3")]
        fa_fa_hourglass_3 = 268,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-hourglass-end")]
        fa_fa_hourglass_end = 269,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-hourglass-half")]
        fa_fa_hourglass_half = 270,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-hourglass-o")]
        fa_fa_hourglass_o = 271,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-hourglass-start")]
        fa_fa_hourglass_start = 272,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-i-cursor")]
        fa_fa_i_cursor = 273,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-image")]
        fa_fa_image = 277,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-inbox")]
        fa_fa_inbox = 278,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-industry")]
        fa_fa_industry = 279,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-info")]
        fa_fa_info = 280,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-info-circle")]
        fa_fa_info_circle = 281,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-institution")]
        fa_fa_institution = 282,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-key")]
        fa_fa_key = 283,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-keyboard-o")]
        fa_fa_keyboard_o = 284,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-language")]
        fa_fa_language = 285,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-laptop")]
        fa_fa_laptop = 286,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-leaf")]
        fa_fa_leaf = 287,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-legal")]
        fa_fa_legal = 288,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-lemon-o")]
        fa_fa_lemon_o = 289,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-level-down")]
        fa_fa_level_down = 290,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-level-up")]
        fa_fa_level_up = 291,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-life-bouy")]
        fa_fa_life_bouy = 292,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-life-buoy")]
        fa_fa_life_buoy = 293,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-life-ring")]
        fa_fa_life_ring = 294,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-life-saver")]
        fa_fa_life_saver = 295,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-lightbulb-o")]
        fa_fa_lightbulb_o = 296,

        /// <summary>
        /// Chart Icons
        /// </summary>
        [Description("fa fa-line-chart")]
        fa_fa_line_chart = 624,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-location-arrow")]
        fa_fa_location_arrow = 298,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-lock")]
        fa_fa_lock = 299,

        /// <summary>
        /// Accessibility Icons
        /// </summary>
        [Description("fa fa-low-vision")]
        fa_fa_low_vision = 517,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-magic")]
        fa_fa_magic = 301,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-magnet")]
        fa_fa_magnet = 302,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-mail-forward")]
        fa_fa_mail_forward = 303,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-mail-reply")]
        fa_fa_mail_reply = 304,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-mail-reply-all")]
        fa_fa_mail_reply_all = 305,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-male")]
        fa_fa_male = 306,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-map")]
        fa_fa_map = 307,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-map-marker")]
        fa_fa_map_marker = 308,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-map-o")]
        fa_fa_map_o = 309,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-map-pin")]
        fa_fa_map_pin = 310,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-map-signs")]
        fa_fa_map_signs = 311,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-meh-o")]
        fa_fa_meh_o = 312,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-microphone")]
        fa_fa_microphone = 314,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-microphone-slash")]
        fa_fa_microphone_slash = 315,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-minus")]
        fa_fa_minus = 316,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-minus-circle")]
        fa_fa_minus_circle = 317,

        /// <summary>
        /// Form Control Icons
        /// </summary>
        [Description("fa fa-minus-square")]
        fa_fa_minus_square = 603,

        /// <summary>
        /// Form Control Icons
        /// </summary>
        [Description("fa fa-minus-square-o")]
        fa_fa_minus_square_o = 604,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-mobile")]
        fa_fa_mobile = 320,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-mobile-phone")]
        fa_fa_mobile_phone = 321,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-money")]
        fa_fa_money = 639,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-moon-o")]
        fa_fa_moon_o = 323,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-mortar-board")]
        fa_fa_mortar_board = 324,

        /// <summary>
        /// Transportation Icons
        /// </summary>
        [Description("fa fa-motorcycle")]
        fa_fa_motorcycle = 550,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-mouse-pointer")]
        fa_fa_mouse_pointer = 326,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-music")]
        fa_fa_music = 327,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-navicon")]
        fa_fa_navicon = 328,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-newspaper-o")]
        fa_fa_newspaper_o = 329,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-object-group")]
        fa_fa_object_group = 330,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-object-ungroup")]
        fa_fa_object_ungroup = 331,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-paint-brush")]
        fa_fa_paint_brush = 332,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-paper-plane")]
        fa_fa_paper_plane = 333,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-paper-plane-o")]
        fa_fa_paper_plane_o = 334,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-paw")]
        fa_fa_paw = 335,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-pencil")]
        fa_fa_pencil = 336,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-pencil-square")]
        fa_fa_pencil_square = 337,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-pencil-square-o")]
        fa_fa_pencil_square_o = 338,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-percent")]
        fa_fa_percent = 339,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-phone")]
        fa_fa_phone = 340,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-phone-square")]
        fa_fa_phone_square = 341,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-photo")]
        fa_fa_photo = 342,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-picture-o")]
        fa_fa_picture_o = 343,

        /// <summary>
        /// Chart Icons
        /// </summary>
        [Description("fa fa-pie-chart")]
        fa_fa_pie_chart = 625,

        /// <summary>
        /// Transportation Icons
        /// </summary>
        [Description("fa fa-plane")]
        fa_fa_plane = 551,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-plug")]
        fa_fa_plug = 346,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-plus")]
        fa_fa_plus = 347,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-plus-circle")]
        fa_fa_plus_circle = 348,

        /// <summary>
        /// Medical Icons
        /// </summary>
        [Description("fa fa-plus-square")]
        fa_fa_plus_square = 971,

        /// <summary>
        /// Form Control Icons
        /// </summary>
        [Description("fa fa-plus-square-o")]
        fa_fa_plus_square_o = 606,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-power-off")]
        fa_fa_power_off = 352,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-print")]
        fa_fa_print = 353,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-puzzle-piece")]
        fa_fa_puzzle_piece = 354,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-qrcode")]
        fa_fa_qrcode = 355,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-question")]
        fa_fa_question = 356,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-question-circle")]
        fa_fa_question_circle = 357,

        /// <summary>
        /// Accessibility Icons
        /// </summary>
        [Description("fa fa-question-circle-o")]
        fa_fa_question_circle_o = 518,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-quote-left")]
        fa_fa_quote_left = 359,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-quote-right")]
        fa_fa_quote_right = 360,

        /// <summary>
        /// Video Player Icons
        /// </summary>
        [Description("fa fa-random")]
        fa_fa_random = 768,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-recycle")]
        fa_fa_recycle = 362,

        /// <summary>
        /// Spinner Icons
        /// </summary>
        [Description("fa fa-refresh")]
        fa_fa_refresh = 596,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-registered")]
        fa_fa_registered = 364,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-remove")]
        fa_fa_remove = 365,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-reorder")]
        fa_fa_reorder = 366,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-reply")]
        fa_fa_reply = 367,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-reply-all")]
        fa_fa_reply_all = 368,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-retweet")]
        fa_fa_retweet = 369,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-road")]
        fa_fa_road = 370,

        /// <summary>
        /// Transportation Icons
        /// </summary>
        [Description("fa fa-rocket")]
        fa_fa_rocket = 552,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-rss")]
        fa_fa_rss = 372,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-rss-square")]
        fa_fa_rss_square = 373,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-search")]
        fa_fa_search = 375,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-search-minus")]
        fa_fa_search_minus = 376,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-search-plus")]
        fa_fa_search_plus = 377,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-send")]
        fa_fa_send = 378,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-send-o")]
        fa_fa_send_o = 379,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-server")]
        fa_fa_server = 380,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-share")]
        fa_fa_share = 381,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-share-alt")]
        fa_fa_share_alt = 904,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-share-alt-square")]
        fa_fa_share_alt_square = 905,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-share-square")]
        fa_fa_share_square = 384,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-share-square-o")]
        fa_fa_share_square_o = 385,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-shield")]
        fa_fa_shield = 386,

        /// <summary>
        /// Transportation Icons
        /// </summary>
        [Description("fa fa-ship")]
        fa_fa_ship = 553,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-shopping-bag")]
        fa_fa_shopping_bag = 388,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-shopping-basket")]
        fa_fa_shopping_basket = 389,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-shopping-cart")]
        fa_fa_shopping_cart = 390,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-sign-in")]
        fa_fa_sign_in = 392,

        /// <summary>
        /// Accessibility Icons
        /// </summary>
        [Description("fa fa-sign-language")]
        fa_fa_sign_language = 519,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-sign-out")]
        fa_fa_sign_out = 394,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-signal")]
        fa_fa_signal = 395,

        /// <summary>
        /// Accessibility Icons
        /// </summary>
        [Description("fa fa-signing")]
        fa_fa_signing = 520,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-sitemap")]
        fa_fa_sitemap = 397,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-sliders")]
        fa_fa_sliders = 398,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-smile-o")]
        fa_fa_smile_o = 399,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-soccer-ball-o")]
        fa_fa_soccer_ball_o = 401,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-sort")]
        fa_fa_sort = 402,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-sort-alpha-asc")]
        fa_fa_sort_alpha_asc = 403,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-sort-alpha-desc")]
        fa_fa_sort_alpha_desc = 404,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-sort-amount-asc")]
        fa_fa_sort_amount_asc = 405,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-sort-amount-desc")]
        fa_fa_sort_amount_desc = 406,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-sort-asc")]
        fa_fa_sort_asc = 407,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-sort-desc")]
        fa_fa_sort_desc = 408,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-sort-down")]
        fa_fa_sort_down = 409,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-sort-numeric-asc")]
        fa_fa_sort_numeric_asc = 410,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-sort-numeric-desc")]
        fa_fa_sort_numeric_desc = 411,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-sort-up")]
        fa_fa_sort_up = 412,

        /// <summary>
        /// Transportation Icons
        /// </summary>
        [Description("fa fa-space-shuttle")]
        fa_fa_space_shuttle = 554,

        /// <summary>
        /// Spinner Icons
        /// </summary>
        [Description("fa fa-spinner")]
        fa_fa_spinner = 597,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-spoon")]
        fa_fa_spoon = 415,

        /// <summary>
        /// Form Control Icons
        /// </summary>
        [Description("fa fa-square")]
        fa_fa_square = 607,

        /// <summary>
        /// Form Control Icons
        /// </summary>
        [Description("fa fa-square-o")]
        fa_fa_square_o = 608,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-star")]
        fa_fa_star = 418,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-star-half")]
        fa_fa_star_half = 419,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-star-half-empty")]
        fa_fa_star_half_empty = 420,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-star-half-full")]
        fa_fa_star_half_full = 421,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-star-half-o")]
        fa_fa_star_half_o = 422,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-star-o")]
        fa_fa_star_o = 423,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-sticky-note")]
        fa_fa_sticky_note = 424,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-sticky-note-o")]
        fa_fa_sticky_note_o = 425,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-street-view")]
        fa_fa_street_view = 426,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-suitcase")]
        fa_fa_suitcase = 427,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-sun-o")]
        fa_fa_sun_o = 428,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-support")]
        fa_fa_support = 429,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-tablet")]
        fa_fa_tablet = 430,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-tachometer")]
        fa_fa_tachometer = 431,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-tag")]
        fa_fa_tag = 432,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-tags")]
        fa_fa_tags = 433,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-tasks")]
        fa_fa_tasks = 434,

        /// <summary>
        /// Transportation Icons
        /// </summary>
        [Description("fa fa-taxi")]
        fa_fa_taxi = 556,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-television")]
        fa_fa_television = 436,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-terminal")]
        fa_fa_terminal = 437,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-thumb-tack")]
        fa_fa_thumb_tack = 449,

        /// <summary>
        /// Hand Icons
        /// </summary>
        [Description("fa fa-thumbs-down")]
        fa_fa_thumbs_down = 539,

        /// <summary>
        /// Hand Icons
        /// </summary>
        [Description("fa fa-thumbs-o-down")]
        fa_fa_thumbs_o_down = 540,

        /// <summary>
        /// Hand Icons
        /// </summary>
        [Description("fa fa-thumbs-o-up")]
        fa_fa_thumbs_o_up = 541,

        /// <summary>
        /// Hand Icons
        /// </summary>
        [Description("fa fa-thumbs-up")]
        fa_fa_thumbs_up = 542,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-ticket")]
        fa_fa_ticket = 454,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-times")]
        fa_fa_times = 455,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-times-circle")]
        fa_fa_times_circle = 456,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-times-circle-o")]
        fa_fa_times_circle_o = 457,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-tint")]
        fa_fa_tint = 460,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-toggle-down")]
        fa_fa_toggle_down = 750,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-toggle-left")]
        fa_fa_toggle_left = 751,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-toggle-off")]
        fa_fa_toggle_off = 463,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-toggle-on")]
        fa_fa_toggle_on = 464,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-toggle-right")]
        fa_fa_toggle_right = 752,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-toggle-up")]
        fa_fa_toggle_up = 753,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-trademark")]
        fa_fa_trademark = 467,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-trash")]
        fa_fa_trash = 468,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-trash-o")]
        fa_fa_trash_o = 469,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-tree")]
        fa_fa_tree = 470,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-trophy")]
        fa_fa_trophy = 471,

        /// <summary>
        /// Transportation Icons
        /// </summary>
        [Description("fa fa-truck")]
        fa_fa_truck = 558,

        /// <summary>
        /// Accessibility Icons
        /// </summary>
        [Description("fa fa-tty")]
        fa_fa_tty = 521,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-tv")]
        fa_fa_tv = 474,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-umbrella")]
        fa_fa_umbrella = 475,

        /// <summary>
        /// Accessibility Icons
        /// </summary>
        [Description("fa fa-universal-access")]
        fa_fa_universal_access = 522,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-university")]
        fa_fa_university = 477,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-unlock")]
        fa_fa_unlock = 478,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-unlock-alt")]
        fa_fa_unlock_alt = 479,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-unsorted")]
        fa_fa_unsorted = 480,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-upload")]
        fa_fa_upload = 481,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-user")]
        fa_fa_user = 482,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-user-plus")]
        fa_fa_user_plus = 486,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-user-secret")]
        fa_fa_user_secret = 487,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-user-times")]
        fa_fa_user_times = 488,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-users")]
        fa_fa_users = 489,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-video-camera")]
        fa_fa_video_camera = 492,

        /// <summary>
        /// Accessibility Icons
        /// </summary>
        [Description("fa fa-volume-control-phone")]
        fa_fa_volume_control_phone = 523,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-volume-down")]
        fa_fa_volume_down = 494,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-volume-off")]
        fa_fa_volume_off = 495,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-volume-up")]
        fa_fa_volume_up = 496,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-warning")]
        fa_fa_warning = 497,

        /// <summary>
        /// Medical Icons
        /// </summary>
        [Description("fa fa-wheelchair")]
        fa_fa_wheelchair = 974,

        /// <summary>
        /// Medical Icons
        /// </summary>
        [Description("fa fa-wheelchair-alt")]
        fa_fa_wheelchair_alt = 975,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-wifi")]
        fa_fa_wifi = 500,

        /// <summary>
        /// Web Application Icons
        /// </summary>
        [Description("fa fa-wrench")]
        fa_fa_wrench = 506,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-hand-o-down")]
        fa_fa_hand_o_down = 742,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-hand-o-left")]
        fa_fa_hand_o_left = 743,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-hand-o-right")]
        fa_fa_hand_o_right = 744,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-hand-o-up")]
        fa_fa_hand_o_up = 745,

        /// <summary>
        /// Medical Icons
        /// </summary>
        [Description("fa fa-ambulance")]
        fa_fa_ambulance = 964,

        /// <summary>
        /// Transportation Icons
        /// </summary>
        [Description("fa fa-subway")]
        fa_fa_subway = 555,

        /// <summary>
        /// Transportation Icons
        /// </summary>
        [Description("fa fa-train")]
        fa_fa_train = 557,

        /// <summary>
        /// Gender Icons
        /// </summary>
        [Description("fa fa-genderless")]
        fa_fa_genderless = 561,

        /// <summary>
        /// Gender Icons
        /// </summary>
        [Description("fa fa-intersex")]
        fa_fa_intersex = 562,

        /// <summary>
        /// Gender Icons
        /// </summary>
        [Description("fa fa-mars")]
        fa_fa_mars = 563,

        /// <summary>
        /// Gender Icons
        /// </summary>
        [Description("fa fa-mars-double")]
        fa_fa_mars_double = 564,

        /// <summary>
        /// Gender Icons
        /// </summary>
        [Description("fa fa-mars-stroke")]
        fa_fa_mars_stroke = 565,

        /// <summary>
        /// Gender Icons
        /// </summary>
        [Description("fa fa-mars-stroke-h")]
        fa_fa_mars_stroke_h = 566,

        /// <summary>
        /// Gender Icons
        /// </summary>
        [Description("fa fa-mars-stroke-v")]
        fa_fa_mars_stroke_v = 567,

        /// <summary>
        /// Gender Icons
        /// </summary>
        [Description("fa fa-mercury")]
        fa_fa_mercury = 568,

        /// <summary>
        /// Gender Icons
        /// </summary>
        [Description("fa fa-neuter")]
        fa_fa_neuter = 569,

        /// <summary>
        /// Gender Icons
        /// </summary>
        [Description("fa fa-transgender")]
        fa_fa_transgender = 570,

        /// <summary>
        /// Gender Icons
        /// </summary>
        [Description("fa fa-transgender-alt")]
        fa_fa_transgender_alt = 571,

        /// <summary>
        /// Gender Icons
        /// </summary>
        [Description("fa fa-venus")]
        fa_fa_venus = 572,

        /// <summary>
        /// Gender Icons
        /// </summary>
        [Description("fa fa-venus-double")]
        fa_fa_venus_double = 573,

        /// <summary>
        /// Gender Icons
        /// </summary>
        [Description("fa fa-venus-mars")]
        fa_fa_venus_mars = 574,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-file")]
        fa_fa_file = 665,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-file-o")]
        fa_fa_file_o = 666,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-file-text")]
        fa_fa_file_text = 667,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-file-text-o")]
        fa_fa_file_text_o = 668,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-cc-amex")]
        fa_fa_cc_amex = 792,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-cc-diners-club")]
        fa_fa_cc_diners_club = 793,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-cc-discover")]
        fa_fa_cc_discover = 794,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-cc-jcb")]
        fa_fa_cc_jcb = 795,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-cc-mastercard")]
        fa_fa_cc_mastercard = 796,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-cc-paypal")]
        fa_fa_cc_paypal = 797,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-cc-stripe")]
        fa_fa_cc_stripe = 798,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-cc-visa")]
        fa_fa_cc_visa = 799,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-google-wallet")]
        fa_fa_google_wallet = 851,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-paypal")]
        fa_fa_paypal = 883,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-bitcoin")]
        fa_fa_bitcoin = 786,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-btc")]
        fa_fa_btc = 790,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-cny")]
        fa_fa_cny = 628,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-dollar")]
        fa_fa_dollar = 629,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-eur")]
        fa_fa_eur = 630,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-euro")]
        fa_fa_euro = 631,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-gbp")]
        fa_fa_gbp = 632,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-gg")]
        fa_fa_gg = 835,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-gg-circle")]
        fa_fa_gg_circle = 836,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-ils")]
        fa_fa_ils = 635,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-inr")]
        fa_fa_inr = 636,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-jpy")]
        fa_fa_jpy = 637,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-krw")]
        fa_fa_krw = 638,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-rmb")]
        fa_fa_rmb = 640,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-rouble")]
        fa_fa_rouble = 641,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-rub")]
        fa_fa_rub = 642,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-ruble")]
        fa_fa_ruble = 643,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-rupee")]
        fa_fa_rupee = 644,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-shekel")]
        fa_fa_shekel = 645,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-sheqel")]
        fa_fa_sheqel = 646,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-try")]
        fa_fa_try = 647,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-turkish-lira")]
        fa_fa_turkish_lira = 648,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-usd")]
        fa_fa_usd = 649,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-won")]
        fa_fa_won = 650,

        /// <summary>
        /// Currency Icons
        /// </summary>
        [Description("fa fa-yen")]
        fa_fa_yen = 651,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-align-center")]
        fa_fa_align_center = 652,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-align-justify")]
        fa_fa_align_justify = 653,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-align-left")]
        fa_fa_align_left = 654,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-align-right")]
        fa_fa_align_right = 655,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-bold")]
        fa_fa_bold = 656,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-chain")]
        fa_fa_chain = 657,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-chain-broken")]
        fa_fa_chain_broken = 658,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-clipboard")]
        fa_fa_clipboard = 659,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-columns")]
        fa_fa_columns = 660,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-copy")]
        fa_fa_copy = 661,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-cut")]
        fa_fa_cut = 662,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-dedent")]
        fa_fa_dedent = 663,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-files-o")]
        fa_fa_files_o = 669,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-floppy-o")]
        fa_fa_floppy_o = 670,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-font")]
        fa_fa_font = 671,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-header")]
        fa_fa_header = 672,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-indent")]
        fa_fa_indent = 673,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-italic")]
        fa_fa_italic = 674,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-link")]
        fa_fa_link = 675,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-list")]
        fa_fa_list = 676,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-list-alt")]
        fa_fa_list_alt = 677,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-list-ol")]
        fa_fa_list_ol = 678,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-list-ul")]
        fa_fa_list_ul = 679,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-outdent")]
        fa_fa_outdent = 680,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-paperclip")]
        fa_fa_paperclip = 681,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-paragraph")]
        fa_fa_paragraph = 682,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-paste")]
        fa_fa_paste = 683,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-repeat")]
        fa_fa_repeat = 684,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-rotate-left")]
        fa_fa_rotate_left = 685,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-rotate-right")]
        fa_fa_rotate_right = 686,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-save")]
        fa_fa_save = 687,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-scissors")]
        fa_fa_scissors = 688,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-strikethrough")]
        fa_fa_strikethrough = 689,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-subscript")]
        fa_fa_subscript = 690,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-superscript")]
        fa_fa_superscript = 691,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-table")]
        fa_fa_table = 692,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-text-height")]
        fa_fa_text_height = 693,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-text-width")]
        fa_fa_text_width = 694,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-th")]
        fa_fa_th = 695,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-th-large")]
        fa_fa_th_large = 696,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-th-list")]
        fa_fa_th_list = 697,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-underline")]
        fa_fa_underline = 698,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-undo")]
        fa_fa_undo = 699,

        /// <summary>
        /// Text Editor Icons
        /// </summary>
        [Description("fa fa-unlink")]
        fa_fa_unlink = 700,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-angle-double-down")]
        fa_fa_angle_double_down = 701,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-angle-double-left")]
        fa_fa_angle_double_left = 702,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-angle-double-right")]
        fa_fa_angle_double_right = 703,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-angle-double-up")]
        fa_fa_angle_double_up = 704,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-angle-down")]
        fa_fa_angle_down = 705,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-angle-left")]
        fa_fa_angle_left = 706,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-angle-right")]
        fa_fa_angle_right = 707,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-angle-up")]
        fa_fa_angle_up = 708,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-arrow-circle-down")]
        fa_fa_arrow_circle_down = 709,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-arrow-circle-left")]
        fa_fa_arrow_circle_left = 710,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-arrow-circle-o-down")]
        fa_fa_arrow_circle_o_down = 711,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-arrow-circle-o-left")]
        fa_fa_arrow_circle_o_left = 712,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-arrow-circle-o-right")]
        fa_fa_arrow_circle_o_right = 713,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-arrow-circle-o-up")]
        fa_fa_arrow_circle_o_up = 714,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-arrow-circle-right")]
        fa_fa_arrow_circle_right = 715,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-arrow-circle-up")]
        fa_fa_arrow_circle_up = 716,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-arrow-down")]
        fa_fa_arrow_down = 717,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-arrow-left")]
        fa_fa_arrow_left = 718,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-arrow-right")]
        fa_fa_arrow_right = 719,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-arrow-up")]
        fa_fa_arrow_up = 720,

        /// <summary>
        /// Video Player Icons
        /// </summary>
        [Description("fa fa-arrows-alt")]
        fa_fa_arrows_alt = 754,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-caret-down")]
        fa_fa_caret_down = 725,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-caret-left")]
        fa_fa_caret_left = 726,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-caret-right")]
        fa_fa_caret_right = 727,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-caret-up")]
        fa_fa_caret_up = 732,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-chevron-circle-down")]
        fa_fa_chevron_circle_down = 733,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-chevron-circle-left")]
        fa_fa_chevron_circle_left = 734,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-chevron-circle-right")]
        fa_fa_chevron_circle_right = 735,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-chevron-circle-up")]
        fa_fa_chevron_circle_up = 736,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-chevron-down")]
        fa_fa_chevron_down = 737,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-chevron-left")]
        fa_fa_chevron_left = 738,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-chevron-right")]
        fa_fa_chevron_right = 739,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-chevron-up")]
        fa_fa_chevron_up = 740,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-long-arrow-down")]
        fa_fa_long_arrow_down = 746,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-long-arrow-left")]
        fa_fa_long_arrow_left = 747,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-long-arrow-right")]
        fa_fa_long_arrow_right = 748,

        /// <summary>
        /// Directional Icons
        /// </summary>
        [Description("fa fa-long-arrow-up")]
        fa_fa_long_arrow_up = 749,

        /// <summary>
        /// Video Player Icons
        /// </summary>
        [Description("fa fa-backward")]
        fa_fa_backward = 755,

        /// <summary>
        /// Video Player Icons
        /// </summary>
        [Description("fa fa-compress")]
        fa_fa_compress = 756,

        /// <summary>
        /// Video Player Icons
        /// </summary>
        [Description("fa fa-eject")]
        fa_fa_eject = 757,

        /// <summary>
        /// Video Player Icons
        /// </summary>
        [Description("fa fa-expand")]
        fa_fa_expand = 758,

        /// <summary>
        /// Video Player Icons
        /// </summary>
        [Description("fa fa-fast-backward")]
        fa_fa_fast_backward = 759,

        /// <summary>
        /// Video Player Icons
        /// </summary>
        [Description("fa fa-fast-forward")]
        fa_fa_fast_forward = 760,

        /// <summary>
        /// Video Player Icons
        /// </summary>
        [Description("fa fa-forward")]
        fa_fa_forward = 761,

        /// <summary>
        /// Video Player Icons
        /// </summary>
        [Description("fa fa-pause")]
        fa_fa_pause = 762,

        /// <summary>
        /// Video Player Icons
        /// </summary>
        [Description("fa fa-pause-circle")]
        fa_fa_pause_circle = 763,

        /// <summary>
        /// Video Player Icons
        /// </summary>
        [Description("fa fa-pause-circle-o")]
        fa_fa_pause_circle_o = 764,

        /// <summary>
        /// Video Player Icons
        /// </summary>
        [Description("fa fa-play")]
        fa_fa_play = 765,

        /// <summary>
        /// Video Player Icons
        /// </summary>
        [Description("fa fa-play-circle")]
        fa_fa_play_circle = 766,

        /// <summary>
        /// Video Player Icons
        /// </summary>
        [Description("fa fa-play-circle-o")]
        fa_fa_play_circle_o = 767,

        /// <summary>
        /// Video Player Icons
        /// </summary>
        [Description("fa fa-step-backward")]
        fa_fa_step_backward = 769,

        /// <summary>
        /// Video Player Icons
        /// </summary>
        [Description("fa fa-step-forward")]
        fa_fa_step_forward = 770,

        /// <summary>
        /// Video Player Icons
        /// </summary>
        [Description("fa fa-stop")]
        fa_fa_stop = 771,

        /// <summary>
        /// Video Player Icons
        /// </summary>
        [Description("fa fa-stop-circle")]
        fa_fa_stop_circle = 772,

        /// <summary>
        /// Video Player Icons
        /// </summary>
        [Description("fa fa-stop-circle-o")]
        fa_fa_stop_circle_o = 773,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-youtube-play")]
        fa_fa_youtube_play = 962,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-500px")]
        fa_fa_500px = 775,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-adn")]
        fa_fa_adn = 776,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-amazon")]
        fa_fa_amazon = 777,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-android")]
        fa_fa_android = 778,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-angellist")]
        fa_fa_angellist = 779,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-apple")]
        fa_fa_apple = 780,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-behance")]
        fa_fa_behance = 782,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-behance-square")]
        fa_fa_behance_square = 783,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-bitbucket")]
        fa_fa_bitbucket = 784,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-bitbucket-square")]
        fa_fa_bitbucket_square = 785,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-black-tie")]
        fa_fa_black_tie = 787,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-buysellads")]
        fa_fa_buysellads = 791,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-chrome")]
        fa_fa_chrome = 800,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-codepen")]
        fa_fa_codepen = 801,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-codiepie")]
        fa_fa_codiepie = 802,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-connectdevelop")]
        fa_fa_connectdevelop = 803,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-contao")]
        fa_fa_contao = 804,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-css3")]
        fa_fa_css3 = 805,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-dashcube")]
        fa_fa_dashcube = 806,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-delicious")]
        fa_fa_delicious = 807,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-deviantart")]
        fa_fa_deviantart = 808,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-digg")]
        fa_fa_digg = 809,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-dribbble")]
        fa_fa_dribbble = 810,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-dropbox")]
        fa_fa_dropbox = 811,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-drupal")]
        fa_fa_drupal = 812,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-edge")]
        fa_fa_edge = 813,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-empire")]
        fa_fa_empire = 815,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-envira")]
        fa_fa_envira = 816,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-expeditedssl")]
        fa_fa_expeditedssl = 818,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-fa")]
        fa_fa_fa = 819,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-facebook")]
        fa_fa_facebook = 820,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-facebook-f")]
        fa_fa_facebook_f = 821,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-facebook-official")]
        fa_fa_facebook_official = 822,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-facebook-square")]
        fa_fa_facebook_square = 823,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-firefox")]
        fa_fa_firefox = 824,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-first-order")]
        fa_fa_first_order = 825,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-flickr")]
        fa_fa_flickr = 826,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-font-awesome")]
        fa_fa_font_awesome = 827,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-fonticons")]
        fa_fa_fonticons = 828,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-fort-awesome")]
        fa_fa_fort_awesome = 829,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-forumbee")]
        fa_fa_forumbee = 830,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-foursquare")]
        fa_fa_foursquare = 831,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-ge")]
        fa_fa_ge = 833,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-get-pocket")]
        fa_fa_get_pocket = 834,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-git")]
        fa_fa_git = 837,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-git-square")]
        fa_fa_git_square = 838,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-github")]
        fa_fa_github = 839,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-github-alt")]
        fa_fa_github_alt = 840,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-github-square")]
        fa_fa_github_square = 841,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-gitlab")]
        fa_fa_gitlab = 842,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-gittip")]
        fa_fa_gittip = 843,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-glide")]
        fa_fa_glide = 844,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-glide-g")]
        fa_fa_glide_g = 845,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-google")]
        fa_fa_google = 846,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-google-plus")]
        fa_fa_google_plus = 847,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-google-plus-circle")]
        fa_fa_google_plus_circle = 848,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-google-plus-official")]
        fa_fa_google_plus_official = 849,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-google-plus-square")]
        fa_fa_google_plus_square = 850,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-gratipay")]
        fa_fa_gratipay = 852,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-hacker-news")]
        fa_fa_hacker_news = 854,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-houzz")]
        fa_fa_houzz = 855,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-html5")]
        fa_fa_html5 = 856,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-instagram")]
        fa_fa_instagram = 858,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-internet-explorer")]
        fa_fa_internet_explorer = 859,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-ioxhost")]
        fa_fa_ioxhost = 860,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-joomla")]
        fa_fa_joomla = 861,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-jsfiddle")]
        fa_fa_jsfiddle = 862,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-lastfm")]
        fa_fa_lastfm = 863,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-lastfm-square")]
        fa_fa_lastfm_square = 864,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-leanpub")]
        fa_fa_leanpub = 865,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-linkedin")]
        fa_fa_linkedin = 866,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-linkedin-square")]
        fa_fa_linkedin_square = 867,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-linux")]
        fa_fa_linux = 869,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-maxcdn")]
        fa_fa_maxcdn = 870,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-meanpath")]
        fa_fa_meanpath = 871,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-medium")]
        fa_fa_medium = 872,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-mixcloud")]
        fa_fa_mixcloud = 874,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-modx")]
        fa_fa_modx = 875,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-odnoklassniki")]
        fa_fa_odnoklassniki = 876,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-odnoklassniki-square")]
        fa_fa_odnoklassniki_square = 877,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-opencart")]
        fa_fa_opencart = 878,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-openid")]
        fa_fa_openid = 879,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-opera")]
        fa_fa_opera = 880,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-optin-monster")]
        fa_fa_optin_monster = 881,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-pagelines")]
        fa_fa_pagelines = 882,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-pied-piper")]
        fa_fa_pied_piper = 884,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-pied-piper-alt")]
        fa_fa_pied_piper_alt = 885,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-pied-piper-pp")]
        fa_fa_pied_piper_pp = 886,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-pinterest")]
        fa_fa_pinterest = 887,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-pinterest-p")]
        fa_fa_pinterest_p = 888,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-pinterest-square")]
        fa_fa_pinterest_square = 889,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-product-hunt")]
        fa_fa_product_hunt = 890,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-qq")]
        fa_fa_qq = 891,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-ra")]
        fa_fa_ra = 893,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-rebel")]
        fa_fa_rebel = 895,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-reddit")]
        fa_fa_reddit = 896,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-reddit-alien")]
        fa_fa_reddit_alien = 897,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-reddit-square")]
        fa_fa_reddit_square = 898,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-renren")]
        fa_fa_renren = 899,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-resistance")]
        fa_fa_resistance = 900,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-safari")]
        fa_fa_safari = 901,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-scribd")]
        fa_fa_scribd = 902,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-sellsy")]
        fa_fa_sellsy = 903,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-shirtsinbulk")]
        fa_fa_shirtsinbulk = 906,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-simplybuilt")]
        fa_fa_simplybuilt = 907,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-skyatlas")]
        fa_fa_skyatlas = 908,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-skype")]
        fa_fa_skype = 909,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-slack")]
        fa_fa_slack = 910,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-slideshare")]
        fa_fa_slideshare = 911,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-snapchat")]
        fa_fa_snapchat = 912,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-snapchat-ghost")]
        fa_fa_snapchat_ghost = 913,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-snapchat-square")]
        fa_fa_snapchat_square = 914,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-soundcloud")]
        fa_fa_soundcloud = 915,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-spotify")]
        fa_fa_spotify = 916,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-stack-exchange")]
        fa_fa_stack_exchange = 917,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-stack-overflow")]
        fa_fa_stack_overflow = 918,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-steam")]
        fa_fa_steam = 919,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-steam-square")]
        fa_fa_steam_square = 920,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-stumbleupon")]
        fa_fa_stumbleupon = 921,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-stumbleupon-circle")]
        fa_fa_stumbleupon_circle = 922,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-tencent-weibo")]
        fa_fa_tencent_weibo = 925,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-themeisle")]
        fa_fa_themeisle = 926,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-trello")]
        fa_fa_trello = 927,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-tripadvisor")]
        fa_fa_tripadvisor = 928,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-tumblr")]
        fa_fa_tumblr = 929,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-tumblr-square")]
        fa_fa_tumblr_square = 930,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-twitch")]
        fa_fa_twitch = 931,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-twitter")]
        fa_fa_twitter = 932,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-twitter-square")]
        fa_fa_twitter_square = 933,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-usb")]
        fa_fa_usb = 934,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-viacoin")]
        fa_fa_viacoin = 935,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-viadeo")]
        fa_fa_viadeo = 936,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-viadeo-square")]
        fa_fa_viadeo_square = 937,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-vimeo")]
        fa_fa_vimeo = 938,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-vimeo-square")]
        fa_fa_vimeo_square = 939,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-vine")]
        fa_fa_vine = 940,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-vk")]
        fa_fa_vk = 941,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-wechat")]
        fa_fa_wechat = 942,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-weibo")]
        fa_fa_weibo = 943,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-weixin")]
        fa_fa_weixin = 944,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-whatsapp")]
        fa_fa_whatsapp = 945,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-wikipedia-w")]
        fa_fa_wikipedia_w = 946,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-windows")]
        fa_fa_windows = 947,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-wordpress")]
        fa_fa_wordpress = 948,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-wpbeginner")]
        fa_fa_wpbeginner = 949,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-wpforms")]
        fa_fa_wpforms = 951,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-xing")]
        fa_fa_xing = 952,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-xing-square")]
        fa_fa_xing_square = 953,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-y-combinator")]
        fa_fa_y_combinator = 954,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-y-combinator-square")]
        fa_fa_y_combinator_square = 955,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-yahoo")]
        fa_fa_yahoo = 956,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-yc")]
        fa_fa_yc = 957,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-yc-square")]
        fa_fa_yc_square = 958,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-yelp")]
        fa_fa_yelp = 959,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-yoast")]
        fa_fa_yoast = 960,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-youtube")]
        fa_fa_youtube = 961,

        /// <summary>
        /// Brand Icons
        /// </summary>
        [Description("fa fa-youtube-square")]
        fa_fa_youtube_square = 963,

        /// <summary>
        /// Medical Icons
        /// </summary>
        [Description("fa fa-h-square")]
        fa_fa_h_square = 965,

        /// <summary>
        /// Medical Icons
        /// </summary>
        [Description("fa fa-hospital-o")]
        fa_fa_hospital_o = 969,

        /// <summary>
        /// Medical Icons
        /// </summary>
        [Description("fa fa-medkit")]
        fa_fa_medkit = 970,

        /// <summary>
        /// Medical Icons
        /// </summary>
        [Description("fa fa-stethoscope")]
        fa_fa_stethoscope = 972,

        /// <summary>
        /// Medical Icons
        /// </summary>
        [Description("fa fa-user-md")]
        fa_fa_user_md = 973
    }
 
}