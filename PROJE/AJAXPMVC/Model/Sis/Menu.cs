﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AJAXPMVC.Ents
{
    [TableAttribute(Scheme = "sis.")]
    public class Menu : Ent
    {
        [TableAttribute(Primary = true, Identity = true)]
        public int Id { get; set; }
        public int? UstId { get; set; }
        public string MenuAdi { get; set; }
        public string TabName { get; set; }
        public string Controller { get; set; }
        public string ClassName { get; set; }
        public string FuncName { get; set; }
        public string PostData { get; set; }
        public string Durum { get; set; }
        public bool Dil { get; set; }
    }



    public class MenuView  
    { 
        public int Id { get; set; }
        public int? UstId { get; set; }
        public string MenuAdi { get; set; }
        public string TabName { get; set; }
        public string Controller { get; set; }
        public string ClassName { get; set; }
        public string FuncName { get; set; }
        public string PostData { get; set; }
        public string Durum { get; set; }
        public bool Dil { get; set; }
        public List<MenuView> SubMenu = null;


        public string mFunc { get; set; }
    }

}