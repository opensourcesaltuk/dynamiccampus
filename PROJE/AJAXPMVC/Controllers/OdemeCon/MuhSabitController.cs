﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;

namespace AJAXPMVC
{
    public class MuhSabitController : MainClass
    {
        public new string oTema = "MuhSabitController";
        string mTabId = "MuhSabitControllerListesi";

        #region Firma Tanimlari
        public void FirmaIsyeriLoad()
        {
            string Id = this.mTabId;//q.request["TabDivId"];


            jqw liste = new jqw(true);
            liste.AddColumn("Id", "Id", "80px");
            liste.AddDataColumns("Id", "int");
            liste.AddColumn("Firma Kodu", "FirmaKodu", "100px");
            liste.AddDataColumns("FirmaKodu", "string");

            liste.AddColumn("Firma Adı", "FirmaAdi", "100px");
            liste.AddDataColumns("FirmaAdi", "string");

            liste.AddColumn("Unvan", "Unvan", "100px");
            liste.AddDataColumns("Unvan", "string");

            liste.AddColumn("SicilNo", "SicilNo", "100px");
            liste.AddDataColumns("SicilNo", "string");

            liste.AddColumn("Vergi Dairesi", "VergiDairesi", "80px");
            liste.AddDataColumns("VergiDairesi", "string");


            liste.AddColumn("Vergi Numrası", "VergiNumrasi", "80px");
            liste.AddDataColumns("VergiNumrasi", "string");



            //liste.hierarchy("KullaniciId", "OgrenciId");
            
            liste.data =  q.con.CollectionFromSql("SELECT * FROM mh.Firma m WITH(NOLOCK,READUNCOMMITTED)").ToArray().getJsonArray();
            liste.Ozellikler.height = "620px";
            liste.Ozellikler.disabled = false; 
            liste.Ozellikler.columnsResize = true;
            liste.Ozellikler.enableHover = true;
            liste.Ozellikler.filterable = true;
            liste.Ozellikler.filterMode = jqfilterMode.Default;
            liste.Ozellikler.rowdetails = true;
            //liste.Ozellikler.selectionmode = jqselectionmode.singlecell;

            liste.PostRowDetailsFunc(this.oNamespace, this.oClassname, "FirmaIsyeriDetay", "data");
            //liste.AddToolbarButton()

            liste.AddToolbarButton("Ekle", fa_icons.fa_fa_plus, this.m("FirmaEkleDuzenleme", "ekle:true,datarow:" + liste.getRowDatajs()));
            liste.AddToolbarButton("Düzenle", fa_icons.fa_fa_pencil, this.m("FirmaEkleDuzenleme", "ekle:false,datarow:" + liste.getRowDatajs()));
            q.ajax.html("#" + Id, liste.Olustur());
        }
        public void FirmaEkleDuzenleme()
        {
            Ent g = Ent.get("mh.Firma", "Id", true, "Id,FirmaKodu,FirmaAdi,Unvan,SicilNo,MersisNo,NaceKodu,KurulusTarihi,VergiDairesi,VergiNumrasi".Split(','));
            string Id = "0";
            if (q.request["ekle"] == "true")
            {
                g=g.Create();
            }
            else
            {
                Id= q.request["datarow"].GetJObject()["Id"].ToString();

                g = g.Where("Id", Id).FirstOrDefault<Ent>();
            }

            FormModelGenerator frm = new FormModelGenerator();

            frm.FormEkleDuzenle("Firma", "Firma Bilgileri");
            frm.EkleTextBox("Firma", "FirmaKodu",   "Firma Kodu", g["FirmaKodu"].toNullStringValue(), 50, true);
            frm.EkleTextBox("Firma", "FirmaAdi",    "Firma Adı",  g["FirmaAdi"].toNullStringValue(), 200, true);
            frm.EkleTextBox("Firma", "Unvan",       "Unvan",      g["Unvan"].toNullStringValue(), 300, true);
            frm.EkleTextBox("Firma", "SicilNo",     "Sicil No",   g["SicilNo"].toNullStringValue(), 50, true);
            frm.EkleTextBox("Firma", "MersisNo",    "Mersis No",  g["MersisNo"].toNullStringValue(), 50, true);
            frm.EkleTextBox("Firma", "NaceKodu",    "Nace Kodu",  g["NaceKodu"].toNullStringValue(), 50, true);
            frm.EkleTextBoxDate("Firma", "KurulusTarihi", "Kuruluş Tarihi", g["KurulusTarihi"].toSerializeJson(), ViewDateFormat.Date, true);
            frm.EkleTextBoxDec("Firma", "VergiDairesi", "Vergi Dairesi", g["VergiDairesi"].toNullGetDecimalValue(),10,0,0,null,false);
            frm.EkleTextBoxDec("Firma", "VergiNumrasi", "Vergi Numrası", g["VergiNumrasi"].toNullGetDecimalValue(), 10, 0, 0, null, false);

            frm.EkleButton("Firma", this.oNamespace, this.oClassname, "FirmaKaydet", "FormData", "Id:" + Id, "Kaydet", ViewBtnClass.btn_success, fa_icons.fa_fa_plus);


            q.ajax.dialogOpen("dd", "FirmaDialog", "Firma Düzenleme - Ekleme", frm.Olustur());

        } 
        public void FirmaKaydet()
        {
            try
            {
                string Id = q.request["Id"];
                Ent et = Ent.get("mh.Firma","Id",true).Where("Id",Id).FirstOrDefault<Ent>();
                if (et==null)
                {
                    et = Ent.get("mh.Firma", "Id", true).Create();
                }
                et.SetJoject(q.request["FormData"].GetJObject()["Firma"]);

                if (et.Save())
                { 
                    q.ajax.alert("ss", "Kaydedildi");
                    this.FirmaIsyeriLoad();
                    q.ajax.dialogClose("FirmaDialog", "FirmaDialog");
                }
                else
                {

                }
            }
            catch (Exception ex) 
            {
                q.ajax.alert("Hata","Firma Kodu Başka Bir Firma İle Aynı Olamaz."); 
            }
        } 
        public void FirmaIsyeriDetay(string FirmaId="",string Selecttor="")
        {
            if (Selecttor.Length == 0)
            {
                var x = q.request["data"].GetJObject();
                Selecttor = Selecttor.Length == 0 ? ("#" + x["MaterParentGridId"] + " #" + x["id"]) : Selecttor;
                FirmaId = FirmaId.Length == 0 ? x["data"]["Id"].ToString() : FirmaId;
            }




            jqw liste = new jqw(true);
            liste.AddColumn("Id", "Id", "80px");
            liste.AddDataColumns("Id", "int");


            liste.AddColumn("İşyeri Kodu", "IsyeriKodu", "100px");
            liste.AddDataColumns("IsyeriKodu", "string");

            liste.AddColumn("İşyeri Adı", "IsyeriAdi", "100px");
            liste.AddDataColumns("IsyeriAdi", "string");

            liste.AddColumn("Unvan", "Unvan", "100px");
            liste.AddDataColumns("Unvan", "string");

            liste.AddColumn("SicilNo", "SicilNo", "100px");
            liste.AddDataColumns("SicilNo", "string");

            liste.AddColumn("Nace Kodu", "NaceKodu", "80px");
            liste.AddDataColumns("NaceKodu", "string"); 



            //liste.hierarchy("KullaniciId", "OgrenciId");

            liste.data = q.con.CollectionFromSql("SELECT * FROM mh.Isyeri m WITH(NOLOCK,READUNCOMMITTED)").ToArray().getJsonArray();
            liste.Ozellikler.height = "620px";
            liste.Ozellikler.disabled = false;
            liste.Ozellikler.columnsResize = true;
            liste.Ozellikler.enableHover = true;
            liste.Ozellikler.filterable = true;
            liste.Ozellikler.filterMode = jqfilterMode.Default;
            liste.Ozellikler.rowdetails = false;

            liste.AddToolbarButton("Ekle", fa_icons.fa_fa_plus, this.m("IsyeriEkleDuzenlemeDialog",      "Selector:'"+ Selecttor + "',FirmaId:"+ FirmaId ));
            liste.AddToolbarButton("Düzenle", fa_icons.fa_fa_pencil, this.m("IsyeriEkleDuzenlemeDialog", "Selector:'" + Selecttor + "',FirmaId:" + FirmaId + ",datarow:" + liste.getRowDatajs()));
            q.ajax.html(Selecttor, liste.Olustur()); 
        }
        #endregion
        #region Isyeri Tanimlari
        public void IsyeriEkleDuzenlemeDialog()
        {
            string FirmaId = q.request["FirmaId"];
            string Selector = q.request["Selector"];

            string Id = "0";
            if(q.request.ContainsKey("datarow"))
            {
                Id = q.request["datarow"].GetJObject()["Id"].toNullStringValue();
            }
            Ent ent = Ent.get("mh.Isyeri", "Id", true, "Id,FirmaId,IsyeriKodu,IsyeriAdi,Unvan,SicilNo,MersisNo,NaceKodu".Split(','));
            if (Id == "0")
            {
                ent = ent.Create();
            }
            else
            {
                ent = ent.Where("Id", Id).FirstOrDefault<Ent>();
            }

            Ent firma = Ent.get("mh.Firma","Id",true).Where("Id", FirmaId).FirstOrDefault<Ent>();


            FormModelGenerator frm = new FormModelGenerator();

            frm.FormEkleDuzenle("Firma", "Firma Bilgileri");
            frm.EkleLabelBox("Firma", "Id", firma["Id"].toNullStringValue());
            frm.EkleLabelBox("Firma", "FirmaKodu", firma["FirmaKodu"].toNullStringValue());
            frm.EkleLabelBox("Firma", "FirmaAdi", firma["FirmaAdi"].toNullStringValue());
            frm.EkleLabelBox("Firma", "Unvan", firma["Unvan"].toNullStringValue());
            frm.EkleLabelBox("Firma", "SicilNo", firma["SicilNo"].toNullStringValue());
            frm.EkleLabelBox("Firma", "MersisNo", firma["MersisNo"].toNullStringValue());
            frm.EkleLabelBox("Firma", "KurulusTarihi", ((DateTime?)firma["KurulusTarihi"]).GetFormatViewDate(ViewDateFormat.Date));
            frm.EkleLabelBox("Firma", "VergiDairesi", firma["VergiDairesi"].toNullStringValue());
            frm.EkleLabelBox("Firma", "VergiNumrasi", firma["VergiNumrasi"].toNullStringValue());

            frm.FormEkleDuzenle("Isyeri", "İşyeri Bilgileri");

            frm.EkleTextBox("Isyeri", "IsyeriKodu", "İşyeri Kodu", ent["IsyeriKodu"].toNullStringValue(), 50, true);
            frm.EkleTextBox("Isyeri", "IsyeriAdi", "İşyeri Adı", ent["IsyeriAdi"].toNullStringValue(), 200, true);
            frm.EkleTextBox("Isyeri", "Unvan", "Unvan", ent["Unvan"].toNullStringValue(), 300, true);
            frm.EkleTextBox("Isyeri", "SicilNo", "Sicil No", ent["SicilNo"].toNullStringValue(), 50, true);
            frm.EkleTextBox("Isyeri", "MersisNo", "Mersis No", ent["MersisNo"].toNullStringValue(), 50, true);
            frm.EkleTextBox("Isyeri", "NaceKodu", "Nace Kodu", ent["NaceKodu"].toNullStringValue(), 50, true);


            frm.FormEkleDuzenle("IsyeriKaydet","");
            frm.EkleButton("IsyeriKaydet", this.oNamespace, this.oClassname, "IsyeriKaydet", "FormData", "Id:" + Id + ",FirmaId:" + FirmaId + ",Selector:'" + Selector+"'", "Kaydet", ViewBtnClass.btn_success, fa_icons.fa_fa_plus);



            q.ajax.dialogOpen("dd", "IsyeriDialog", "İşyeri Düzenleme - Ekleme", frm.Olustur());

        }
        public void IsyeriKaydet()
        {

            string FirmaId = q.request["FirmaId"];
            string Selector = q.request["Selector"];
            string Id = q.request["Id"];

            var x = q.request["FormData"].GetJObject()["Isyeri"];


            try
            {
                Ent kytIsyeri = Ent.get("mh.Isyeri","Id",true);
                if (Id == "0") {
                    kytIsyeri = kytIsyeri.Create();
                    kytIsyeri["FirmaId"] = FirmaId;
                }else
                {
                    kytIsyeri = kytIsyeri.Where("Id", Id).FirstOrDefault<Ent>();
                }
                kytIsyeri.SetJoject(x);

                if (kytIsyeri.Save())
                {
                    this.FirmaIsyeriDetay(FirmaId, Selector);
                    q.ajax.alert("ss", "Kaydedildi"); 
                    q.ajax.dialogClose("IsyeriDialog", "IsyeriDialog");
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                q.ajax.alert("Hata", "İşyeri Kodu Başka Bir İşyeri Kodu İle Aynı Olamaz.");
            }






        }
        #endregion
        #region Banka Tanimlari
        string mTabBankaId = "#BankaSabitControllerListesi";
        public void BankaLoad()
        {
            jqw liste = new jqw(true);
            liste.data = q.con.CollectionFromSql("SELECT b.Id,b.BankaKodu,b.BankaAdi,b.AktifMi,b.OtsAktifMi FROM mh.Banka b WITH(NOLOCK,READUNCOMMITTED)").ToArray().getJsonArray();

            liste.AddDataColumns("Id", "int");
            liste.AddColumn("Id", "Id", "80px");

            liste.AddDataColumns("BankaKodu", "string");
            liste.AddColumn("Banka Kodu", "BankaKodu", "100px");

            liste.AddDataColumns("BankaAdi", "string");
            liste.AddColumn("Banka Adı", "BankaAdi", "250px");

            liste.AddDataColumns("AktifMi", "bool");
            liste.AddColumn("Aktif Mi?", "AktifMi", "100px",jqcolumntype.checkbox);
             
            liste.AddDataColumns("OtsAktifMi", "bool");
            liste.AddColumn("Ots Aktif Mi?", "OtsAktifMi", "100px", jqcolumntype.checkbox);

            liste.Ozellikler.height = "500px";

            liste.AddToolbarButton("Ekle", fa_icons.fa_fa_plus_circle, this.m("BankaDialog","Ekle:1"));
            liste.AddToolbarButton("Düzenle", fa_icons.fa_fa_edit, this.m("BankaDialog", "Ekle:0,datarow:" + liste.getRowDatajs()));

            q.ajax.html(mTabBankaId, liste.Olustur());

        }

        public void BankaDialog()
        {
            string Id = "0";
            if (q.request["Ekle"] == "0" && !q.request.ContainsKey("datarow"))
            {
                q.ajax.alert("hata","Kayıt Seçmediniz");
                return;
            }
            if (q.request.ContainsKey("datarow"))
            {
                Id=q.request["datarow"].GetJObject()["Id"].toNullStringValue();
            }

            Ent bankaRow = Ent.get("mh.Banka", "Id", true, "Id,BankaKodu,BankaAdi,AktifMi,OtsAktifMi".Split(','));

            if (Id != "0")
            {
                bankaRow = bankaRow.Where("Id", Id).FirstOrDefault<Ent>();
            }

            FormModelGenerator frm = new FormModelGenerator();
            frm.FormEkleDuzenle("Banka", "Banka Bilgileri");
            frm.EkleTextBox("Banka", "BankaKodu", "Banka Kodu", bankaRow["BankaKodu"].toNullStringValue(), 20, true);
            frm.EkleTextBox("Banka", "BankaAdi", "Banka Adı", bankaRow["BankaAdi"].toNullStringValue(), 200, true);
            frm.EkleCheckBox("Banka", "AktifMi", "Aktif Mi?",  bankaRow["AktifMi"].BoolNullValue(true),  true);
            frm.EkleCheckBox("Banka", "OtsAktifMi", "Ots Aktif Mi?",  bankaRow["OtsAktifMi"].BoolNullValue(false),  true);
            frm.EkleButton("Banka", this.oNamespace, this.oClassname, "BankaKaydet", "FormData", "Id:" + Id, "Kaydet", ViewBtnClass.btn_success, fa_icons.fa_fa_save);

            q.ajax.dialogOpen("aa", "BankaDialog", "Banka Ekleme ve Düzenleme", frm.Olustur());


        }

        public void BankaKaydet()
        {
            string Id = q.request["Id"];

            Ent BankaRow=Ent.get("mh.Banka");
            if (Id == "0")
            {
                BankaRow = BankaRow.Create();
            }
            else
            {
                BankaRow = BankaRow.Where("Id", Id).FirstOrDefault<Ent>();
            }
            BankaRow.SetJoject(q.request["FormData"].GetJObject()["Banka"]);
            try
            {
                if (BankaRow.Save())
                {
                    q.ajax.alert("aa","Kaydedildi.");
                    this.BankaLoad();
                    q.ajax.dialogClose("BankaDialog", "BankaDialog");
                }
            }
            catch (Exception ex)
            {
                q.ajax.alert("hata","Banka Kodu veya Adi Kullanımda");
            }
           
        }
        #endregion

        #region Hesap Kasa Listesi Tanımları
        string mTabHesapKasaId = "#HesapSabitControllerListesi";
        public void HesapKasaLoad()
        {
            jqw liste = new jqw(true);
            liste.data = q.con.CollectionFromSql("SELECT * FROM mh.vw_HesapKasa").ToArray().getJsonArray();

            liste.AddDataColumns("Id", "int");
            liste.AddColumn("Id", "Id", "80px");
             

            liste.AddDataColumns("FirmaId", "int");
            liste.AddColumn("FirmaId", "FirmaId", "80px");

            liste.AddDataColumns("FirmaAdi", "string");
            liste.AddColumn("Firma Adı", "FirmaAdi", "100px");


            liste.AddDataColumns("HesapTipiAdi", "string");
            liste.AddColumn("Hesap Tipi", "HesapTipiAdi", "100px");

            liste.AddDataColumns("BankaId", "int");
            liste.AddColumn("BankaId", "BankaId", "80px");


            liste.AddDataColumns("BankaAdi", "string");
            liste.AddColumn("Banka Adı", "BankaAdi", "100px");



            liste.AddDataColumns("Kodu", "string");
            liste.AddColumn("Hesap Kodu", "Kodu", "100px");

            liste.AddDataColumns("Adi", "string");
            liste.AddColumn("Hesap Adı", "Adi", "100px");



            liste.AddDataColumns("ParaBirimiKodu", "string");
            liste.AddColumn("Para Birimi", "ParaBirimiKodu", "100px");

             
            liste.AddDataColumns("AktifMi", "bool");
            liste.AddColumn("Aktif Mi?", "AktifMi", "100px", jqcolumntype.checkbox);



            liste.AddColumn("Sil", "Sil", "100px", jqcolumntype.button,"",null, this.m("HesapKasaSil", "datarow:datarow"));


            liste.Ozellikler.height = "500px";

            liste.AddToolbarButton("Ekle", fa_icons.fa_fa_plus_circle, this.m("HesapKasaDialog", "Ekle:1"));
            liste.AddToolbarButton("Düzenle", fa_icons.fa_fa_edit, this.m("HesapKasaDialog", "Ekle:0,datarow:" + liste.getRowDatajs()));

            q.ajax.html(mTabHesapKasaId, liste.Olustur());

        }
        public void HesapKasaSil()
        {
            if (q.request.ContainsKey("datarow"))
            {
               string Id = q.request["datarow"].GetJObject()["Id"].toNullStringValue();

                Ent sil = Ent.get("mh.HesapKasa").Where("Id",Id).FirstOrDefault<Ent>();
                if (sil.Delete())
                {
                    this.HesapKasaLoad();
                    q.ajax.alert("aa","Silindi.");
                }

            }
            else
            {
                q.ajax.alert("hata","Kayıt Seçmediniz.");
            }

        }
        public void HesapKasaDialog()
        {
            string Id = "0";
            if (q.request["Ekle"] == "0" && !q.request.ContainsKey("datarow"))
            {
                q.ajax.alert("hata", "Kayıt Seçmediniz");
                return;
            }
            if (q.request.ContainsKey("datarow"))
            {
                Id = q.request["datarow"].GetJObject()["Id"].toNullStringValue();
            }

            Ent bankaRow = Ent.get("mh.vw_HesapKasa", "Id");

            if (Id != "0")
            {
                bankaRow = bankaRow.Where("Id", Id).FirstOrDefault<Ent>();
            }
            //Id	FirmaId	FirmaAdi	HesapTipiId	HesapTipiAdi	BankaId	BankaAdi	Kodu	Adi	ParaBirimiId	ParaBirimiKodu	AktifMi
            FormModelGenerator frm = new FormModelGenerator();
            frm.FormEkleDuzenle("HesapKasa", "Hesap,Kasa Bilgileri");

            frm.EkleDropDownList("HesapKasa", "FirmaId", "Firma", bankaRow["FirmaId"].toNullStringValue(), bankaRow["FirmaAdi"].toNullStringValue(),
                q.con.CollectionFromSql("SELECT Id id,FirmaAdi label FROM mh.Firma NOLOCK").ToArray().getJsonArray()
                , true);

            frm.EkleDropDownList("HesapKasa", "HesapTipiId", "Hesap Tipi", bankaRow["HesapTipiId"].toNullStringValue(), bankaRow["HesapTipiAdi"].toNullStringValue(),
              q.con.CollectionFromSql("SELECT Id id,TipAdi label FROM mh.HesapTipi NOLOCK").ToArray().getJsonArray()
              , true);


            frm.EkleDropDownList("HesapKasa", "BankaId", "Banka Adı", bankaRow["BankaId"].toNullStringValue(), bankaRow["BankaAdi"].toNullStringValue(),
              q.con.CollectionFromSql("SELECT Id id, BankaAdi label FROM mh.Banka NOLOCK").ToArray().getJsonArray()
              , false);

            frm.EkleTextBox("HesapKasa", "Kodu", "Kodu", bankaRow["Kodu"].toNullStringValue(), 200, true);
            frm.EkleTextBox("HesapKasa", "Adi", "Adi", bankaRow["Adi"].toNullStringValue(), 200, true);


            frm.EkleDropDownList("HesapKasa", "ParaBirimiId", "Para Birimi", bankaRow["ParaBirimiId"].toNullStringValue(), bankaRow["ParaBirimiKodu"].toNullStringValue(),
                q.con.CollectionFromSql("SELECT Id id,Kodu label FROM mh.ParaBirimi NOLOCK ORDER BY id").ToArray().getJsonArray()
                , true);


            frm.EkleCheckBox("HesapKasa", "AktifMi", "Aktif Mi?", bankaRow["AktifMi"].BoolNullValue(true), true);





            frm.EkleButton("HesapKasa", this.oNamespace, this.oClassname, "HesapKasaKaydet", "FormData", "Id:" + Id, "Kaydet", ViewBtnClass.btn_success, fa_icons.fa_fa_save);

            q.ajax.dialogOpen("aa", "HesapKasaDialog", "Hesap, Kasa Tanımı", frm.Olustur());


        } 
        public void HesapKasaKaydet()
        {
            string Id = q.request["Id"];

            var ViewRow= q.request["FormData"].GetJObject()["HesapKasa"];

            Ent HesapTipi = Ent.get("mh.HesapTipi").Where("Id",ViewRow["HesapTipiId"].toNullStringValue()).FirstOrDefault<Ent>();


            if (HesapTipi["TipKodu"].toNullStringValue() == "banka" && ViewRow["BankaId"].IsNull())
            {

                q.ajax.alert("hata","Hesap tipi \"Banka\" olarak seçildiyse \"Banka Adı\" alanı dolu olmalıdır.");
                return;

            }




            Ent HesapKasaRow = Ent.get("mh.HesapKasa");




            if (Id == "0")
            {
                HesapKasaRow = HesapKasaRow.Create();
            }
            else
            {
                HesapKasaRow = HesapKasaRow.Where("Id", Id).FirstOrDefault<Ent>();
            }
            HesapKasaRow.SetJoject(ViewRow);
            try
            {
                if (HesapKasaRow.Save())
                {
                    q.ajax.alert("aa", "Kaydedildi.");
                    this.HesapKasaLoad();
                    q.ajax.dialogClose("HesapKasaDialog", "HesapKasaDialog");
                }
            }
            catch (Exception ex)
            {
                q.ajax.alert("hata", "Banka Kodu veya Adi Kullanımda");
            }

        }
        #endregion
        #region Burs Tanimlari 
        string mTabBursId = "#BursControllerListesi";
        public void BursLoad()
        {
            jqw liste = new jqw(true);
            liste.data = q.con.CollectionFromSql("SELECT * FROM mh.BursOran NOLOCK").ToArray().getJsonArray();

            liste.AddDataColumns("Id", "int");
            liste.AddColumn("Id", "Id", "80px");
             

            liste.AddDataColumns("BursAdi", "string");
            liste.AddColumn("Burs Adı", "BursAdi", "250px");

             

            liste.AddDataColumns("BursOran", "decimal");
            liste.AddColumn("Oran", "BursOran", "80px",jqcolumntype.numberinput);
             

            liste.AddDataColumns("AktifMi", "bool");
            liste.AddColumn("Aktif Mi?", "AktifMi", "100px", jqcolumntype.checkbox);



            liste.AddColumn("Sil", "Sil", "100px", jqcolumntype.button, "", null, this.m("BursSil", "datarow:datarow"));


            liste.Ozellikler.height = "500px";

            liste.AddToolbarButton("Ekle", fa_icons.fa_fa_plus_circle, this.m("BursDialog", "Ekle:1"));
            liste.AddToolbarButton("Düzenle", fa_icons.fa_fa_edit, this.m("BursDialog", "Ekle:0,datarow:" + liste.getRowDatajs()));

            q.ajax.html(mTabBursId, liste.Olustur());

        }
        public void BursSil()
        {
            if (q.request.ContainsKey("datarow"))
            {
                string Id = q.request["datarow"].GetJObject()["Id"].toNullStringValue();

                Ent sil = Ent.get("mh.BursOran").Where("Id", Id).FirstOrDefault<Ent>();
                if (sil==null || sil.Delete())
                {
                    this.BursLoad();
                    q.ajax.alert("aa", "Silindi.");
                }

            }
            else
            {
                q.ajax.alert("hata", "Kayıt Seçmediniz.");
            }

        }
        public void BursDialog()
        {
            string Id = "0";
            if (q.request["Ekle"] == "0" && !q.request.ContainsKey("datarow"))
            {
                q.ajax.alert("hata", "Kayıt Seçmediniz");
                return;
            }
            if (q.request.ContainsKey("datarow"))
            {
                Id = q.request["datarow"].GetJObject()["Id"].toNullStringValue();
            }

            Ent BursRow = Ent.get("mh.BursOran", "Id");

            if (Id != "0")
            {
                BursRow = BursRow.Where("Id", Id).FirstOrDefault<Ent>();
            }
            //Id	FirmaId	FirmaAdi	HesapTipiId	HesapTipiAdi	BankaId	BankaAdi	Kodu	Adi	ParaBirimiId	ParaBirimiKodu	AktifMi
            FormModelGenerator frm = new FormModelGenerator();
            frm.FormEkleDuzenle("BursOran", "Burs Bilgileri");  
            frm.EkleTextBox("BursOran", "BursAdi", "Burs Adı", BursRow["BursAdi"].toNullStringValue(), 200, true);
            if (Id == "0")
            {
                frm.EkleTextBoxDec("BursOran", "BursOran", "Oran", BursRow["BursOran"].toNullGetDecimalValue(), 3, 2, 0, 100, true);

            }
            else
            {

                frm.EkleLabelBox("BursOran", "BursOran",  BursRow["BursOran"].toNullGetDecimalValue().ToString());
            }
            frm.EkleCheckBox("BursOran", "AktifMi", "Aktif Mi?", BursRow["AktifMi"].BoolNullValue(true), true); 

            frm.EkleButton("BursOran", this.oNamespace, this.oClassname, "BursKaydet", "FormData", "Id:" + Id, "Kaydet", ViewBtnClass.btn_success, fa_icons.fa_fa_save);

            q.ajax.dialogOpen("aa", "BursDialog", "Burs Tanımı", frm.Olustur());


        }
        public void BursKaydet()
        {
            string Id = q.request["Id"]; 
            var ViewRow = q.request["FormData"].GetJObject()["BursOran"]; 
            Ent BursRow = Ent.get("mh.BursOran"); 
            if (Id == "0")
            {
                BursRow = BursRow.Create();
            }
            else
            {
                BursRow = BursRow.Where("Id", Id).FirstOrDefault<Ent>();
            }
            BursRow.SetJoject(ViewRow);
            try
            {
                if (BursRow.Save())
                {
                    q.ajax.alert("aa", "Kaydedildi.");
                    this.BursLoad();
                    q.ajax.dialogClose("BursDialog", "BursDialog");
                }
            }
            catch (Exception ex)
            {
                q.ajax.alert("hata", ex.ToString());
            }

        }
        #endregion 
        #region UcretTipi Tanimlari 
        string mTabUcretTipiId = "#UcretTipiControllerListesi";
        public void UcretTipiLoad()
        {
            jqw liste = new jqw(true);
            liste.data = q.con.CollectionFromSql("SELECT * FROM mh.vw_UcretTipi NOLOCK").ToArray().getJsonArray();

            liste.AddDataColumns("Id", "int");
            liste.AddColumn("Id", "Id", "80px");


            liste.AddDataColumns("Kodu", "string");
            liste.AddColumn("Kodu", "Kodu", "250px");

            liste.AddDataColumns("Adi", "string");
            liste.AddColumn("Adı", "Adi", "250px");




            liste.AddDataColumns("UcretTipiId", "int");
            liste.AddColumn("Bağlı Ücret Tipi Id", "UcretTipiId", "80px");

            liste.AddDataColumns("UcretTipiKodu", "string");
            liste.AddColumn("Üst Kodu", "UcretTipiKodu", "250px");

            liste.AddDataColumns("UcretTipiAdi", "string");
            liste.AddColumn("Üst Adı", "UcretTipiAdi", "250px");


            liste.AddDataColumns("YilBazliKontrolEdilsinMi", "bool");
            liste.AddColumn("Üst Ücret Yil Bazlı Kontrol Edilsin Mi??", "YilBazliKontrolEdilsinMi", "200px", jqcolumntype.checkbox);


            liste.AddDataColumns("ZorunluPesinMi", "bool");
            liste.AddColumn("Peşin Mi?", "ZorunluPesinMi", "100px", jqcolumntype.checkbox);



            liste.AddColumn("Sil", "Sil", "100px", jqcolumntype.button, "", null, this.m("UcretTipiSil", "datarow:datarow"));


            liste.Ozellikler.height = "500px";

            liste.AddToolbarButton("Ekle", fa_icons.fa_fa_plus_circle, this.m("UcretTipiDialog", "Ekle:1"));
            liste.AddToolbarButton("Düzenle", fa_icons.fa_fa_edit, this.m("UcretTipiDialog", "Ekle:0,datarow:" + liste.getRowDatajs()));

            q.ajax.html(mTabUcretTipiId, liste.Olustur());

        }
        public void UcretTipiSil()
        {
            if (q.request.ContainsKey("datarow"))
            {
                string Id = q.request["datarow"].GetJObject()["Id"].toNullStringValue();

                Ent sil = Ent.get("mh.UcretTipi").Where("Id", Id).FirstOrDefault<Ent>();
                if (sil == null || sil.Delete())
                {
                    this.UcretTipiLoad();
                    q.ajax.alert("aa", "Silindi.");
                }

            }
            else
            {
                q.ajax.alert("hata", "Kayıt Seçmediniz.");
            }

        }
        public void UcretTipiDialog()
        {
            string Id = "0";
            if (q.request["Ekle"] == "0" && !q.request.ContainsKey("datarow"))
            {
                q.ajax.alert("hata", "Kayıt Seçmediniz");
                return;
            }
            if (q.request.ContainsKey("datarow"))
            {
                Id = q.request["datarow"].GetJObject()["Id"].toNullStringValue();
            }

            Ent row = Ent.get("mh.vw_UcretTipi", "Id");

            if (Id != "0")
            {
                row = row.Where("Id", Id).FirstOrDefault<Ent>();
            }
            //Id	FirmaId	FirmaAdi	HesapTipiId	HesapTipiAdi	BankaId	BankaAdi	Kodu	Adi	ParaBirimiId	ParaBirimiKodu	AktifMi
            FormModelGenerator frm = new FormModelGenerator();
            frm.FormEkleDuzenle("UcretTipi", "Ücret Tipi Bilgileri");
            frm.EkleTextBox("UcretTipi", "Kodu", "Kodu", row["Kodu"].toNullStringValue(), 200, true);
            frm.EkleTextBox("UcretTipi", "Adi", "Adi", row["Adi"].toNullStringValue(), 200, true);

            frm.EkleDropDownList("UcretTipi", "UcretTipiId", "Girilen ücrettipinden önce hangi ücret tipi ödenmelidir?", row["UcretTipiId"].toNullStringValue(), row["UcretTipiAdi"].toNullStringValue(), q.con.CollectionFromSql("SELECT Id id,Adi label FROM mh.vw_UcretTipi").ToArray().getJsonArray(), false);

            frm.EkleCheckBox("UcretTipi", "YilBazliKontrolEdilsinMi", "Önce Tahsil Edilen Ücret tipi yil bazlı mı kontrol edilsin?", row["YilBazliKontrolEdilsinMi"].BoolNullValue(false), true);
            frm.EkleCheckBox("UcretTipi", "ZorunluPesinMi", "Tahsilat Peşin Mi Yapılır?", row["ZorunluPesinMi"].BoolNullValue(true), true);


            frm.EkleLabelBox("UcretTipi", "Açıklama","Üst Ücreti Tipi : Örneğin Depozito ve Yurt tipinde iki farklı ücret tipiniz olsun.Depozito ücretini ödemeden yurt ücretini tahsil etmek istemediğinizde Üst Ücret Tanımlarsanız Öncelikli Olarak Üst Ücreti Sistem Tahsil Etmek İsteyecektir.<br>İstisnası : Dekont girişi çek senet girişlerinde dikkate alınmamaktadır.");


            frm.EkleButton("UcretTipi", this.oNamespace, this.oClassname, "UcretTipiKaydet", "FormData", "Id:" + Id, "Kaydet", ViewBtnClass.btn_success, fa_icons.fa_fa_save);

            q.ajax.dialogOpen("aa", "UcretTipiDialog", "Ücret Tipi Tanımı", frm.Olustur());


        }
        public void UcretTipiKaydet()
        {
            string Id = q.request["Id"];
            var ViewRow = q.request["FormData"].GetJObject()["UcretTipi"];
            Ent row = Ent.get("mh.UcretTipi");
            if (Id == "0")
            {
                row = row.Create();
            }
            else
            {
                row = row.Where("Id", Id).FirstOrDefault<Ent>();
            }
            row.SetJoject(ViewRow);
            try
            {
                if (row.Save())
                {
                    q.ajax.alert("aa", "Kaydedildi.");
                    this.UcretTipiLoad();
                    q.ajax.dialogClose("UcretTipiDialog", "UcretTipiDialog");
                }
            }
            catch (Exception ex)
            {
                q.ajax.alert("hata", ex.ToString());
            }

        }
        #endregion

        #region Birim Muhasebe Tanimi
        string mTabBirimMuhasebeListesi = "#BirimMuhasebeListesi";
        public void BirimMuhasebeTanimiLoad()
        {
            FormModelGenerator frm = new FormModelGenerator();
            string Id=frm.FormEkleDuzenle("dada", "Birim Muhasebe Tanımları");
            Id = "#" + Id + " #Nesneler";

            jqw liste = new jqw(true);
            liste.data = q.con.CollectionFromSql("SELECT * FROM br.vw_FakulteProgramListesi a").ToArray().getJsonArray();
           

            liste.AddColumn("Düzenle", "Edit", "100px", jqcolumntype.button,"",null,this.m("BirimMuhasebeTanimiDuzenle","datarow:datarow"));
            liste.AddDataColumns("ProgramId", "int");
            liste.AddColumn("Id", "ProgramId", "80px", jqcolumntype.textbox);

            liste.AddDataColumns("AkademikBirimAdi", "string");
            liste.AddColumn("Üst Birim Adı", "AkademikBirimAdi", "250px", jqcolumntype.textbox);

            liste.AddDataColumns("ProgramAdi", "string");
            liste.AddColumn("Program Adı", "ProgramAdi", "250px", jqcolumntype.textbox);

            liste.AddDataColumns("IsyeriId", "string");
            liste.AddColumn("IsyeriId", "IsyeriId", "80px", jqcolumntype.textbox);


            liste.AddDataColumns("IsyeriAdi", "string");
            liste.AddColumn("İşyeri", "IsyeriAdi", "150px", jqcolumntype.textbox);


            liste.AddDataColumns("PesinIndrimBursOranAdi", "string");
            liste.AddColumn("Peşin Burs Oranı", "PesinIndrimBursOranAdi", "80px", jqcolumntype.textbox);

            liste.AddDataColumns("MaxTaksitSayisiOts", "string");
            liste.AddColumn("OTS Taksit Limit", "MaxTaksitSayisiOts", "80px", jqcolumntype.textbox);


            liste.AddDataColumns("MaxTaksitSayisiKrediKarti", "string");
            liste.AddColumn("K.Kartı Taksit Limit", "MaxTaksitSayisiKrediKarti", "80px", jqcolumntype.textbox);


            liste.AddDataColumns("PesinMiTahsilEdilir", "string");
            liste.AddColumn("Peşin Mi Tahsil Edilir?", "PesinMiTahsilEdilir", "100px", jqcolumntype.checkbox);

            liste.Ozellikler.height = "800px";
            liste.Ozellikler.filterable = true;
            liste.Ozellikler.columnsResize = true;
        

            q.ajax.html(mTabBirimMuhasebeListesi, frm.Olustur());
            q.ajax.html(Id, liste.Olustur());
        }
        public void BirimMuhasebeTanimiDuzenle()
        {
            var x = q.request["datarow"].GetJObject();
            if (x != null)
            {
                Ent row = Ent.get("br.vw_FakulteProgramListesi").Where("ProgramId", x["ProgramId"].toNullGetDecimalValue()).FirstOrDefault<Ent>();
                // AkademikBirimId	AkademikBirimUstBirimId	AkademikBirimTipId	AkademikBirimTipAdi	AkademikBirimTipKodu	AkademikBirimAdi	AkademikBirimAdiGlobal	AkademikBirimSiralama	AkademikBirimAktifId	AkademikBirimAktifAdi	AkademikBirimBirimKodu	ProgramId	ProgramUstBirimId	ProgramTipId	ProgramTipAdi	ProgramTipKodu	ProgramAdi	ProgramAdiGlobal	ProgramSiralama	ProgramAktifId	ProgramAktifAdi	ProgramBirimKodu	AzamiYariYil	KacYariYil	WebGorunsunId	HazirlikVarMi	HazirlikModulId	IsyeriId	PesinIndrimBursOranId	PesinIndrimBursOranAdi	PesinIndrimBursOran	MaxTaksitSayisiOts	MaxTaksitSayisiKrediKarti	PesinMiTahsilEdilir
                FormModelGenerator frm = new FormModelGenerator();
                string ModelAdi = "BirimOzellik";
                frm.FormEkleDuzenle(ModelAdi, "Birim Muhasebe Tanımları");
                frm.EkleLabelBox(ModelAdi, "Düzenlenen Birim", row["AkademikBirimAdi"].toNullStringValue() + " - " + row["ProgramAdi"].toNullStringValue());

                frm.EkleDropDownList(ModelAdi, "IsyeriId", "Bağlı Olduğu İşyeri", row["IsyeriId"].toNullStringValue(), row["IsyeriAdi"].toNullStringValue(), q.sq.getTipData("SELECT IsyeriId id, IsyeriAdi label,FirmaAdi 'group' FROM mh.vw_FirmaIsyeri", "vw_FirmaIsyeri",false), true);
                 
                frm.EkleDropDownList(ModelAdi, "PesinIndrimBursOranId", "Peşin İndirim Burs Oranı", row["PesinIndrimBursOranId"].toNullStringValue(), row["PesinIndrimBursOranAdi"].toNullStringValue(), q.sq.getTipData("SELECT Id AS id,BursAdi AS label FROM mh.BursOran WITH(NOLOCK)", "mh.BursOran", false), true);

                frm.EkleTextBoxDec(ModelAdi, "MaxTaksitSayisiOts", "OTS Taksit Sınırı", row["MaxTaksitSayisiOts"].toNullGetDecimalValue(), 3, 0, 0, 99, true);
                frm.EkleTextBoxDec(ModelAdi, "MaxTaksitSayisiKrediKarti", "Kredi Kartı Taksit Sınırı", row["MaxTaksitSayisiKrediKarti"].toNullGetDecimalValue(), 3, 0, 0, 99, true);

                frm.EkleCheckBox(ModelAdi, "PesinMiTahsilEdilir", "Birim Ücretleri Zorunlu Peşin Mi Tahsil Edilsin", row["PesinMiTahsilEdilir"].BoolNullValue(false), true);

                frm.EkleButton(ModelAdi, oNamespace, oClassname, "BirimMuhasebeTanimiKaydet", "FormData", "Id:" + x["ProgramId"].toNullStringValue(), " Kaydet", ViewBtnClass.btn_success, fa_icons.fa_fa_plus);


                q.ajax.dialogOpen("BirimMuhasebeTanimlariDialog", "BirimMuhasebeTanimlariDialog", "Birim Muhasebe Tanımları Düzenleme", frm.Olustur());

            }
        }
        public void BirimMuhasebeTanimiKaydet()
        {
            var x = q.request["FormData"].GetJObject();

            if (x != null)
            {
                var Id = q.request["Id"].toNullGetDecimalValue();

                Ent ent = Ent.get("br.BirimGenelOzellik").Where("Id", Id).FirstOrDefault<Ent>();


                ent.SetJoject(x["BirimOzellik"]);
                try
                {

                    if (ent.Save())
                    {
                        q.ajax.alert("aa", "Kaydedildi.");
                        q.ajax.dialogClose("BirimMuhasebeTanimlariDialog", "BirimMuhasebeTanimlariDialog");
                        this.BirimMuhasebeTanimiLoad();
                    }
                }
                catch (Exception ex)
                {
                    q.ajax.alert("hata", "Kaydedilmedi\n" + ex.Message);
                }

            }

        }
        #endregion

        #region Birim Yillik Ucretleri

        string YilUcretleriDiv = "#UcretTipiControllerListesi";
        public void YilUcretleriLoad()
        {
            FormModelGenerator frm = new FormModelGenerator();

            frm.FormEkleDuzenle("YilSec", "Yıl Seçimi");

            frm.EkleDropDownList("YilSec", "YilId", "Yıl Seçimi", "", "", q.sq.getTipData("SELECT Id id, YilAdi label FROM tpl.vw_Yillar ORDER BY id DESC", "vw_Yillar", false), true);

            string IdListe = frm.FormEkleDuzenle("YilSecListe", "");

            frm.EkleButton("YilSec", oNamespace, oClassname, "YilUcretleriListe", "FormData", "ListeId: "+IdListe.toJavaScriptValue(), "YilUcretleriListe", ViewBtnClass.btn_default, fa_icons.fa_fa_edit);

            q.ajax.html(YilUcretleriDiv, frm.Olustur());
             
        }
        public void YilUcretleriListe()
        {
            int YilId = (int)q.request["FormData"].GetJObject()["YilSec"]["YilId"].toNullGetDecimalValue();
            string ListeDivId = "#" + q.request["ListeId"].toNullStringValue();
            YilUcretleriListeIslem(YilId, ListeDivId);
        }
        public void YilUcretleriListeIslem(int YilId, string ListeDivId)
        {
            //int YilId = (int)q.request["FormData"].GetJObject()["YilSec"]["YilId"].toNullGetDecimalValue();
            //string ListeDivId = "#"+q.request["ListeId"].toNullStringValue();


            string SQL = @"SELECT  a.ProgramId AS BirimId ,
                                yl.YilAdi YilAdi,
                                yl.Id AS YilId ,
                                a.AkademikBirimAdi ,
                                a.ProgramAdi ,
                                ucr.BrutUcret,
                                ucr.Id
                        FROM    br.vw_FakulteProgramListesi a
                                INNER JOIN tpl.vw_Yillar yl ON yl.Id = @YilId
                                LEFT JOIN mh.BirimYilUcretleri ucr ON ucr.BirimId = a.ProgramId AND ucr.YilId = yl.Id".Replace("@YilId", YilId.ToString());


            jqw liste = new jqw(true);
            liste.data = q.con.CollectionFromSql(SQL).ToArray().getJsonArray();

            liste.AddDataColumns("BirimId", "int");
            liste.AddDataColumns("YilAdi", "string");
            liste.AddDataColumns("YilId", "int");
            liste.AddDataColumns("AkademikBirimAdi", "string");
            liste.AddDataColumns("ProgramAdi", "string");
            liste.AddDataColumns("BrutUcret", "decimal");
            liste.AddDataColumns("Id", "int"); 

            liste.AddColumn("BirimId", "BirimId", "80px",jqcolumntype.textbox,"",null,"",false); 
            liste.AddColumn("Yıl", "YilAdi", "100px",jqcolumntype.textbox,"",null,"",false); 
            liste.AddColumn("Akademik Birim", "AkademikBirimAdi",  "200px",jqcolumntype.textbox,"",null,"",false); 
            liste.AddColumn("Program", "ProgramAdi",  "200px",jqcolumntype.textbox,"",null,"",false); 
            liste.AddColumn("Brüt Ücret", "BrutUcret", "150px",jqcolumntype.numberinput,"",null,"",true);  


            liste.Ozellikler.height = "800px";
            liste.Ozellikler.groupable = true;
            liste.Ozellikler.selectionmode = jqselectionmode.singlecell;
            liste.Ozellikler.editable = true;
            
            liste.AddToolbarButton("Kaydet", fa_icons.fa_fa_edit, m("YilUcretleriKaydet", "ListeDivId:"+ListeDivId.toJavaScriptValue()+", Liste:"+liste.getDataJs()+", YilId:"+YilId.ToString()),ViewBtnClass.btn_success); 

            q.ajax.html(ListeDivId, liste.Olustur("YillikBrutUcretTanimlariBirim"));

        }
        public void YilUcretleriKaydet()
        {
            int YilId = (int)q.request["YilId"].toNullGetDecimalValue();
            string ListeDivId = q.request["ListeDivId"].toNullStringValue();

            var array = q.request["Liste"].GetJArray();

            for (int i = 0; i < array.Count; i++)
            {
                var row = array[i];
                Ent ent = Ent.get("mh.BirimYilUcretleri");
                if (row["Id"].toNullGetDecimalValue() == null)
                    ent = ent.Create();
                else
                {
                    ent = ent.Where("Id", row["Id"].toNullGetDecimalValue()).FirstOrDefault<Ent>();
                }
                if (ent["BrutUcret"].toNullGetDecimalValue() != row["BrutUcret"].toNullGetDecimalValue())
                {
                    ent["BirimId"] = row["BirimId"].toNullGetDecimalValue();
                    ent["YilId"] = row["YilId"].toNullGetDecimalValue();
                    ent["BrutUcret"] = row["BrutUcret"].toNullGetDecimalValue();
                    ent["KullaniciId"] = q.userdata.user.Id;
                    if (ent.Save())
                    {

                    }
                }
            }
            YilUcretleriListeIslem(YilId, ListeDivId);
            q.ajax.alert("Kaydedildi", "Kaydedildi");

        }
        #endregion
        #region Banka Kredi Karti Calisma Sekilleri
        string KrediKartiCalismasiDiv = "#KrediKartiCalismasi";
        public void KrediKartiCalismasi()
        {
            string SQL = @"SELECT  
                                kt.Id ,
	                            frm.Id AS FirmaId,
	                            frm.FirmaAdi AS FirmaAdi,
                                bnk.Id BankaId,
                                bnk.BankaAdi AS BankaAdi,
                                ISNULL(kt.AktifMi, 0)  AktifMi,
                                CAST(frm.Id AS NVARCHAR(10))+':'+CAST(bnk.Id AS NVARCHAR(10))+':'+CAST(kt.Id AS NVARCHAR(10)) HesapKasaId ,
	                            frm.FirmaKodu+', '+bnk.BankaAdi+', '+ ks.Adi AS HesapAdi,
                                kt.TekCekimMerkezi ,
                                kt.KomisyonIlkTaksittenCekilsin ,
                                kt.ZorunluTaksitSayisi ,
                                kt.IlkTaksitGun ,
                                kt.IlkTaksitKom ,
                                kt.TaksitGunAraligi ,
                                kt.TaksitKomisyon ,
                                kt.PesinGunAraligi ,
                                kt.PesinKomisyon,
	                            kt.KullaniciId
                            FROM    mh.Banka bnk WITH(NOLOCK)
                                    INNER JOIN mh.Firma frm WITH(NOLOCK) ON 1 = 1
                                    LEFT JOIN mh.KrediKartiCalismasi kt WITH(NOLOCK) ON kt.FirmaId = frm.Id AND kt.BankaId = bnk.Id
                                    LEFT JOIN mh.vw_HesapKasa ks ON ks.Id = kt.HesapKasaId AND ks.HesapTipKodu = 'banka'
                            ORDER BY FirmaAdi,bnk.BankaAdi";


            jqw liste = new jqw(true);
            liste.data = q.con.CollectionFromSql(SQL).ToArray().getJsonArray();


            liste.AddDataColumns("Id", "int");
            liste.AddDataColumns("FirmaId", "int");
            liste.AddDataColumns("FirmaAdi", "string");
            liste.AddDataColumns("BankaId", "int");
            liste.AddDataColumns("BankaAdi", "string");
            liste.AddDataColumns("AktifMi", "bool");
            liste.AddDataColumns("HesapKasaId", "int");
            liste.AddDataColumns("HesapAdi", "string");
            liste.AddDataColumns("TekCekimMerkezi", "bool");
            liste.AddDataColumns("KomisyonIlkTaksittenCekilsin", "bool");
            liste.AddDataColumns("ZorunluTaksitSayisi", "int");
            liste.AddDataColumns("IlkTaksitGun", "int");
            liste.AddDataColumns("IlkTaksitKom", "decimal");
            liste.AddDataColumns("TaksitGunAraligi", "int");
            liste.AddDataColumns("TaksitKomisyon", "decimal");
            liste.AddDataColumns("PesinGunAraligi", "int");
            liste.AddDataColumns("PesinKomisyon", "decimal");
            liste.AddDataColumns("KullaniciId", "int");

            liste.AddColumn("Firma", "FirmaAdi", "100px", jqcolumntype.textbox, "", null, "", false);
            liste.AddColumn("Banka", "BankaAdi", "100px", jqcolumntype.textbox, "", null, "", false);
            liste.AddColumn("Aktif Mi?", "AktifMi", "60px", jqcolumntype.checkbox, "", null, "", true);
            liste.AddColumn("Hesap Adı", "HesapKasaId", "300px", jqcolumntype.dropdownlist, "HesapAdi", q.sq.getTipData("SELECT CAST(hsp.FirmaId AS NVARCHAR(10))+':'+CAST(hsp.BankaId AS NVARCHAR(10))+':'+CAST(hsp.Id AS NVARCHAR(10)) id, hsp.FirmaKodu+', '+hsp.BankaAdi+', '+hsp.Adi label FROM mh.vw_HesapKasa hsp WHERE hsp.HesapTipKodu='banka'", "vw_HesapKasa_banka"), "", true);
            liste.AddColumn("Tek Çekim Mkz.", "TekCekimMerkezi", "60px", jqcolumntype.checkbox, "", null, "", true);
            liste.AddColumn("TümKomİlkTak.Ck.", "KomisyonIlkTaksittenCekilsin", "60px", jqcolumntype.checkbox, "", null, "", true);
            liste.AddColumn("Zorunlu Taksit Sayisi", "ZorunluTaksitSayisi", "150px", jqcolumntype.numberinput, "", null, "", true);
            liste.AddColumn("İlk Tak. Gün", "IlkTaksitGun", "150px", jqcolumntype.numberinput, "", null, "", true);
            liste.AddColumn("İlk Tak. Kom", "IlkTaksitKom", "150px", jqcolumntype.numberinputdec5, "", null, "", true);
            liste.AddColumn("Tak. Gün", "TaksitGunAraligi", "150px", jqcolumntype.numberinput, "", null, "", true);
            liste.AddColumn("Tak. Kom", "TaksitKomisyon", "150px", jqcolumntype.numberinputdec5, "", null, "", true);
            liste.AddColumn("Peş. Gün", "PesinGunAraligi", "150px", jqcolumntype.numberinput, "", null, "", true);
            liste.AddColumn("Peş. Kom", "PesinKomisyon", "150px", jqcolumntype.numberinputdec5, "", null, "", true); 


            liste.Ozellikler.height = "600px";
            liste.Ozellikler.groupable = true;
            liste.Ozellikler.selectionmode = jqselectionmode.singlecell;
            liste.Ozellikler.editable = true;

            liste.AddToolbarButton("Kaydet", fa_icons.fa_fa_edit, m("YilUcretleriKaydet", "Liste:" + liste.getDataJs()), ViewBtnClass.btn_success);

            q.ajax.html(KrediKartiCalismasiDiv, liste.Olustur("YillikBrutUcretTanimlariBirim"));
        }
        #endregion
    }
}

