﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AJAXPMVC.Ents
{
    [TableAttribute(Scheme = "brc.")]
    public class Borc : Ent
    {
        [TableAttribute(Primary = true, Identity = true)]
        public int Id { get; set; }
        public long DonemId { get; set; }
        public int UcretTipiId { get; set; }
        public long KullaniciId { get; set; }
        public long? OgrenciBolumId { get; set; }
        public long? OgrenciDonemKayitId { get; set; }
        public DateTime? SonOdemeTarihi { get; set; }
        public bool PesinOdenir { get; set; }
        public decimal BrutUcret { get; set; }
        public decimal NakitIndirimTutari { get; set; }
        public int EnBuyukTaksitSayisi { get; set; }
        public int ParaBirimiId { get; set; }
    } 
    [TableAttribute(Scheme = "brc.")]
    public class BorcBursOran : Ent
    {
        [TableAttribute(Primary = true, Identity = true)]
        public int Id { get; set; }
        public int BorcId { get; set; }
        public int BursOranId { get; set; }
        public int Sira { get; set; }
        public decimal Tutar { get; set; }
        public DateTime OlusturmaTarihi { get; set; }
        public DateTime? SilmeTarihi { get; set; }
        public bool SilDurum { get; set; }
        public bool PesinBursOranMi { get; set; }
    }

    [TableAttribute(Scheme = "brc.")]
    public class vw_Borc : Ent
    {
        [TableAttribute(Primary = true, Identity = true)]
        public int Id { get; set; }
        public long DonemId { get; set; }
        public string DonemAdi { get; set; }
        public long YilId { get; set; }
        public string YilAdi { get; set; }
        public int UcretTipiId { get; set; }
        public string UcretTipiKodu { get; set; }
        public string UcretTipiAdi { get; set; }
        public bool UcretTipiZorunluPesinMi { get; set; }
        public int? UstUcretTipiId { get; set; }
        public string UstUcretTipiKodu { get; set; }
        public string UstUcretTipiAdi { get; set; }
        public bool UstYilBazliKontrolEdilsinMi { get; set; }
        public long KullaniciId { get; set; }
        public string KimlikNo { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Cinsiyet { get; set; }
        public long? OgrenciBolumId { get; set; }
        public string OgrenciNumarasi { get; set; }
        public long? AkademikBirimId { get; set; }
        public long? AkademikBirimTipId { get; set; }
        public string AkademikBirimTipKodu { get; set; }
        public string AkademikBirimAdi { get; set; }
        public long? AkademikBirimAktifId { get; set; }
        public long? ProgramId { get; set; }
        public string ProgramAdi { get; set; }
        public string ProgramTipAdi { get; set; }
        public string ProgramBirimKodu { get; set; }
        public long? OgrenciDonemKayitId { get; set; }
        public int? Sinif { get; set; }
        public string SinifAdi { get; set; }
        public string SinifAciklama { get; set; }
        public string OgrenciDonemKayitDonemAdi { get; set; }
        public long? OgrenciDonemKayitDonemId { get; set; }
        public DateTime? SonOdemeTarihi { get; set; }
        public bool PesinOdenir { get; set; }
        public decimal BrutUcret { get; set; }
        public float? ToplamIndirimOrani { get; set; }
        public decimal NakitIndirimTutari { get; set; }
        public float? NetUcret { get; set; }
        public int EnBuyukTaksitSayisi { get; set; }
        public int ParaBirimiId { get; set; }
        public string ParaBirimiKodu { get; set; }
        public string ParaBirimiSembolu { get; set; }
        public bool ParaBirimiAktifMi { get; set; }
        public int ParaBirimiEntegrasyonKodu { get; set; }
        public int MuhasebeIsyeriId { get; set; }
        public string MuhasebeIsyeriAdi { get; set; }
        public int FirmaId { get; set; }
        public string FirmaAdi { get; set; }
        public int? MuhasebeMaxTaksitSayisiOts { get; set; }
        public int? MuhasebeMaxTaksitSayisiKrediKarti { get; set; }
        public int? MuhasebePesinIndrimBursOranId { get; set; }
        public string MuhasebePesinIndrimBursOranAdi { get; set; }
        public decimal? MuhasebePesinIndrimBursOran { get; set; }
        public bool? MuhasebePesinMiTahsilEdilir { get; set; }
    }

}