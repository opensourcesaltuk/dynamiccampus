﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AJAXPMVC.Ents
{
    [TableAttribute(Scheme = "Kul.")]
    public class Kullanici : Ent
    {
        [TableAttribute(Primary = true, Identity = true)]
        public long Id { get; set; }
        public string KimlikNo { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Cinsiyet { get; set; }
        public long? DilId { get; set; }
    }


    [TableAttribute(Scheme = "Kul.")]
    public class vw_KullaniciBilgileri : Ent
    {
        [TableAttribute(Primary = true, Identity = true)]
        public long Id { get; set; }
        public string KimlikNo { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Cinsiyet { get; set; }
        public DateTime? DogumTarihi { get; set; }
        public string DogumYeri { get; set; }
        public string BabaAdi { get; set; }
        public string AnneAdi { get; set; }
        public string NufusCuzdaniSeriNo { get; set; }
        public string NufusCuzdaniNo { get; set; }
        public string KoyMah { get; set; }
        public string CiltNo { get; set; }
        public string AileSiraNo { get; set; }
        public string VerilisNedeni { get; set; }
        public DateTime? VerilisTarihi { get; set; }
        public string PasaportNo { get; set; }
        public long? UyrukId { get; set; }
        public long? NufusaKayitliOlduguIl { get; set; }
        public long? NufusaKayitliOlduguIlce { get; set; }
        public long? MedeniHal { get; set; }
        public long? Kangrubu { get; set; }
        public string KanGrubuAdi { get; set; }
        public string MedeniHaliAdi { get; set; }
        public string NufusaKayitliOlduguIlAdi { get; set; }
        public string NufusaKayitliOlduguIlPlakaKodu { get; set; }
        public string NufusaKayitliOlduguIlKodu { get; set; }
        public string NufusaKayitliOlduguIlceAdi { get; set; }
        public string NufusaKayitliOlduguIlcePlakaKodu { get; set; }
        public string NufusaKayitliOlduguIlceKodu { get; set; }
        public string UyrukAdi { get; set; }
        public string UyrukUlkeKodu { get; set; }
        public long? DilId { get; set; }
        public string DilAdi { get; set; }
        public string DilKodu { get; set; }
        public string DilKoduYazi { get; set; }

    }
}




































